import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import reducers from '../client/reducers';

export default (baseURL) => {
    const axiosInstance = axios.create({
        baseURL: baseURL,
        headers: {}
    });
    return createStore(
        reducers,
        {},
        applyMiddleware(thunk.withExtraArgument(axiosInstance))
    );
};

import 'babel-polyfill';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import Routes from './client/routes';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';
import { Helmet } from "react-helmet";
const http = require("http");
const compression = require('compression');
const globalBaseUrl = require('../src/client/endpoints/index').baseUrl;
http.globalAgent.maxSockets = Infinity;

const app = express();

//Setup express
require("./express")(app);
app.use(compression());
app.use(express.static('public'));

const handleRenderer = (req, store, res) => {
    const promises = matchRoutes(Routes, req.path)
        .map(({ route }) => {
            return route.loadData ? route.loadData(store) : null;
        })
        .map(promise => {
            if (promise) {
                return new Promise((resolve, reject) => {
                    promise.then(resolve).catch(resolve);
                });
            }
        });
    Promise.all(promises).then(() => {
        const context = {};
        const content = renderer(req, store, context);
        if (context.url) {
            return res.redirect(301, context.url);
        }
        if (context.notFound) {
            res.status(404);
        }
        let reactString = content;
        let head = Helmet.rewind();
        head = {
            title: head.title.toString(),
            meta: head.meta.toString(),
            link: head.link.toString(),
            script: head.script.toString()
        };
        res.set('Cache-Control', 'no-store');
        res.render("index.jade", {
            reactString: reactString,
            head: head
        });
    });
};
const dynamicRenderer = (req, res) => {
    const store = createStore(globalBaseUrl);
    handleRenderer(req, store, res);
};
const dynamicCityRenderer = (req, res) => {
    const store = createStore(globalBaseUrl + "city/" + req.params.subpage);
    handleRenderer(req, store, res);
};
const dynamicCountryRenderer = (req, res) => {
    const store = createStore(globalBaseUrl + "countries/" + req.params.page);
    handleRenderer(req, store, res);
};
const dynamicBlogRenderer = (req, res) => {
    const store = createStore(globalBaseUrl + "blogs/" + req.params.subpage);
    handleRenderer(req, store, res);
};
const dynamicPropertyRenderer = (req, res) => {
    const store = createStore(globalBaseUrl + "property/" + req.params.property);
    handleRenderer(req, store, res);
};
const dynamicUniversityRenderer = (req, res) => {
    const store = createStore(globalBaseUrl + "university/" + req.params.subsubpage);
    handleRenderer(req, store, res);
};


app.get('/:page/:subpage/:superpage/book-now/thank-you', dynamicRenderer);
app.get('/:page/:subpage/:superpage/get-in-touch/thank-you', dynamicRenderer);
app.get('/:page/:subpage/:superpage/book-now/paid', dynamicRenderer);
app.get('/:page/:subpage/:superpage/book-now/pay', dynamicRenderer);
app.get('/:page/:subpage/:superpage/book-now', dynamicRenderer);
app.get('/l/:subpage/:superpage/thank-you', dynamicRenderer);
app.get('/:page/:subpage/u/:subsubpage', dynamicUniversityRenderer);
app.get('/:country/:city/:property', dynamicPropertyRenderer);
app.get('/blog-details/:subpage', dynamicBlogRenderer);
app.get('/:page/:subpage', dynamicCityRenderer);
app.get('/:page', dynamicCountryRenderer);
app.get('/:page', dynamicRenderer);
app.get('/', dynamicRenderer);


http.createServer(app).listen(process.env.PORT || 9000, () => {
    console.log('Listening on port 9000');
});

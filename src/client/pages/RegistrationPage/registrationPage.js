import React, { Component } from "react";

import CountryCodeDropdown from "../../components/CountryCode/CountryCode";
import endPointConfig from "../../endpoints";

const axios = require('axios');

class RegistrationPage extends Component {
    state = {
        signupData : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            password : "",
            confirmPassword : ""
        },
        contactCountryCode : "",
        errorMessage : "",
        disableButton : false
    };

    componentDidMount() {
        if(sessionStorage.getItem("token")) {
            window.location.assign(document.referrer);
        }
    }

    handleFormInputValueChanges = (e) => {
        let obj = this.state.signupData;
        obj[e.target.id] = e.target.value;
        this.setState({
            signupData : obj
        })
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

    validateSignUpForm = () => {
        if(this.state.signupData.firstName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your first name.",
                disableButton : false
            })
            return false
        }
        else if(this.state.signupData.lastName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your last name.",
                disableButton : false
            })
            return false
        }
        else if(this.state.signupData.email.trim() === "") {
            this.setState({
                errorMessage : "Please enter your email address.",
                disableButton : false
            })
            return false
        }
        else if(this.state.signupData.contactNumber.trim() === "") {
            this.setState({
                errorMessage : "Please enter your contact number.",
                disableButton : false
            })
            return false
        }
        else if(this.state.signupData.password.trim() === "") {
            this.setState({
                errorMessage : "Your password must not contain space characters.",
                disableButton : false
            })
            return false
        }
        else if(this.state.signupData.password.trim() !== this.state.signupData.confirmPassword.trim()) {
            this.setState({
                errorMessage : "Your passwords did not match.",
                disableButton : false
            })
            return false
        }
        else return true
    };

    loginAfterSignUp = () => {
        let loginBody = {
            username : this.state.signupData.email,
            password : this.state.signupData.password
        }
        axios({
            method : 'POST',
            url : endPointConfig.login,
            data : loginBody
        })
            .then((res) => {
                if(res.status === 200) {
                    sessionStorage.setItem("token", res.data.access);
                    window.location.assign("/");
                }
            })
    };

    submitSignupForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : "",
            disableButton : true
        })
        if(this.validateSignUpForm() === true) {
            let signUpBodyData = {
                first_name : this.state.signupData.firstName.trim(),
                last_name : this.state.signupData.lastName.trim(),
                email : this.state.signupData.email.trim().toLowerCase(),
                contact_number : this.state.contactCountryCode + "-" + this.state.signupData.contactNumber.trim(),
                password : this.state.signupData.password.trim(),
            };
            axios({
                method : 'POST',
                url : endPointConfig.signUp,
                data : signUpBodyData
            })
                .then((res) => {
                    if(res.status === 201) {
                        this.setState({
                            errorMessage : "You are signed up successfully.",
                            disableButton : false
                        }, () => {
                            this.loginAfterSignUp();
                        })
                    }
                })
                .catch((err) => {
                    if(err) {
                        this.setState({
                            errorMessage : "EMail Address already exists. Please login.",
                            disableButton : false
                        })
                    }
                })
        }
    };

    render() {
        return(
            <div className="registration_page">
                <div className="container">
                    <div className="row">
                        <div className="show_desktop col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div>

                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="registration_form_wrapper">
                                <p className="form_headline">Please enter the details to create a new account</p>
                                <form onSubmit={this.submitSignupForm}>
                                    <div className="single_field_row">
                                        <div className="single_field_wrapper">
                                            <input placeholder="FIRST NAME" value={this.state.signupData.firstName} type="text" id="firstName" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>

                                        <div className="single_field_wrapper">
                                            <input placeholder="LAST NAME" value={this.state.signupData.lastName} type="text" id="lastName" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                    </div>

                                    <div className="single_field_row">
                                        <div className="single_field_wrapper email_wrapper">
                                            <input placeholder="EMAIL ADDRESS" value={this.state.signupData.email} type="email" id="email" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                    </div>

                                    <div className="single_field_row">
                                        <div className="single_field_wrapper contact_wrapper">
                                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)} />
                                            <input placeholder="CONTACT NUMBER" value={this.state.signupData.contactNumber} type="number" id="contactNumber" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                    </div>

                                    <div className="single_field_row">
                                        <div className="single_field_wrapper">
                                            <input placeholder="ENTER PASSWORD" value={this.state.signupData.password} type="password" id="password" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>

                                        <div className="single_field_wrapper">
                                            <input placeholder="RE-ENTER PASSWORD" value={this.state.signupData.confirmPassword} type="password" id="confirmPassword" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                    </div>
                                    {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                                    <button disabled={this.state.disableButton} className="btn btn-primary">SIGN UP</button>
                                </form>
                                <p className="login_command">
                                    Already have an account?&nbsp;
                                    <a href="/login">
                                        Login Now
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component : RegistrationPage
};
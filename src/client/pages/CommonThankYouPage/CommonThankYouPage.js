import React, { Component } from "react";

import CommonThankYouMessage from "../../components/CommonThankYouMessage/CommonThankYouMessage";

class CommonThankYouPage extends Component {
    render() {
        return (
            <div className="common_thank_you_page">
                <CommonThankYouMessage />
            </div>
        );
    }
}

export default {
    component : CommonThankYouPage
};
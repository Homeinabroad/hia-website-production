import React, {Component} from 'react';

import AboutUsBanner from "../../../assets/graphics/aboutUs/about-us-banner.jpg";

class About extends Component {
    render() {
        return (
            <div className="about_us_page">
                <div className="banner_wrapper">
                    <img src={AboutUsBanner} alt="About Us" />
                </div>
                <div className="container">
                    <div className="row">
                        <div  className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="page_description">
                                <p className="page_heading">Making home away from home</p>
                                <p className="page_content">Homeinabroad is one of the leading marketplace for international student housing,
                                    helping student to find a perfect home. We have lived the journey, that every
                                    students are facing the pain to find the perfect accommodations. We have a widest
                                    and carefully crafted different range of rooms that are built and managed specific for
                                    students near to their universities.<br></br><br></br>
                                    We have given power to students and explore different range of properties, across
                                    different neighbourhoods so that you can make a perfect choice for your
                                    accommodation. Our tech-enabled platform helps the students to view the room
                                    through Virtual or Skype tours.<br></br>
                                    Our trained experts will guide you to find the perfect accommodation depending on
                                    your need and your budget. We want this experience to be hassle free.
                                </p>
                                <p className="page_heading">We treat our values as our life</p>
                                <p  className="page_content">We will be with on every step of your student journey, starting from finding the
                                    perfect accommodation to room essentials. We want to give you total peace of mind
                                    so we treat every student with Empathy, Integrity, Transparency and Commitment.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default {
    component: About
};
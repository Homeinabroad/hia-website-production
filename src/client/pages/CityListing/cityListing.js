import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import moment from 'moment';
import {connect} from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
import {Helmet} from "react-helmet";
import $ from "jquery";


import {mainListing} from '../../actions/index';
import endPointConfig, { cityPropertyListing } from "../../endpoints/index";
import LeadsGenerationForm from "../../components/LeadGenerationForm/leadGenerationForm";
import CoronaIcon from "../../../assets/graphics/details/corona-icon.svg";
import FilterIcon from "../../../assets/graphics/listingPage/filter-icon.png";


const axios = require("axios");

class CityListingPage extends Component {
    state = {
        propertyLists: [],
        noPropertyMessage: "",
        expandCityDescription : false,
        showCancellationPolicy: false,
        showFilterPopup : false,
        showCancellationFlexibility : false,
        pageNumber : 0,
        propertyLimit : 9,
        numberofAccommodations : "",

       // filterToggle : false,

    };

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getPropertyListing();
    }

    scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#leadsFormSection").offset().top - 100
        }, 1000);
    }

    handleFilterPopUp =() =>{
        this.setState({
            showFilterPopup : true
        })
    }

    closeFilterPopUp = () =>{
        this.setState({
            showFilterPopup : false
        })
    }

    openCancellationPolicy = () =>{
        this.setState({
        showCancellationPolicy:true
        }
        )
    };

    closeCancellationPolicy= ()=> {
        this.setState({
            showCancellationPolicy:false
            }
            )
    };

    openCancellationFlexibility = () =>{
        this.setState({
        showCancellationFlexibility:true
        }
        )
    };

    closeCancellationFlexibility = ()=> {
        this.setState({
            showCancellationFlexibility:false
            }
            )
    };

    loadMoreProperties = (e)=>{
        e.preventDefault();
        this.setState({
            pageNumber : this.state.propertyLimit+this.state.pageNumber,
        }, ()=>{
            this.getPropertyListing();
        })
    };

   

    // handleFilterToggle =() =>{
    //     this.setState({
    //         filterToggle : !this.state.filterToggle
    //     })
    // }
    

    expandDescription = (cityDescription) => {
        if (cityDescription === false) {
            this.setState({
                expandCityDescription: true
            }, () => {
                $('html, body').stop(true, false).animate({
                    scrollTop: $(".city_details").offset().top - 100
                }, 1000);
            });
        } else {
            this.setState({
                expandCityDescription: false
            }, () => {
                $('html, body').stop(true, false).animate({
                    scrollTop: $(".city_details").offset().top - 100
                }, 1000);
            });
        }
    };

    getPropertyListing = () => {
        axios.get(endPointConfig.cityPropertyListing + this.props.cityDetails.slug + "&limit=" + this.state.propertyLimit + "&offset=" + this.state.pageNumber )
            .then((res) => {
                if (res.status === 200 && res.data.results.length > 0) {
                    this.setState({
                        propertyLists:[...this.state.propertyLists, ...res.data.results],
                        numberofAccommodations : res.data.count,
                    })
                } else if (res.status === 200 && res.data.results.length === 0) {
                    this.setState({
                        noPropertyMessage: "No Property found for your searched result/specification."
                    })
                } else return null;
            })
           
    };

    head = () => {
        return (
            <Helmet>
                <title>{this.props.cityDetails.meta_title}</title>
                <meta name="description" content={this.props.cityDetails.meta_description}/>
                <meta name="keywords" content={this.props.cityDetails.meta_keyword}/>
                <link rel="canonical" href={"https://www.homeinabroad.com" + this.props.location.pathname}/>
            </Helmet>
        )
    };

    toggleSortAndFilterPopupForMobile = (manipulationType) => {
        if (manipulationType === "filetrData") {
            this.setState({
                showFilterPopupInMobile: !this.state.showFilterPopupInMobile
            })
        } else if (manipulationType === "sortData") {
            this.setState({
                showSortingPopupInMobile: !this.state.showSortingPopupInMobile
            })
        } else return null;
    };

    render() {
        let propertyLists = (
            this.state.propertyLists.length > 0 ?
                this.state.propertyLists.map((property, i) => {
                    let offersArray = [];
                    let offerObj = {
                        propertyId: "",
                        offers: []
                    }
                    property.propertyOffers.map((offer) => {
                        offerObj.propertyId = property.id;
                        if (moment(offer.validity_date).isAfter(moment())) {
                            offerObj.offers.push(offer.title);
                        }
                    })
                    offersArray = offerObj;
                    return (
                        <div>
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div key={i} className="single_property_wrapper">
                            <div className="property_description_image_wrapper">
                                <div className="property_image_wrapper">
                                <a href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug} target="_blank">
                                    <img src={property.first_image} alt={property.name}/>
                                    </a>
                                    <div className="rating_wrapper">
                                        <span className="glyphicon glyphicon-star"></span>
                                           {property.rating}
                                    </div>
                                </div>
                                <div className="property_config_wrapper">
                                <a href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug} target="_blank">
                                    <p className="property_name">{property.name}</p> 
                                   </a>
                                     <div className="room_type_wrapper">
                                     {
                                        property.roomCategories.length > 0 ?
                                            property.roomCategories.map((roomCategory, i) => {
                                                return (
                                                <p className="room_name"> {roomCategory.name} </p>
                                                )
                                            })
                                            : null
                                    }
                                    </div>
                                   
                                    <p className="distance">Distance From City Centre : {property.distance_from_city_center.toFixed(2) + " " + "miles"}
                                   </p>
                                    {
                                        offersArray.offers.length > 0 ?
                                            offersArray.offers.map((offer, i) => {
                                                return (
                                                    <div key={i} className="property_offers">
                                                        <p className="offer_title">{offer.length} OFFERS FOR YOU</p>
                                                    </div>
                                                )
                                            })
                                            : null
                                    }

                                <div className="price_amenity_wrapper">
                                    <div className="amenity_wrapper">
                                    <p className="amenities_headline">Amenities</p>
                                    {
                                        property.amenities.length > 0 ?
                                            property.amenities.map((amenity, i) => {
                                                return (
                                                    <img src={amenity.logo} alt={amenity.title} title={amenity.title}/>
                                                )
                                            })
                                            : null
                                    }
                                    </div>
                                    <p className="room_starting_price">starts at <span>{property.price_unit + property.min_bed_pricing + "/week"}</span></p>
                                    <div className="button_wrapper">
                                    <button onClick={this.scrollToForm} id="enquireButton" className="btn btn-success enquire_button">ENQUIRE
                                    </button>
                                    <a href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug} target="_blank">
                                        <button className="btn btn-primary redirect_button">VIEW ROOMS</button>
                                    </a>
                                    </div>
                                </div>        
                                </div>
                           
                                  
                            </div>
                        </div>
                        </div>
                        </div>
                    )
                })
                : null
        );

        return (
            <div className="city_listing_page">
                {this.head()}
              <img className="banner_image show_desktop" src={this.props.cityDetails.banner_image} alt={this.props.cityDetails.name}></img>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 className="listing_page_headline">
                                HomeinAbroad helps you find your perfect student accommodation in
                                <b> {this.props.cityDetails.name}</b>. We are always here to help you 24/7. Reserve your
                                accommodation in {this.props.cityDetails.name} now and pay later.
                            </h2>
                        </div>


                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="property_listing_wrapper">
                                {
                                    this.state.propertyLists.length > 0 ?
                                    <div className="property_filter_wrapper">
                                        <p className="property_listing_headline"> We have <span>
                                             {this.state.numberofAccommodations=== 1 ? this.state.numberofAccommodations + " " + " Accommodation" + ""
                                            : this.state.numberofAccommodations + " " + " Accommodations" + " "
                                            }   </span> in  {this.props.cityDetails.name} for you.
                                          
                                        </p> 

                                          

                                            <div className="policy_filter_wrapper">
                                                <div className="covid_policy_wrapper">
                                                    <p className="policy_check_line">Check Covid-19 Cancellation Policy. </p>
                                                    <p className="cancellation_policy" onClick={this.openCancellationPolicy}>
                                                     Learn More </p>
                                                 </div> 
                                                        {
                                                            this.state.showCancellationPolicy ?
                                                            <div className="policy_wrapper">
                                                                    <div className="close_policy" onClick={this.closeCancellationPolicy}>X</div>
                                                                        <div className="policy_content_wrapper">
                                                                        <p className="policy_title">  Covid-19 Cancellation Policy  </p>
                                                                    <p className="policy_content"  >
                                                                    Flexible Check In - Let us know if your Uni Term dates change and you won't be charged for
                                                                    weeks you don't need.</p>
                                                                    <li> 2020.03.13 - 2020.08.31</li>
                                                                    <p className="policy_terms"  >
                                                                    Terms & Conditions: This offer applies to all existing customers who booked and have 
                                                                    already completed a Tenancy Agreement for a room with us, and all new customers who complete
                                                                    a booking and complete a Tenancy Agreement for 2020/21 on or before 31st August 2020. To be 
                                                                    eligible to benefit from this promotion the original 2020/21 Tenancy Agreement must be for a 
                                                                    minimum tenancy length of 34 weeks, with a tenancy check in date on or before 31st October 2020. 
                                                                    This promotion excludes all short stays and/or semester stays. In order to benefit from this 
                                                                    promotion a customer’s university or higher education institute (HEI) must delay the start of 
                                                                    the 2020/21 Academic Year for the customer’s course. In the event that they do so we will be 
                                                                    willing to defer the check in date of that Tenancy Agreement and credit the customer’s account 
                                                                    for the unused complete calendar weeks of the tenancy, up until the earlier of (1) a period of one 
                                                                    calendar week before the new Academic Year start date, (2) the 31st October 2020, subject always to 
                                                                    the terms set out in this promotion. This offer also applies to customers whose course starts 
                                                                    initially online only. You can apply for this offer if the face to face teaching for your course 
                                                                    has a delayed start date. We will continue to monitor university and HEI Academic Year start dates
                                                                    and may review or amend this offer accordingly. If a university or HEI delays the start of the 
                                                                    Academic Year and a customer wishes to delay their check in date under this promotion they will
                                                                    need to contact us by email on or before 31st August 2020. Having received contact from a customer
                                                                    we will make a web-form available for the customer to complete on or before 7th September 2020 to
                                                                    inform us of their request for a new check in date in line with the above. We will then cross 
                                                                    check this information against their Tenancy Agreement and HEI Academic Year dates before 
                                                                    applying the promotion. The customer’s account will be adjusted to reflect the new check in
                                                                    date and any future payment changes once the customer has checked into their accommodation f
                                                                    or the 2020/21 Academic Year. Customers who have paid for the whole of their 2020/21 tenancy 
                                                                    in full before they check in will receive a refund for the unused weeks, after they have checked in.
                                                            </p>
                                                            </div>
                                                            </div>
                                                            : null
                                                        }   
                                                <div className="filter_options_wrapper">
                                              {/* <p className="filter_option" > Filters</p> 
                                                <img src={FilterIcon} onClick={this.handleFilterPopUp}></img>*/}
                                                {
                                                this.state.showFilterPopup ?
                                                <div className="filter_popup_wrapper">
                                                    <div className="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                        <div className="filters_wrapper">
                                                        <div className="close_filter" onClick={this.closeFilterPopUp}>X</div>
                                                            <div className="single_filter_category_wrapper">
                                                                <p>Rating</p>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>3 Star</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>4 Star</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>5 Star</label>
                                                                </div>
                                                            </div>

                                                            <div className="single_filter_category_wrapper">
                                                                <p>Property Type</p>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>PBSA</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Private Apartment</label>
                                                                </div>
                                                            </div>

                                                            <div className="single_filter_category_wrapper">
                                                                <p>Room Type</p>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Ensuite</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Studio</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Apartment</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>House</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Others</label>
                                                                </div>
                                                            </div>

                                                            <div className="single_filter_category_wrapper">
                                                                <p>Amenities</p>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Furnished</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Gym</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Cinema</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Games Room</label>
                                                                </div>

                                                                <div className="single_filter_type_wrapper">
                                                                    <input type="checkbox" />
                                                                    <label>Laundry</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                :null
                                            }
                                                 
                                            </div>
                                            </div>

                                </div>: null     
                                }
                                {propertyLists}
                                {this.state.noPropertyMessage !== "" && this.state.propertyLists.length === 0 ? <p className="noproperty_errormessage">{this.state.noPropertyMessage}</p> : null}
                          
                        </div>
                        </div>
                        <div className="pagination_wrapper">
                            {
                                this.state.numberofAccommodations >9 ?
                                <button onClick={this.loadMoreProperties} className={" load_more_button "  + (this.state.propertyLists.length === this.state.numberofAccommodations  ? "diasbled_button" : "")} >
                                {
                                    this.state.propertyLists.length === this.state.numberofAccommodations ?
                                    "You have done with all the properties" 
                                    :
                                    "LOAD MORE PROPERTIES ..." + this.state.propertyLists.length + "/" + this.state.numberofAccommodations
                                }
                                 </button>
                                : 
                                this.state.numberofAccommodations > 0 ? 
                                <button  className="load_more_button diasbled_button">
                                {
                                    this.state.propertyLists.length === this.state.numberofAccommodations ?
                                    "You have done with all the properties" 
                                    :
                                    null
                                }
                                 </button>
                                 : null
                            }
                              
                            </div>
                    </div>
                </div>

               {/* <div className="show_mobile filter_and_sort_link">
                    <div onClick={() => this.toggleSortAndFilterPopupForMobile("filetrData")}>FILTER</div>
                    <div onClick={() => this.toggleSortAndFilterPopupForMobile("sortData")}>SORT</div>
                </div> */}

   

               
                <div className="city_details_wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div id="leadsFormSection" className="leads_generation_form_wrapper">
                                    <LeadsGenerationForm/>
                                </div>
                                <h1 className="city_description_headline"> {this.props.cityDetails.heading1}</h1>
                                <p className={this.state.expandCityDescription 
                                    ? "expandedHeight city_details" 
                                    : "collapsedHeight city_details"}>

                                    {ReactHtmlParser(this.props.cityDetails.description)} </p>
                                    {
                                            this.state.expandCityDescription ?
                                                <h6 className="load_more"
                                                    onClick={() => this.expandDescription(this.state.expandCityDescription)}>
                                                    <b>Read Less </b></h6>
                                                :
                                                <h6 className="load_more"
                                                    onClick={() => this.expandDescription(this.state.expandCityDescription)}>
                                                    <b>Read More  </b></h6>

                                    }
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {cityDetails: state.cityDetails};
}

function loadData(store) {
    return store.dispatch(mainListing());
}

export default {
    loadData,
    component: withRouter(connect(mapStateToProps, {mainListing})(CityListingPage))
};
import React, {Component} from "react";

import MeetGreetBanner from "../../../assets/graphics/servicesPage/meetGreetPage/meet-and-greet-banner.jpg";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class MeetAndGreetPage extends Component {
    

    render() {
        return (
            <div className="meet_greet_page_wrapper">
                   <div className="meet_greet_hero_section">
                    <div className="meet_greet_hero_image_wrapper">
                            <img src={MeetGreetBanner}></img>
                      </div>
                </div>


                <div className="container">

                <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="meet_greet_content_section">
                    <p className="meet_greet_title">Meet and Greet Service</p>
                    <p className="meet_greet_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum

                     
                    </p>
                </div>
            </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Get Meet And Greet Service</p>
                    <ServiceForm/>
                 </div>
                </div>
                </div>
                </div>
            </div>
        )
    }
}

export default {
    component: MeetAndGreetPage
};
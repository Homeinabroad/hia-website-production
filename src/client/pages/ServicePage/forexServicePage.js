import React, {Component} from "react";

import ForexBanner from "../../../assets/graphics/servicesPage/forexPage/forex-banner.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class ForexServicePage extends Component {
    

    render() {
        return (
            <div className="forex_page_wrapper">
                   <div className="forex_hero_section">
                    <div className="forex_hero_image_wrapper">
                            <img src={ForexBanner}></img>
                      </div>
                </div>


                <div className="container">
                <div className="row">

<               div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="forex_content_section">
                    <p className="forex_title"> FOREX </p>
                    <p className="forex_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum

                     <br></br>
                     <br></br>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum
                    </p>
                </div>
                </div>

            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Get Forex Solutions</p>
                    <ServiceForm/>
                 </div>
                </div>
                </div>
                </div>
            </div>
        )
    }
}

export default {
    component: ForexServicePage
};
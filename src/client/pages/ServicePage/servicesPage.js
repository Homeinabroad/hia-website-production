import React, { Component } from 'react';
import $ from 'jquery';

import webConfig from '../../../../webConfig.json';
import GetServiceForm from "../../components/ServiceForm/serviceForm";

import LeadsBannerImage from '../../../assets/graphics/servicesPage/mainServicePage/main-service-page-banner.jpg';
import AccommodationServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/accommodation-icon.svg';
import RoomReplacementServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/room-replacement-icon.svg';
import RoomEssentialsServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/room-essentials-icon.svg';
import RemittanceServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/remmitance-service-icon.svg';
import ForexServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/forex-service-icoon.svg';
import AirportPickUpServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/airport-pickup-icon.svg';
import EducationLoanServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/education-loan-icon.svg';
import MeetAndGreetServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/meet-and-greet-icon.svg';
import GuarntorServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/gurantor-service-icon.svg';
import OverSeasSimServiceIcon from '../../../assets/graphics/servicesPage/mainServicePage/overseas-icon.svg';

class Services extends Component {
   

    componentDidMount() {
     
    }

    handleServiceSelection = (e) => {
        this.setState({
            selectedService: e.target.id
        },() => {
            sessionStorage.setItem("selectedServiceRequest", this.state.selectedService);
        })
    };

    render() {
        return (
            <div className="service_page">
                   <div className="landingPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={LeadsBannerImage} alt="HomeinAbroad"/>
                    </div>

                </div>

                <div className="container">
                <p className="page_headline">
                     We know that you need a lot with your accommodation. 
                     <br></br>Don't worry! We have value added service available at our platform 
                </p>

                <div className="main_service_wrapper">
                    <div className="row"> 
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div  className="single_service_wrapper">
                                <img src={AccommodationServiceIcon}/>
                                <p className="service_title">Accommodation</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/accommodation-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div className="single_color_service_wrapper">
                                <img  src={RoomEssentialsServiceIcon} />
                                <p className="service_title" >Room Essentials</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/room-essentials" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_service_wrapper">
                               <img src={RoomReplacementServiceIcon}></img>
                                <p className="service_title" >Room Replacement</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/room-replacement-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>
                        </div>

                        <div className="row">
                       <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_color_service_wrapper">
                                <img src={AirportPickUpServiceIcon}  ></img>
                                <p className="service_title">Airport PickUp</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/airport-pickup-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>
                      
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div  className="single_service_wrapper">
                                <img src={ForexServiceIcon}  ></img>
                                <p className="service_title" >Forex</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/forex-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_color_service_wrapper">
                               <img src={RemittanceServiceIcon}></img>
                                <p className="service_title">Remittance</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/remittance-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>
                        </div>

                        <div className="row">
                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div  className="single_service_wrapper">
                                <img src={GuarntorServiceIcon}  ></img>
                                <p className="service_title">Guarntor</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/gurantor-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_color_service_wrapper">
                                <img src={OverSeasSimServiceIcon}  ></img>
                                <p className="service_title">OverSeas Service</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/overseas-sim-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>
                       
                       

                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_service_wrapper">
                                <img src={EducationLoanServiceIcon}  ></img>
                                <p className="service_title">Education Loan</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/education-loan-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                        </div>

                        </div>
                        <div className="row">
                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            </div>
                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div className="single_service_wrapper">
                                <img src={MeetAndGreetServiceIcon}  ></img>
                                <p className="service_title">Meet And Greet</p>
                                <p className="service_description"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                                  eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                <a href="/services/meet-and-greet-service" target="blank">
                                <button  className="btn tbn-primary">KNOW MORE</button>
                                </a>
                            </div>
                            </div>
                        </div>
                        </div>

                        <div className="service_form">
                        <p className="form_title">Get your Service</p>
                            <GetServiceForm/>
                        </div>

                 
                </div> 
            </div>
        );
    }

};

export default {
    component: Services
};
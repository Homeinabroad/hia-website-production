import React, {Component} from "react";

import AccommodationBanner from "../../../assets/graphics/servicesPage/accommodationPage/accommodation-banner.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class AccommodationPage extends Component {
    

    render() {
        return (
            <div className="accommodation_page_wrapper">
                   <div className="accommodation_hero_section">
                    <div className="accommodation_hero_image_wrapper">
                            <img src={AccommodationBanner}></img>
                      </div>
                </div>


                <div className="container">
                <div className="row">

            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="accommodation_content_section">
                    <p className="accommodation_title"> Accommodation </p>
                    <p className="accommodation_description">
                    Planning to study abroad can be easy but finding the 
                    right place to stay abroad isn't that easy.

                     <br></br>
                     <br></br>
                     But don't worry HomeInAbroad will help you in finding 
                     the right accommodation for students. We will provide you best and 
                     verified properties which has been handpicked by student accommodation
                     experts.Based on its onsite facilities which makes your daily life easy
                     and on its latest security features which gives you a secure environment.
                    <br></br>
                    <br></br>
                    Our Support Team is available 24hrs to help you on shortlist properties and
                     to resolve all your queries. So waiting for what just go and book your perfect accomodation right now!
                    </p>
                </div>
                </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Get Your Accommodation</p>
                    <ServiceForm/>
                 </div>
                 </div>
                 </div>
                </div>
            </div>
        )
    }
}

export default {
    component: AccommodationPage
};
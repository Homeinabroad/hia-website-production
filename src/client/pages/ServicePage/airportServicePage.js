import React, {Component} from "react";

import AirportBanner from "../../../assets/graphics/servicesPage/airportServicePage/airport-service-banner.png";
import StepFirstIcon from '../../../assets/graphics/servicesPage/airportServicePage/step-one.svg';
import StepSecondIcon from '../../../assets/graphics/servicesPage/airportServicePage/step-second.svg';
import StepThirdIcon from '../../../assets/graphics/servicesPage/airportServicePage/step-third.svg';
import FirstCar from "../../../assets/graphics/servicesPage/airportServicePage/car-first.png";
import SecondCar from "../../../assets/graphics/servicesPage/airportServicePage/car-second.png";
import ThirdCar from "../../../assets/graphics/servicesPage/airportServicePage/car-third.png";
import FourthCar from "../../../assets/graphics/servicesPage/airportServicePage/car-fourth.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";



class AirportServicePage extends Component {
   state= {
       answerSection:"ans_1",
       
   }
   expandFaqSection = (e) =>{
        this.setState({
            answerSection : e.target.id,
        })
    }

    collapseFaqSection = () =>{
        this.setState({
            answerSection:false
        })
    }

   

    render() {
        return (
            <div className="airport_service_page_wrapper">
                 <div className="servicePage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={AirportBanner} alt="HomeinAbroad"/>
                    </div>
                    <div className="hero_content_wrapper">
                        <p className="hero_title">TRANSFERS TO AND FROM ALL 
                        <br></br>INTERNATIONAL AIRPORTS </p>
                    </div>
                </div>

                <div className="container">
                <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="airport_content_section">
                <p className="airport_title"> Airport PickUp </p>
                    <p className="airport_description"> Planning to study abroad can be an exciting experience, 
                        but traveling into and out of the busiest airport system in the 
                        world can sometimes be a stressful experience. We understand it for
                         our students and that is the reason we came up with this service
                          where students can book airport pickup services from us and get
                           to their accommodation hassle-free.
                           <br></br>
                           <br></br>
                     Homeinabroad partnered with some of the top airports pick-up 
                        services, where the student can book the airport pickup and 
                        reach their destination in a safe and sound manner. More to give 
                        you exclusive student discounts that can’t be found anywhere else.
                         On top of that, we also offer frequent promo codes to make your
                          airport pick-up the cheapest it can possibly be.
                    </p>
                     </div>
                </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <p className="service_title">Have a Question</p>
                <div className="contact_form">
                        <ServiceForm/>
                    </div>
                    </div>
                    </div>

                    <div className="login_section">
                    <p className="login_line">Login/Register Now to avail free discount on flight booking</p>
                    <button><a href="/login" target="blank">Login</a> / <a href="/registration" target="blank">Register</a></button>
                    </div>

                    <div className="row">
                        <div className="how_airport_works_section">
                            <p className="how_pickup_works_headline">How does Homeinabroad
                             Airport pick-up work,exactly?</p>
                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="single_step_section">
                                <p className="step_heading">Step 1</p>
                                <img src={StepFirstIcon}></img>
                                <p className="step_description">
                                Register/Login to Homeinabroad and Become a member (for FREE)
                                and unlock our best prices and fares. Wanna save even more?
                                Check out our promo codes for additional savings!
                                </p>
                            </div>
                            </div>

                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="single_step_section">
                                <p className="step_heading">Step 2</p>
                                <img src={StepSecondIcon}></img>
                                <p className="step_description">
                                Search for the airport or city you are going to
                                 land and we will showcase your a list of available 
                                 cars with pricing. We get special discounts from the 
                                 different vendors just for students, so you can get the 
                                 best deals no matter where you’re headed.

                                </p>
                            </div>
                            </div>

                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="single_step_section">
                                <p className="step_heading">Step 3</p>
                                <img src={StepThirdIcon}></img>
                                <p className="step_description">
                                Book a super cheap pick-up with our student and youth
                                discounts, then start getting ready for an amazing trip!
                                </p>
                            </div>
                            </div>
                        </div>
                    </div> 

                    <p className="car_section_headline">YOUR CHOICE OF CAR CLASSES </p>
                    <div className="car_section">
                    <div className="row">
                  
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div className="single_car_section"> 
                        <p className="car_name">ECONOMY LIGHT</p>
                        <p className="car_quality">Up to 3 Passangers</p>
                        <p className="car_quality">2 pieces of baggage</p>
                        <p className="car_quality">e.g. Toyota Corrola, VW Golf</p>
                        <img src={FirstCar}></img>
                    </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div className="single_car_section"> 
                        <p className="car_name">STANDARD</p>
                        <p className="car_quality">Up to 3 Passangers</p>
                        <p className="car_quality">2 pieces of baggage</p>
                        <p className="car_quality">e.g. VW Passat, Mercedes-Benz C-Class</p>
                        <img src={SecondCar}></img>
                    </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div className="single_car_section">
                        <p className="car_name">FIRST CLASS</p>
                        <p className="car_quality">Up to 3 Passangers</p>
                        <p className="car_quality">2 pieces of baggage</p>
                        <p className="car_quality">e.g. Mercedes-Benz S-Class, BMW 7 Series</p>
                        <img src={ThirdCar}></img>
                    </div>
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div className="single_car_section">
                        <p className="car_name">BUSINESS SUV</p>
                        <p className="car_quality">Up to 6 Passangers</p>
                        <p className="car_quality">4 pieces of baggage</p>
                        <p className="car_quality">e.g. Mercedes-Benz V-Class</p>
                        <img src={FourthCar}></img>
                    </div>
                    </div>
                </div>
                </div>

                    <div className="faq_section">
                        <p className="faq_heading">Frequently Asked Questions</p>
                        <div className="single_question_wrapper" id="ans_1">
                        <p className="question_line">Q. Where will my driver pick me up?</p>
                        {
                            this.state.answerSection === "ans_1" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_1"></span>  
                        }
                        {
                        this.state.answerSection === "ans_1" ?
                        <p className="answer_line">A. For pickups at any airport in your
                         driver will be waiting to greet you once you have exited baggage 
                         claim. They'll be holding a sign with your name clearly displayed 
                         on it so you can find them quickly.
                       <br></br>
                       <br></br>
                        When you book your transfer, you are able to enter your flight
                        number which will let your driver know exactly where to pick you up. This 
                        is particularly important if you are flying into international airport 
                       there will be more than one passenger terminal.
                       <br></br>
                       <br></br>
                       If you are booking a transfer to the airport, your driver will wait 
                       for you outside your chosen address or inside the lobby of your 
                       accommodation/hotel, anywhere in and around city.
                        </p>
                        : null                
                        }
                        </div>

                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_2">
                        <p className="question_line">Q. What happens if my flight is delayed?</p>
                        {
                             this.state.answerSection === "ans_2" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_2"></span>  
                        } 
                     
                        {
                        this.state.answerSection === "ans_2" ?
                        <p className="answer_line">A. As long as you enter your flight
                         number when you book your transfer from any of airports, 
                         your driver will be able to track your actual arrival time. 
                         So if your flight is delayed, then your pickup time will simply 
                         be adjusted and your driver will be there to pick you up when you 
                         actually arrive.
                        </p>
                        :  null          
                        }
                        </div>

                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_3">
                            <p className="question_line">Q. What if I am held up at the airport?</p>
                        {
                        this.state.answerSection === "ans_3" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_3"></span>  
                        }
                    
                        {
                        this.state.answerSection === "ans_3"?
                        <p className="answer_line">A. You don’t need to worry if you take 
                        longer to reach your driver than expected. Each transfer from the
                         airport comes with up to 60 minutes free waiting time depending 
                         on your chosen car class. During the booking process, you are also
                        able to select how long after landing you will be picked up to give
                         yourself some additional time if you think it will be necessary.
                        </p>
                        : null           
                        }
                        </div>
                        <hr></hr>
                    </div>
                
                </div>
            </div>
        )
    }
}

export default {
    component: AirportServicePage
};
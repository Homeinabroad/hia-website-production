import React, {Component} from "react";

import RoomReplacementBanner from "../../../assets/graphics/servicesPage/roomReplacementPage/room-replacement-banner.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class AccommodationPage extends Component {
    

    render() {
        return (
            <div className="room_replacement_page_wrapper">
                   <div className="room_replacement_hero_section">
                    <div className="room_replacement_hero_image_wrapper">
                            <img src={RoomReplacementBanner}></img>
                      </div>
                </div>


                <div className="container">
                <div className="row">

                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div className="room_replacement_content_section">
                    <p className="room_replacement_title"> Easy Room Replacement </p>
                    <p className="room_replacement_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                    tempor incididunt ut labore et dolore magna aliqua

                     <br></br>
                     <br></br>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
                     do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                      ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                       reprehenderit in voluptate velit esse cillum dolore eu fugiat
                        nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                         sunt in culpa qui officia deserunt mollit anim id est laborum
                    </p>
                </div>
                </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Replace Your Room</p>
                    <ServiceForm/>
                 </div>
                 </div>

                 </div>
                </div>
            </div>
        )
    }
}

export default {
    component: AccommodationPage
};
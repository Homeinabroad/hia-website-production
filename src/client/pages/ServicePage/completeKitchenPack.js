import React, {Component} from "react";
import $ from "jquery";

import CompleteKitchenPack from "../../../assets/graphics/servicesPage/roomEssentialsPage/kitchen-pack-page.png";
import Kitchen from "../../../assets/graphics/servicesPage/roomEssentialsPage/kitchenPack.jpg";
import Crockery from "../../../assets/graphics/servicesPage/roomEssentialsPage/crockery-image.jpg";
import ListIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/list-icon.png";
import KitchenLinen from "../../../assets/graphics/servicesPage/roomEssentialsPage/kitchen-linen.jpg";
import CookingUtensils from "../../../assets/graphics/servicesPage/roomEssentialsPage/cooking-utensils.jpg";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class CompleteKitchenPage extends Component {
    state= {
        openSection:false,
        
    }
    expandFacilitySection = (e) =>{
         this.setState({
             openSection : e.target.id,
         })
     };
 
     collapseFacilitySection = () =>{
         this.setState({
             openSection:false
         })
     };

     scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };
 
    render() {
        return (
            <div className="room_essentials_page_wrapper">
               <div className="kitchenPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={CompleteKitchenPack} alt="HomeinAbroad"/>
                    </div>
                    <div className="kitchen_hero_content_wrapper">
                        <p className="kitchen_title">Kitchen Pack</p>
                        <hr></hr>
                        <p className="price_title">Price £ 140</p>
                        <button className="enquire_Button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                    </div>
                </div>

                <div className="container">
                <div className="kitchen_content_wrapper">
                    <p className="kitchen_content_title"> Cook & Dine at University with our 42 piece kitchen set</p>
                    <p className="kitchen_content"> Satisfy your cooking and dining needs. </p>
                    <p className="kitchen_content_question">Want to dine and cook at University with ease and style? </p>
                    <p className="kitchen_content">
                    Our Luxury Student Kitchen Pack is for you. We have created this luxury kitchen set with 
                    the University experience in mind; Induction cooker ready pots and pans that work with any type of cooker, stylish utensils and high quality durable plates that will not get broken easily, 
                    as well as simple to clean kitchen accessories.</p>
                    <p className="kitchen_content">Our pots and pans work with any cooker including gas, electric or halogen, because University Cookers 
                    may vary.</p>
                    <p className="kitchen_content">Don’t settle for any essentials at University, choose our kitchen pack for ultimate convenience and value.
                    </p>
                    <p className="kitchen_content">We will deliver your pack on time to whatever location & date you choose, you can also choose the time you need delivery, so order with confidence now.
                    </p>
                    <p className="facility_content_heading">This Pack Includes</p>

                    <div className="facility_wrapper">
                        <div className="row">

                         <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper" id="facility_1">
                                 <img src={Kitchen}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">Cookware & Bakeware</p>
                                      {
                                        this.state.openSection === "facility_1" ?
                                          <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_1"></span>  
                                        }
                                      
                                        {
                                             this.state.openSection === "facility_1" ?
                                        <div>
                                      <div className="single_facility_content_heading">
                                       <img className="list_icon" src={ListIcon}></img>
                                       <div className="facility_description">
                                        <p className="title" >Premium Quality 18cm Saucepans </p>
                                        <p className="sub_title" > 2 Included, suitable for use with any University cooker, 
                                        including induction. Lids included. Non Stick. </p>
                                        </div>
                                        </div>

                                        <div className="single_facility_content_heading">
                                        <img className="list_icon"  src={ListIcon}></img>
                                        <div className="facility_description">
                                        <p className="title" >Premium Quality 20cm Frying Pan</p>
                                         <p className="sub_title">Suitable for use with any University cooker, including induction.
                                       Non Stick.</p>
                                        </div>
                                        </div>

                                        <div className="single_facility_content_heading">
                                        <img className="list_icon"  src={ListIcon}></img>
                                        <div className="facility_description">
                                        <p className="title" >Baking Tray</p>
                                        </div>
                                        </div>
                                        </div> 
                                        : null
                                        }      
                             </div>
                             </div>
                         </div>
                   
                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div className="single_facility_wrapper" id="facility_2">
                    <img src={Crockery}></img>
                    <div className="facility_content_wrapper">
                        <p className="facility_title">Crockery</p>
                        {
                                            this.state.openSection === "facility_2" ?
                                          <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_2"></span>  
                                        }
                                      
                                        {
                                             this.state.openSection === "facility_2" ?
                                        <div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Upgraded Premium 100% Stainless Steel Cutlery </p>
                        <p className="sub_title">
                        16 Pieces: Knives, Forks, Spoons & Tea Spoons Included</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Large Plate </p>
                     <p className="sub_title"> 2 included – Dishwasher safe- chip resistant </p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Bowl  </p>
                       <p className="sub_title"> 2 included – Dishwasher safe- chip resistant</p>
                         </div>
                         </div>

                         <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Side Plate   </p>
                       <p className="sub_title"> 2 included – Dishwasher safe- chip resistant</p>
                         </div>
                         </div>

                         <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Mug    </p>
                       <p className="sub_title">2 included – Dishwasher safe- chip resistant</p>
                         </div>
                         </div>

                         <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Glass Tumbler  </p>
                       <p className="sub_title">2 included </p>
                         </div>
                         </div>
                         </div> 
                         : null}
                       
                        </div>
                        </div>
                        </div>
                                            
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_3">
                        <img src={KitchenLinen}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Kitchen Linen</p>
                        {
                                            this.state.openSection === "facility_3" ?
                                          <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_3"></span>  
                                        }
                                      
                                        {
                                             this.state.openSection === "facility_3" ?
                                        <div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Tea Towels </p>
                        <p className="sub_title">  2 included</p>
                        </div>
                        </div>
                       </div>
                       : null }
                         </div>
                        </div>
                        </div>
                        
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_4">
                        <img src={CookingUtensils}></img>
                        <div className="facility_content_wrapper">
                            <p className="facility_title">Cooking Utensils</p>
                            {
                                            this.state.openSection === "facility_4" ?
                                          <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_4"></span>  
                                        }
                                      
                                        {
                                             this.state.openSection === "facility_4" ?
                                        <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Slotted Serving Spoon </p>
                        < p className="sub_title">  Premium wood & Grey Silicone design</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Slotted Turner </p>
                            <p className="sub_title"> Premium wood & Grey Silicone design</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Spatula </p>
                            <p className="sub_title">Premium wood & Grey Silicone design</p>
                           </div> 
                           </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Whisk  </p>
                           <p className="sub_title"> Premium wood & Grey Silicone design</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Wooden Spoon Cheese Grater</p>  
                           <p className="sub_title"> Versatile folded design, Black & Chrome detailing</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Vegetable Peeler </p>
                            <p className="sub_title">Black & Chrome detailing </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Bottle Opener  </p>
                            <p className="sub_title">Black & Chrome detailing </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Kitchen Scissors Colonder  </p>
                            <p className="sub_title">Grey Silicone, collapsible design </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Measuring Jug Wooden Chopping Board  </p>
                            <p className="sub_title">Ultra durable, dishwasher safe </p>
                            </div>
                            </div>
                            </div> 
                            : null}
                            </div>
                            </div>
                        </div>

                         </div>
                    </div>

                    <div className="button_wrapper">
                    <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                    </div>

                    <div className="important_points_wrapper">
                     <p > * Items may vary in exact color or dimensions to the image shown depending on availability.  </p>
                    </div>

                    <div id="formSection" className="contact_form">
                        <ServiceForm/>
                    </div>

                             
                </div>

                </div>
            </div>
        )
    }
}

export default {
    component: CompleteKitchenPage
};
import React, {Component} from "react";

import RemmitanceBanner from "../../../assets/graphics/servicesPage/remittancePage/remittance-banner.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";


class RemittanceServicePage extends Component {
    state ={
        expandRateCArdList : false
    }
    
    viewFullRateCardList = () =>{
       this.setState({
        expandRateCArdList : true
       })
    }
    viewLessRateCardList = () =>{
        this.setState({
         expandRateCArdList :false
        })
     }


    render() {
        return (
            <div className="remittance_page_wrapper">

                <div className="remittance_hero_section">
                    <div className="remittance_hero_image_wrapper">
                            <img src={RemmitanceBanner}></img>
                      </div>
                </div>

                <div className="container">
                <div className="row">

            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="remittance_content_section">
                    <p className="remittance_title"> Remittance </p>
                    <p className="remittance_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum
                     <br></br>
                     <br></br>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum
                    </p>
                </div>
                </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div className="contact_form">
                    <p className="form_title">Get Remittance Service</p>
                        <ServiceForm/>
                    </div>
                    </div>
                    </div>

                    <div className="remittance_rate">
                    <p className="remittance_rate_title">LIVE FOREX CURRENCY BUY EXCHANGE RATES</p>
                 
                        {
                            this.state.expandRateCArdList ?
                            <div className="table_wrapper">
                                <table>
                            <tr>
                                <th>CURRENCY</th>
                                <th>FOREX CARD</th>
                                <th>REMITTANCE</th>
                                <th>CASH</th>
                            </tr>

                            <tr>
                                <td>US Dollar</td>
                                <td>73.76</td>
                                <td>73.76</td>
                                <td>73.76</td>
                            </tr>

                            <tr>
                                <td>Euro</td>
                                <td>86.8137</td>
                                <td>86.8137</td>
                                <td>86.8137</td>
                            </tr>

                            <tr>
                                <td>UAE Dirham</td>
                                <td>20.3722</td>
                                <td>20.3722</td>
                                <td>20.3722</td>
                            </tr>

                            <tr>
                                <td>Thai Baht</td>
                                <td>2.7505</td>
                                <td>2.7505</td>
                                <td>2.7505</td>
                            </tr>

                            <tr>
                                <td>British Pound</td>
                                <td>95.6946</td>
                                <td>95.6946</td>
                                <td>95.6946</td>
                            </tr>

                            <tr>
                                <td>Australian Dollar</td>
                                <td>52.4401</td>
                                <td>52.4401</td>
                                <td>52.4401</td>
                            </tr>

                            <tr>
                                <td>Canadian Dollar</td>
                                <td>56.11</td>
                                <td>56.11</td>
                                <td>56.11</td>
                            </tr>

                            <tr>
                                <td>Kuwaiti Dinar</td>
                                <td>239.8725</td>
                                <td>239.8725</td>
                                <td>239.8725</td>
                            </tr>

                            <tr>
                                <td>Swiss Franc</td>
                                <td>81.0083</td>
                                <td>81.0083</td>
                                <td>81.0083</td>
                            </tr>

                            <tr>
                                <td>Danish Krone</td>
                                <td>12.0118</td>
                                <td>12.0118</td>
                                <td>12.0118</td>
                            </tr>

                            <tr>
                                <td>South African Rand</td>
                                <td>4.8512</td>
                                <td>4.8512</td>
                                <td>4.8512</td>
                            </tr>

                            <tr>
                                <td>Hong Kong Dollar</td>
                                <td>9.8654</td>
                                <td>9.8654</td>
                                <td>9.8654</td>
                            </tr>

                            <tr>
                                <td>Bahraini Dinar</td>
                                <td>194.8857</td>
                                <td>194.8857</td>
                                <td>194.8857</td>
                            </tr>

                            <tr>
                                <td>Japanese Yen</td>
                                <td>1.096167</td>
                                <td>1.096167</td>
                                <td>1.096167</td>
                            </tr>

                            <tr>
                                <td>Norwegian Krone</td>
                                <td>8.2905</td>
                                <td>8.2905</td>
                                <td>8.2905</td>
                            </tr>

                            <tr>
                                <td>New Zealand Dollar</td>
                                <td>49.089</td>
                                <td>49.089</td>
                                <td>49.089</td>
                            </tr>

                            <tr>
                                <td>Saudi Riyal</td>
                                <td>19.9486</td>
                                <td>19.9486</td>
                                <td>19.9486</td>
                            </tr>

                            <tr>
                                <td>Swedish Krona</td>
                                <td>8.7405</td>
                                <td>8.7405</td>
                                <td>8.7405</td>
                            </tr>

                            <tr>
                                <td>Swedish Krona</td>
                                <td>8.7405</td>
                                <td>8.7405</td>
                                <td>8.7405</td>
                            </tr>

                            <tr>
                                <td>Singapore Dollar</td>
                                <td>54.4206</td>
                                <td>54.4206</td>
                                <td>54.4206</td>
                            </tr>

                            <tr>
                                <td>Malaysian Ringitt</td>
                                <td>18.1185</td>
                                <td>18.1185</td>
                                <td>18.1185</td>
                            </tr>

                            <tr>
                                <td>Sri Lankan Rupee</td>
                                <td>0.7978</td>
                                <td>0.7978</td>
                                <td>0.7978</td>
                            </tr>

                            <tr>
                                <td>Omani Rial</td>
                                <td>190.9207</td>
                                <td>190.9207</td>
                                <td>190.9207</td>
                            </tr>

                            <tr>
                                <td>Chinese Yuan</td>
                                <td>11.3715</td>
                                <td>11.3715</td>
                                <td>11.3715</td>
                            </tr>

                            <tr>
                                <td>Qatari Riyal	</td>
                                <td>20.5483</td>
                                <td>20.5483</td>
                                <td>20.5483</td>
                            </tr>
                        </table>
                        <button onClick={this.viewLessRateCardList} id ="viewLessRate" className="view_rate_card">VIEW LESS RATE CARD</button>
                            </div>
                            : 
                            <div className="table_wrapper">
                                <table>
                            <tr>
                                <th>CURRENCY</th>
                                <th>FOREX CARD</th>
                                <th>REMITTANCE</th>
                                <th>CASH</th>
                            </tr>

                            <tr>
                                <td>US Dollar</td>
                                <td>73.76</td>
                                <td>73.76</td>
                                <td>73.76</td>
                            </tr>

                            <tr>
                                <td>Euro</td>
                                <td>86.8137</td>
                                <td>86.8137</td>
                                <td>86.8137</td>
                            </tr>

                            <tr>
                                <td>UAE Dirham</td>
                                <td>20.3722</td>
                                <td>20.3722</td>
                                <td>20.3722</td>
                            </tr>
                        </table>

                        <button onClick={this.viewFullRateCardList} id ="viewFullRate" className="view_rate_card">VIEW FULL RATE CARD</button>
                            </div>
                        }
                </div>


            </div>
            </div>
        )
    }
}

export default {
    component: RemittanceServicePage
};
import React, {Component} from "react";
import $ from "jquery";

import RoomBanner from "../../../assets/graphics/servicesPage/roomEssentialsPage/room-banner.png";
import BedroomPack from "../../../assets/graphics/servicesPage/roomEssentialsPage/bedroom-pack.jpg";
import BedLinen from "../../../assets/graphics/servicesPage/roomEssentialsPage/bed-image.jpg";
import Kitchen from "../../../assets/graphics/servicesPage/roomEssentialsPage/kitchenPack.jpg";
import Crockery from "../../../assets/graphics/servicesPage/roomEssentialsPage/crockery-image.jpg";
import ListIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/list-icon.png";
import KitchenLinen from "../../../assets/graphics/servicesPage/roomEssentialsPage/kitchen-linen.jpg";
import FoodContainer from "../../../assets/graphics/servicesPage/roomEssentialsPage/food-container.jpg";
import CookingUtensils from "../../../assets/graphics/servicesPage/roomEssentialsPage/cooking-utensils.jpg";
import BeddingSize from "../../../assets/graphics/servicesPage/roomEssentialsPage/bedding-size.jpg";
import PPEKit from "../../../assets/graphics/servicesPage/roomEssentialsPage/ppe-kit.jpg";
import ServiceForm from "../../components/ServiceForm/serviceForm";


class CompleteRoomPack extends Component {
    state= {
        openSection:false,
        
    }
    expandFacilitySection = (e) =>{
         this.setState({
             openSection : e.target.id,
         })
     };
 
     collapseFacilitySection = () =>{
         this.setState({
             openSection:false
         })
     };

     scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };
 
    render() {
        return (
            <div className="room_essentials_page_wrapper">
               <div className="complete_roomPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={RoomBanner} alt="HomeinAbroad"/>
                    </div>
                    <div className="hero_content_wrapper">
                        <p className="hero_title">Complete Room Pack</p>
                        <hr></hr>
                        <p className="price_title">Price £ 250</p>
                        <button className="enquire_Button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                    </div>
                </div>

                <div className="container">
                <div className="roomPack_content_wrapper">
                    <p className="roomPack_content_title">Most Popular - Our luxury all-in-one room pack includes our 
                    complete bedroom and kitchen pack in one box (60 items).</p>

                    <p className="roomPack_content"> Our Luxury all-in-one set contains all the items that you will 
                        need to cook and dine with friends and sleep in supreme comfort at 
                        University. Each item has been handpicked because of its quality,
                         versatility and usefulness- from the luxurious bedding & super 
                         stylish utensils, to the high quality non-stick induction pots 
                         and pans that work with any cooker. Rest assured, you now have the 
                         right essentials to get you off to a great start at Uni. We also 
                         include a PPE Amenity Kit in your pack.
                    </p>

                    <p className="roomPack_content">We know what items are perfect for Uni which is why they are durable, 
                        easy to clean and bold enough to stand out in a shared kitchen.</p>

                    <p className="roomPack_content">Our pots and pans work with any cooker including gas, electric or halogen, because University Cookers 
                    may vary.</p>

                    <p className="facility_content_heading">This Pack Includes</p>

                    <div className="facility_wrapper">
                        <div className="row">

                         <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper" id="facility_1">
                                 <img src={BedroomPack}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">Bedroom</p>
                                      {
                                        this.state.openSection === "facility_1" ?
                                          <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_1"></span>  
                                        }
                                         {
                                        this.state.openSection === "facility_1" ?
                                        <div>
                                      <p className="facility_subtitle">Duvet and Pillows</p>
                                      <div className="single_facility_content_heading">
                                       <img className="list_icon" src={ListIcon}></img>
                                       <div className="facility_description">
                                        <p className="title" >Luxury 10.5 Tog Duvet</p>
                                        <p className="sub_title" > Bouncy and 
                                            comfortable – Hollow fibre and warm enough for all year round + 
                                            Anti Allergen. </p>
                                            </div>
                                            </div>

                                        <div className="single_facility_content_heading">
                                        <img className="list_icon"  src={ListIcon}></img>
                                        <div className="facility_description">
                                        <p className="title" >Luxury Pillows</p>
                                      <p className="sub_title">2 Jumbo Luxury Pillows included in every pack,
                                        single, double or 3/4. (650 gsm quality)</p>
                                        </div>
                                        </div> 
                                        </div>
                                        : 
                                        null}
                                   
                             </div>
                             </div>
                         </div>
                   
                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div className="single_facility_wrapper" id="facility_2">
                    <img src={BedLinen}></img>
                    <div className="facility_content_wrapper">
                    <p className="facility_title">Bed Linen</p>
                          {
                            this.state.openSection === "facility_2" ?
                            <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                            : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_2"></span>  
                             }
                              {
                                this.state.openSection === "facility_2" ?
                                <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Premium Duvet Cover Set </p>
                        <p className="sub_title">
                        Choice of 6 University inspired designs 
                        (Please see photo gallery), includes matching pillow case. Made from luxurious 
                         combed yarns, our 180 thread count duvets sets are softer, stronger, 
                        and easy to care for. Includes matching pillow cases.</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Fitted Sheet </p>
                     <p className="sub_title"> White Colour, 200 thread count. Size will fit your bedding 
                        size choice. </p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Pillowcases </p>
                       <p className="sub_title"> 2 included.</p>
                         </div>
                         </div>
                         </div> 
                         : null}
                       
                        </div>
                        </div>
                        </div>
                     
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_3">
                        <img src={Kitchen}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Kitchen</p>
                        {
                            this.state.openSection === "facility_3" ?
                            <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                            : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_3"></span>  
                        }
                         {
                            this.state.openSection === "facility_3" ?
                            <div>
                        <p className="facility_subtitle">Cookware & Bakeware</p>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Premium Quality 16cm and 18cm Saucepans </p>
                        <p className="sub_title"> 2 Included, suitable for use with any University cooker, including induction. Lids included. Non Stick.</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Premium Quality 24cm Frying Pan </p>
                       <p className="sub_title">Suitable for use with any University cooker,including induction. Non Stick.</p>   
                         </div> 
                         </div>

                         <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Baking Tray </p>  
                            <p className="sub_title">Medium</p>
                            </div>
                            </div>
                            </div>
                            : null}
                         </div>
                        </div>
                        </div>
                        
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_4">
                        <img src={Crockery}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Crockery</p>
                            {
                                this.state.openSection === "facility_4" ?
                                <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                                : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_4"></span>  
                            }
                         {
                            this.state.openSection === "facility_4" ?
                            <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Upgraded Premium 100% Stainless Steel Cutlery </p>
                        < p className="sub_title"> 8 Pieces: Knives, Forks, Spoons & Tea Spoons Included</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Large Plate </p>
                            <p className="sub_title"> 2 included – Dishwasher safe- chip resistant</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" > Bowl </p>
                            <p className="sub_title">2 included – Dishwasher safe- chip resistant</p>
                           </div> 
                           </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Side Plate </p>
                           <p className="sub_title"> 2 included – Dishwasher safe- chip resistant</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Mug</p>  
                           <p className="sub_title"> 2 included – Dishwasher safe- chip resistant</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Glass Tumbler</p>
                            <p className="sub_title">2 included </p>
                            </div>
                            </div>
                            </div>
                            : null}
                            </div>
                            </div>
                        </div>


                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_5">
                        <img src={CookingUtensils}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Cooking Utensils</p>
                        {
                        this.state.openSection === "facility_5" ?
                        <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_5"></span>  
                        }
                         {
                            this.state.openSection === "facility_5" ?
                            <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Slotted Serving Spoon  </p>
                        < p className="sub_title"> Nylon Head</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Slotted Turner </p>
                            <p className="sub_title"> Nylon Head</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Spatula </p>
                            <p className="sub_title">Premium wood & Grey Silicone design</p>
                           </div> 
                           </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Masher </p>
                           <p className="sub_title">  Nylon Head</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Whisk</p>  
                           <p className="sub_title"> Stainless Steel</p>
                            </div>
                            </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Wooden Spoon Utility Knife</p>
                            <p className="sub_title"> Black </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Cheese Grater</p>
                            <p className="sub_title"> Black & Chrome detailing </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Vegetable Peeler </p>
                            <p className="sub_title"> Black & Chrome detailing </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Can/Bottle Opener</p>
                            <p className="sub_title">  Black & Chrome detailing </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Kitchen Scissors</p>
                            <p className="sub_title">  Black  </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Colonder </p>
                            <p className="sub_title"> Stainless steel  </p>
                            </div>
                            </div>

                            <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Measuring Jug 2x Wooden Chopping Board </p>
                            <p className="sub_title">  Ultra durable, dishwasher safe  </p>
                            </div>
                            </div>
                            </div>
                            : null}
                            </div>
                            </div>
                          </div>


                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_6">
                        <img src={KitchenLinen}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Kitchen Linen</p>
                        {
                        this.state.openSection === "facility_6" ?
                        <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_6"></span>  
                        }
                         {
                        this.state.openSection === "facility_6" ?
                        <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Tea Towels </p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >Black Oven Glove </p>
                        </div>
                        </div>
                        </div>
                        : null}
                        </div>
                        </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_7">
                        <img src={FoodContainer}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">Food Storage</p>
                        {
                        this.state.openSection === "facility_7" ?
                        <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_7"></span>  
                        }
                         {
                        this.state.openSection === "facility_7" ?
                        <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >Cereal Container </p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >Lock n Lock Lunch Box 2.3L</p>
                        </div>
                        </div>
                        </div> 
                        : null}
                        </div>
                        </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div className="single_facility_wrapper" id="facility_8">
                        <img src={PPEKit}></img>
                        <div className="facility_content_wrapper">
                        <p className="facility_title">PPE Amenity Kit</p>
                        {
                        this.state.openSection === "facility_8" ?
                        <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                        : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_8"></span>  
                        }
                         {
                        this.state.openSection === "facility_8" ?
                        <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >4 x sachets of anti bac wipes </p>
                        </div>
                        </div>
                        
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >4 x civilian non-medical facemask</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >4 x pairs of vinyl non-medical gloves</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >1 x 50ML anti bac gel</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="sub_title" >All packed in 1 x reusable organza bag</p>
                        </div>
                        </div>
                        </div> 
                        : null}
                        </div>
                        </div>
                        </div>

                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div className="single_facility_wrapper" id="facility_9">
                    <img src={BeddingSize}></img>
                    <div className="facility_content_wrapper">
                    <p className="facility_title">Bedding Sizing & Addition Information</p>
                    {
                    this.state.openSection === "facility_9" ?
                    <span onClick={this.collapseFacilitySection} className="arrow up_arrow"></span>
                    : <span onClick={this.expandFacilitySection} className="arrow down_arrow" id="facility_9"></span>  
                    }
                     {
                    this.state.openSection === "facility_9" ?
                     <div>
                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                        <p className="title" >STYLISH DUVET SET </p>
                        <p className="sub_title"> University inspired designs. Includes duvet cover and pillow covers.</p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                     <p className="sub_title"> Single Bed </p>
                        </div>
                        </div>

                        <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                       <p className="sub_title">Compact Double or 3/4 Bed / Small Double Bed</p>
                         </div>
                         </div>

                         <div className="single_facility_content_heading">
                        <img className="list_icon" src={ListIcon}></img>
                        <div className="facility_description">
                       <p className="sub_title">Double Bed</p>
                         </div>
                         </div>
                         </div>
                         : null}  
                        </div>
                        </div>
                        </div>

                        </div>
                    </div>

                    <div className="button_wrapper">
                    <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                    </div>

                    <div className="important_points_wrapper">
                        <p className="important_point_title">SOME IMPORTANT POINTS </p>
                             <p className="main_point">* Items may vary in exact color or dimensions to the image shown depending on availability.  </p>
                             <p className="main_point">* Our bed linen is made from combed yarns, which means the yarn has undergone the combing process
                              so that all the fibers are straight and parallel. This process creates a smoother, 
                              stronger, and more compact yarn that is excellent for weaving.  </p>          
                             <p className="main_point">* The majority of the cheaper duvet set
                              options are made with carded yarns. Carded yarn is a cotton
                               yarn which has been carded but not combed. This type of yarn 
                               contains a wide range of fibre length. As a result, carded 
                               yarn is not as uniform as combed yarns. Carded yarns are 
                               considerably cheaper and are used in coarse and medium counts.
                                </p>
                          
                    </div>

                    <div id= "formSection" className="contact_form">
                        <ServiceForm/>
                    </div>
                             
                </div>

                </div>
            </div>
        )
    }
}

export default {
    component: CompleteRoomPack
};
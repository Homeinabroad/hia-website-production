import React, {Component} from "react";
import $ from "jquery";

import RoomEssentialsBanner from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/room-essentials-banner.jpg";
import RoomPackRound from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/room-pack-round.png";
import BedRoomPackRound from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/bedroom-round.png";
import KitchenPackROund from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/kitchen-round.png";
import PPEKitRound from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/ppe-kit-round.png";
import OrderBeforeArrival from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/order-before-arrival.png";
import ListIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/list-icon.png";
import HowItWorks from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/how-it-works.png";
import PackIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/pack-icon.svg";
import DelieveryIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/delievery-icon.svg";
import SitRelaxIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/sit-relax-icon.svg";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class RoomEssentailsPage extends Component {
    state= {
        answerSection:"ans_1",
    }
    expandFaqSection = (e) =>{
         this.setState({
             answerSection : e.target.id,
         })
     }
 
     collapseFaqSection = () =>{
         this.setState({
             answerSection:false
         })
     }

    scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };

    render() {
        return (
            <div className="room_essentials_page_wrapper">
               <div className="essentialsPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={RoomEssentialsBanner} alt="HomeinAbroad"/>
                    </div>
                    <div className="hero_content_wrapper">
                        <p className="hero_title">Bedroom and Kitchen Packs</p>
                        <div className="hero_sub_section">
                            <p className="subheading_first">We packed everything you need to survive in your new home into our starter packs.</p>
                            <p className="subheading_second">Stressing over utensils to cook or tableware, shouldn’t be your main concerns when moving to a new place.
                             Pre-order to ensure your pack is delivered and ready for when you arrive.</p>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="room_essentails_main_wrapper">
                         <p className="room_essentails_main_title">Order Now</p>
                         <div className="row">
                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div className="single_service_main_wrapper">
                                    <div className="single_service_wrapper">
                                        <div className="image_wrapper">
                                            <img src={RoomPackRound}></img>
                                        </div>
                                        <div className="price_wrapper">  £225 </div>
                                        <div className="single_service_content">
                                            <p className="single_service_content_title">Complete Room Pack</p>
                                            <p className="single_service_description">MOST POPULAR - Our luxury all-in-one room 
                                            pack includes our complete bedroom and kitchen pack in one box
                                            (60 items)</p>
                                        </div>
                                    </div>
                                    <div className="buttons_wrapper">
                                    <a href="/services/room-essentials/complete-room-pack" target="blank">
                                        <button className="more_info_button">More Info</button>
                                        </a>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                    </div>
                                 </div>
                             </div>

                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div className="single_service_main_wrapper">
                                    <div className="single_service_wrapper">
                                        <div className="image_wrapper">
                                            <img src={BedRoomPackRound}></img>
                                        </div>
                                        <div className="price_wrapper">£95 </div>
                                        <div className="single_service_content">
                                            <p className="single_service_content_title">Complete Bedroom Pack</p>
                                            <p className="single_service_description">Soft and sumptuous, our bedding will make the 
                                            ideal surface to snuggle up in. Choose from 6 styles.
                                            From our warm duvets to our supportive pillows, give your new </p>
                                        </div>
                                    </div>
                                    <div className="buttons_wrapper">
                                    <a href="/services/room-essentials/complete-bedroom-pack" target="blank">
                                        <button className="more_info_button">More Info</button>
                                        </a>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                    </div>
                                 </div>
                             </div>

                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div className="single_service_main_wrapper">
                                    <div className="single_service_wrapper">
                                        <div className="image_wrapper">
                                            <img src={KitchenPackROund}></img>
                                        </div>
                                        <div className="price_wrapper">£140 </div>
                                        <div className="single_service_content">
                                            <p className="single_service_content_title">Complete Kitchen Pack</p>
                                            <p className="single_service_description">Cook & Dine at University with our 42 piece 
                                            kitchen set.Satisfy your cooking and dining needs.Want to dine and cook at </p>
                                        </div>
                                    </div>
                                    <div className="buttons_wrapper">
                                        <a href="/services/room-essentials/kitchen-pack" target="blank">
                                        <button className="more_info_button">More Info</button>
                                        </a>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                    </div>
                                 </div>
                             </div>

                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div className="single_service_main_wrapper">
                                    <div className="single_service_wrapper">
                                        <div className="image_wrapper">
                                            <img src={PPEKitRound}></img>
                                        </div>
                                        <div className="price_wrapper"> £225 </div>
                                        <div className="single_service_content">
                                            <p className="single_service_content_title">COVID-19: PPE and Hand Sanitiser</p>
                                            <p className="single_service_description">Order our Personal Protective Equipment (PPE)
                                             Amenity pack, or hand sanitiser products to help keep 
                                            you safe.PPE Amenity Kit contains: 4 x sachets of a </p>
                                        </div>
                                    </div>
                                    <div className="buttons_wrapper">
                                    <a href="/services/room-essentials/complete-ppekit-pack" target="blank">
                                        <button className="more_info_button">More Info</button>
                                        </a>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                    </div>
                                 </div>
                             </div>
                             </div>

                            <div className="row">
                             <div className="order_before_wrapper">
                                 <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile">
                                    <p className="mobile_order_heading">Save time and pre-order your room essentials</p>
                                    <img className="kitchen_pack_image" src={OrderBeforeArrival}></img>
                                 </div>
                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <div className="content_wrapper">
                                    <p className="show_desktop">Save time and pre-order your room essentials</p>
                                    <p>Order before your arrival</p>
                                    <p>Stressing over utensils to cook or tableware,
                                         shouldn’t be your main concerns when moving to 
                                         a new place. Make sure to hit the ground running 
                                         at your new home. Rest assured, our essential packs 
                                         have you covered with all the key things you will 
                                         need. Just check out how we can simplify your life 
                                         and moving experience.</p>
                                         <div className="list_wrapper">
                                             <div className="single_list_wrapper">
                                                 <img src={ListIcon}></img>
                                                 <p className="list_content">All the Essentials in one Box</p>
                                             </div>

                                             <div className="single_list_wrapper">
                                                 <img src={ListIcon}></img>
                                                 <p className="list_content">Free & Convenient Delivery</p>
                                             </div>

                                             <div className="single_list_wrapper">
                                                 <img src={ListIcon}></img>
                                                 <p className="list_content">Only Quality Items included</p>
                                             </div>
                                         </div>
                                  </div>
                                 </div>
                                 <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">
                                   <img className="kitchen_pack_image" src={OrderBeforeArrival}></img>
                                 </div>
                             </div>
                         </div>

                         <div className="row">
                             <div className="how_it_works_wrapper">
                             <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                             <p className="show_mobile"> How it Works</p>
                                    <img src={HowItWorks}></img>
                                    </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                    <div className="how_it_works_content">
                                        <p className="show_desktop"> How it Works</p>
                                        <div className="single_how_it_works_wrapper">
                                            <img src={PackIcon}></img>
                                            <p className="single_wrapper_headline">Pick your pack</p>
                                            <p className="single_wrapper_content">Select and customise your pack from the 
                                            range of our bedding, kitchen and bathroom products.</p>
                                        </div>
                                        <div className="single_how_it_works_wrapper">
                                        <img src={DelieveryIcon}></img>
                                            <p className="single_wrapper_headline">Select delivery date</p>
                                            <p className="single_wrapper_content">Select your delivery date, we recommend
                                             ordering your pack before your due to arrive.</p>
                                        </div>
                                        <div className="single_how_it_works_wrapper">
                                        <img src={SitRelaxIcon}></img>
                                            <p className="single_wrapper_headline" >Sit back and relax</p>
                                            <p className="single_wrapper_content">Your first steps covered, move in stress free knowing 
                                            all your essentials are ready and waiting.</p>
                                        </div>
                                    </div>
                                    </div>   
                             </div>
                         </div>

                         <div id= "formSection" className="contact_form">
                             <p className="service_title">Get Room Essentials Service</p>
                        <ServiceForm/>
                    </div>
      
                    
                    <div className="frequently_question_wrapper">
                         <div className="frequently_asked_questions">
                             <p className="frequently_title">Frequently Asked Questions</p>
                            
                             <p className="question_title">Placing Orders</p>

                        <div className="single_question_wrapper" id="ans_1">
                        <p className="question_line">Q. When should I place my order?</p>
                        {
                        this.state.answerSection === "ans_1" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_1"></span>  
                        }
                        {
                        this.state.answerSection === "ans_1" ?
                        <p className="answer_line">A. Our website allows you to select a delivery date in the future, so we would encourage 
                        you to place your order as soon as you book your room to guarantee your slot,
                        then it’s one less thing to worry about and we will ensure your pack is in your 
                        room ready for when you arrive.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_2">
                        <p className="question_line">Q. Can I change my order once I've 
                             placed it?</p>
                        {
                        this.state.answerSection === "ans_2" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_2"></span>  
                        }
                        {
                        this.state.answerSection === "ans_2" ?
                        <p className="answer_line">A. If you wish to change your order you 
                        can notify us by chatting to our support team, by going to 
                        your order and go to CHAT.  If it’s before we have dispatched 
                        the goods to you we will be more than happy to help. If already despatched a refund can be issued however 
                        a return fee of £25 will apply.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                       <p className="question_title">Bedding Sizes</p>
                        
                        <div className="single_question_wrapper" id="ans_3">
                        <p className="question_line">Q. How do I know what size bedding to order?</p>
                        {
                        this.state.answerSection === "ans_3" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_3"></span>  
                        }
                        {
                        this.state.answerSection === "ans_3" ?
                        <p className="answer_line">A. We strongly recommend speaking to your accommodation before ordering. 
                        3/4 beds which are also often referred to as compact double/small double beds are most 
                        popular in student accommodations.  Double Bed and single bed bedding packs are also 
                        available</p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_4">
                        <p className="question_line">Q. How do I know what the size of your duvets are?</p>
                        {
                        this.state.answerSection === "ans_4" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_4"></span>  
                        }
                        {
                        this.state.answerSection === "ans_4" ?
                        <p className="answer_line">A. As standard, our bedding is available in single (3'), three quarter (4’), or Double (4’6) - We have lots of packs 
                        for all different sizes and are able to provide bedding to suit your requirements but
                         please make sure you know the size of your bed before ordering.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_5">
                        <p className="question_line">Q. What size are your pillows?</p>
                        {
                        this.state.answerSection === "ans_5" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_5"></span>  
                        }
                        {
                        this.state.answerSection === "ans_5" ?
                        <p className="answer_line">A. Our Pillows are 50 x 75cm
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_6">
                        <p className="question_line">Q. What is the difference between a 3/4 small double and double bed?</p>
                        {
                        this.state.answerSection === "ans_6" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_6"></span>  
                        }
                        {
                        this.state.answerSection === "ans_6" ?
                        <p className="answer_line">A. Three quarter is 120x190 & Double Beds are 135x190cm. The only difference between Three quarter and Double Bed packs is the fitted sheet.
                        Our Pillows are 50 x 75cm.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr> 

                        <p className="question_title">Delivery</p>
                        <div className="single_question_wrapper" id="ans_7">
                        <p className="question_line">Q.What is the cost for delivery?</p>
                        {
                        this.state.answerSection === "ans_7" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_7"></span>  
                        }
                        {
                        this.state.answerSection === "ans_7" ?
                        <p className="answer_line">A. There is no cost for your delivery this is included in the price.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_8">
                        <p className="question_line">Q. What are your delivery times?</p>
                        {
                        this.state.answerSection === "ans_8" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_8"></span>  
                        }
                        {
                        this.state.answerSection === "ans_8" ?
                        <p className="answer_line">A.We offer a free next working day delivery service. This is available for deliveries Monday to Friday.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_9">
                        <p className="question_line">Q. I am arriving at the weekend, do I need to wait until Monday for my order? </p>
                        {
                        this.state.answerSection === "ans_9" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_9"></span>  
                        }
                        {
                        this.state.answerSection === "ans_9" ?
                        <p className="answer_line">A.Most students arrive at a weekend . If you are arriving on a Saturday or Sunday, please select the Friday before as your required delivery date to ensure your goods will 
                        be waiting for you upon arrival
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>

                        <div className="single_question_wrapper" id="ans_10">
                        <p className="question_line">Q. What happens if I am not at the property when my order is delivered? </p>
                        {
                        this.state.answerSection === "ans_10" ?
                         <span onClick={this.collapseFaqSection} className="arrow up_arrow"></span>
                         : <span onClick={this.expandFaqSection} className="arrow down_arrow" id="ans_10"></span>  
                        }
                        {
                        this.state.answerSection === "ans_10" ?
                        <p className="answer_line">A.All our packages are sent on a signed for service and will be accepted by the resident concierge at your property. If you pre-order your pack before you arrive this will 
                        be available for you when you check in.
                       </p>
                        : null                
                        }
                        </div>
                        <hr></hr>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component: RoomEssentailsPage
};
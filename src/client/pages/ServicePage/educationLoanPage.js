import React, {Component} from "react";

import EducationLoanBanner from "../../../assets/graphics/servicesPage/educationLoanPage/education-service-banner.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";


class EducationLoanPage extends Component {
    state={
       eligibilityCalculator : false,
       rangeValue :{
           emiRateRange : "11.5",
           emiYearsRange : "4",
           emiAmountRange : "1000000",
           eligibilityIncomeRange : "1000000",
           eligibilityObligationRange : "500000",
           eligibilityRateRange : "10",
           eligibilityYearsRange : "3",
       },
       emiCalculatedValue : "",
       eligibileCalculatedValue : "",
       showCalculatedEligibleAmount : false, 
       showCalculatedEmiAmount : false

    }
    
    showEligibiltyCalculator = () => {
        this.setState({
            eligibilityCalculator : true,
        })
    };

    showEmiCalculator = () => {
        this.setState({
            eligibilityCalculator :false,
        })
    };

    handleRangeValue =(e) =>{
        let obj = {...this.state.rangeValue};
        obj[e.target.id] = e.target.value;
        this.setState({
            rangeValue : obj
        })
    };

    emiCalculation =() =>{
        let P = parseInt(this.state.rangeValue.emiAmountRange);
        let R = parseFloat(((this.state.rangeValue.emiRateRange / 12) / 100));
        let N = parseInt(this.state.rangeValue.emiYearsRange * 12);

        let A = P * R;
        let B = Math.pow((1 + R), N);
        let C = B - 1;

        let finalCalculatedEmi = A * (B / C);
        this.setState({
            emiCalculatedValue : Math.ceil(finalCalculatedEmi).toLocaleString(),
            showCalculatedEmiAmount : true
        })
    };

    resetEmiCalculator = () =>{
        this.setState({
            showCalculatedEmiAmount : false,
            rangeValue :{
                emiRateRange : "11.5",
                emiYearsRange : "4",
                emiAmountRange : "1000000",
            },
        })
    };

    eligibilityCalculation = () =>{
        let per = 12;
        let rate =  eval((this.state.rangeValue.eligibilityRateRange)/(per * 100));;
        let nper  = parseFloat(this.state.rangeValue.eligibilityYearsRange * 12);
        let pmt = parseFloat((this.state.rangeValue.eligibilityIncomeRange) - (this.state.rangeValue.eligibilityObligationRange));
        let fv = 0;

        let x = Math.pow(1 + rate, -nper);
        let y = Math.pow(1 + rate, nper);

        let pv_value = ( x * ( fv * rate - pmt + y * pmt )) / rate;

        this.setState({
            eligibileCalculatedValue : parseInt(pv_value).toLocaleString(),
            showCalculatedEligibleAmount : true
        })
    };


    resetEligibilityCalculator = () =>{
        this.setState({
            showCalculatedEligibleAmount : false,
            rangeValue :{
                eligibilityIncomeRange : "1000000",
                eligibilityObligationRange : "500000",
                eligibilityRateRange : "10",
                eligibilityYearsRange : "3",
            },
        })
    }
   

    render() {
        return (
            <div className="education_loan_page_wrapper">
                   <div className="education_loan_hero_section">
                    <div className="education_loan_hero_image_wrapper">
                            <img src={EducationLoanBanner}></img>
                      </div>
                </div>


                <div className="container">
                <div className="row">

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="education_loan_content_section">
                    <p className="education_loan_title"> Education Loan </p>
                    <p className="education_loan_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum

                     <br></br>
                     <br></br>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum
                     <br></br>
                     <br></br>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
                     enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
                     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
                     qui officia deserunt mollit anim id est laborum
                    </p>
                </div>
                    </div>

                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Apply Education Loan</p>
                    <ServiceForm/>
                 </div>
                 </div>
                 </div>

                 <div className="education_loan_calculator">
                    <p className="calculator_title"> Emi And Eligibility Calculator</p>
                        <button onClick={this.showEmiCalculator} className="calculator_button" id="emiButton">EMI Calculator</button>
                        <button onClick={this.showEligibiltyCalculator} className="calculator_button" id="elibilityButton">Eligibilty Calculator</button>
                        {
                            this.state.eligibilityCalculator ?
                            <div className="eligibility_emi_calculator">
                                <div className="single_input_wrapper">
                                <p>Your monthly income (₹)</p>
                                <input className="slider_field" type="range" min="1000" max="10000000" step="5000" value={this.state.rangeValue.eligibilityIncomeRange} onChange={this.handleRangeValue} id ="eligibilityIncomeRange"></input>
                                <input className="number_field" type="number" min="1000" max="10000000" step="5000" value={this.state.rangeValue.eligibilityIncomeRange} onChange={this.handleRangeValue} id ="eligibilityIncomeRange"></input>
                                </div>

                                <div className="single_input_wrapper">
                                <p>Your monthly obligations (₹)</p>
                                <input className="slider_field" type="range" min="5000" max="10000000" step="50000" id ="eligibilityObligationRange"  value={this.state.rangeValue.eligibilityObligationRange} onChange={this.handleRangeValue}></input>
                                <input  className="number_field" type="number" min="5000" max="10000000" step="50000" id ="eligibilityObligationRange"  value={this.state.rangeValue.eligibilityObligationRange} onChange={this.handleRangeValue}></input>
                                </div>

                                <div className="single_input_wrapper">
                                <p>Interset Rate (%)</p>
                                <input className="slider_field" type="range" min="1" max="50" step="0.01" id ="eligibilityRateRange"  value={this.state.rangeValue.eligibilityRateRange} onChange={this.handleRangeValue}></input>
                                <input  className="number_field" type="number" min="1" max="50" step="0.01" id ="eligibilityRateRange"  value={this.state.rangeValue.eligibilityRateRange} onChange={this.handleRangeValue}></input>
                                </div>

                                <div className="single_input_wrapper">
                                <p>Tenure in Years (in years)</p>
                                <input className="slider_field" type="range" min="1" max="30" step="0.5" id ="eligibilityYearsRange"  value={this.state.rangeValue.eligibilityYearsRange} onChange={this.handleRangeValue}></input>
                                <input  className="number_field" type="number" min="1" max="30" step="0.5" id ="eligibilityYearsRange"  value={this.state.rangeValue.eligibilityYearsRange} onChange={this.handleRangeValue}></input>
                                </div>

                                {
                                this.state.showCalculatedEligibleAmount ?
                                <p className="caluclated_amount_line">The Loan Eligible Amount : {this.state.eligibileCalculatedValue}</p>
                                : null
                                }

                                <div className="buttons_wrapper">
                                         <button className="submit_button" onClick={this.eligibilityCalculation} id="submitEmiCalculator">SUBMIT</button>
                                         <button className="reset_button"  onClick={this.resetEligibilityCalculator} id="resetEmiCalculator">RESET</button>
                                </div>
                         
                            </div>
                            : 
                            <div className="eligibility_emi_calculator">
                                 <div className="single_input_wrapper">
                                    <p>Annual Rate of Interset (%)</p>
                                    <input className="slider_field" type="range" min="1" max="50" step="0.01" id ="emiRateRange"  value={this.state.rangeValue.emiRateRange} onChange={this.handleRangeValue}></input>
                                    <input  className="number_field" type="number" min="1" max="50" step="0.01" id ="emiRateRange"  value={this.state.rangeValue.emiRateRange} onChange={this.handleRangeValue}></input>
                                   </div>

                                   <div className="single_input_wrapper"> 
                                       <p>Tenure(in years)</p>
                                       <input className="slider_field" type="range" min="1" max="30" step="0.5" id ="emiYearsRange"  value={this.state.rangeValue.emiYearsRange} onChange={this.handleRangeValue}></input>
                                       <input className="number_field" type="number" min="1" max="30" step="0.5" id ="emiYearsRange"  value={this.state.rangeValue.emiYearsRange} onChange={this.handleRangeValue}></input>
                                    </div>  

                                    <div className="single_input_wrapper"> 
                                      <p>Amount (₹)</p>
                                      <input className="slider_field" type="range" min="10000" max="10000000" step="10000" id ="emiAmountRange"  value={this.state.rangeValue.emiAmountRange} onChange={this.handleRangeValue}></input>
                                      <input  className="number_field" type="number" min="10000" max="10000000" step="10000" id ="emiAmountRange"  value={this.state.rangeValue.emiAmountRange} onChange={this.handleRangeValue}></input>
                                     </div> 
                                   
                                     {
                                        this.state.showCalculatedEmiAmount ?
                                        <p className="caluclated_amount_line">The Loan Eligible Amount : {this.state.emiCalculatedValue}</p>
                                        : null
                                    }


                                     <div className="buttons_wrapper">
                                         <button className="submit_button" onClick={this.emiCalculation} id="submitEmiCalculator">SUBMIT</button>
                                         <button className="reset_button"  onClick={this.resetEmiCalculator} id="resetEmiCalculator">RESET</button>
                                     </div>
                         
                        </div>
                        }

                    </div>  
                </div>
            </div>
        )
    }
}

export default {
    component: EducationLoanPage
};
import React, {Component} from "react";

import GurantorBanner from "../../../assets/graphics/servicesPage/gurantorPage/gurantor-service.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";

class GurantorPage extends Component {
    

    render() {
        return (
            <div className="gurantor_page_wrapper">
                   <div className="gurantor_hero_section">
                    <div className="gurantor_hero_image_wrapper">
                            <img src={GurantorBanner}></img>
                      </div>
                </div>


                <div className="container">
                <div className="row">

            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="gurantor_content_section">
                    <p className="gurantor_title"> Guarantor Service </p>
                    <p className="gurantor_description">
                    Want to stay abroad but facing a problem in finding a right guarantor 
                    there because we know most of the accommodation providers ask for a guarantor if you're unable 
                    to pay rent then guarantor will responsible for paying your rent.
                     <br></br>
                     <br></br>
                     So if you're looking for a guarantor and you can't find it then don't 
                     worry HomeInAbroad will help you in finding a right Guarantor  
                     for you. We have partnership with hello rented which gives you true and 
                     genuine guarantor on a good price and also help you in pay your rents in
                      easy installments. So go and find your genuine guarantor anywhere you want in UK,Australia and US. Contact our 
                     support team for any help and queries.
                    </p>
                </div>
                </div>
                
            <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="contact_form">
                    <p className="form_title">Find a Guarantor</p>
                    <ServiceForm/>
                 </div>
                </div>
                </div>
                </div>
            </div>
        )
    }
}

export default {
    component: GurantorPage
};
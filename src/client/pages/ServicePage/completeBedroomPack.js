import React, {Component} from "react";
import $ from "jquery";

import BedroomPack from "../../../assets/graphics/servicesPage/roomEssentialsPage/bedroom-pack.png";
import PillowSet from "../../../assets/graphics/servicesPage/roomEssentialsPage/pillow-set.jpg";
import BedSheet from "../../../assets/graphics/servicesPage/roomEssentialsPage/bed-sheet.jpg";
import ListIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/list-icon.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";



class CompleteBedroomPackPage extends Component {

    scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };
    
    render() {
        return (
            <div className="room_essentials_page_wrapper">
               <div className="roomPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={BedroomPack} alt="HomeinAbroad"/>
                    </div>
                    <div className="roomPage_hero_content_wrapper">
                        <p className="roomPage_title">Complete Bedroom Pack </p>
                        <hr></hr>
                        <p className="price_title">Price £ 95</p>
                        <button className="enquire_Button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                    </div>
                </div>

                <div className="container">
                <div className="roomPage_content_wrapper">
                    <p className="roompage_content_title"> Soft and sumptuous, our bedding will make the ideal surface to snuggle up in. </p>

                    <p className="roomPage_content"> From our warm duvets to our supportive pillows,
                     give your new Uni bedroom a cosy feel that will create the perfect environment
                      for rest and relaxation. Our exclusive bedding styles are perfect for University 
                      and will make your new home stylish straight away, all of our student bedroom set 
                      items are made from synthetic fibres so there is no need to worry about itching or irritation. </p>

                      <p className="kitchen_content_question">Sizes </p>

                    <div className="single_bedtype_wrapper">
                    <img src={ListIcon}></img>
                   <p> Single Bed</p>
                   </div>
                   <div className="single_bedtype_wrapper">
                   <img src={ListIcon}></img>
             <p>  3/4 size / Small Double / 4 foot Double Bed</p> 
                </div>
                <div className="single_bedtype_wrapper">
                <img src={ListIcon}></img>
              <p> Double Bed</p> 
              </div>
                <p className="kitchen_content_question">Delivery </p>
                    <p className="roomPage_content">Price includes next day delivery as standard (Monday to Friday), order before 14:00 for next day delivery.</p>

                    <p className="roomPage_content">Don’t settle for any essentials at University, choose our kitchen pack for ultimate convenience and value.
                    </p>

                    <p className="roomPage_content">We will deliver your pack on time to whatever location & date you choose, you can also choose the time you need delivery, so order with confidence now.
                    </p>

                    <p className="facility_content_heading">This Pack Includes</p>

                    <div className="facility_wrapper">
                        <div className="row">

                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper_small">
                                <img src={PillowSet}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">LUXURY DUVET AND PILLOW SET</p>
                                        <p className="sub_title" >The 10.5 luxury Tog duvet and 2x jumbo pillows have a 
                                        hollowfibre filling that provides a long lasting plumpness, wrapped in a beautifully 
                                        soft microfibre cover. Anti-allergen material for extra hygiene and dust protection. </p>    
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                   
                             </div>
                             </div>
                         </div>

                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper_small">
                                <img src={BedSheet}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">STYLISH DUVET SET</p>
                                        <p className="sub_title" >University inspired designs. Includes fitted sheet, duvet cover and pillow covers. Made from luxurious combed yarns, Our 50/50 polycotton, 144 thread count duvets sets are soft, strong, and easy to care for.   </p>  
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                                          
                                   
                             </div>
                             </div>
                         </div>
                   
                         </div>
                    </div>
                    <div id= "formSection" className="contact_form">
                        <ServiceForm/>
                    </div>
      
                </div>

                </div>
            </div>
        )
    }
}

export default {
    component: CompleteBedroomPackPage
};
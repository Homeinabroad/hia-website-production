import React, {Component} from "react";
import $ from "jquery";


import PPEBanner from "../../../assets/graphics/servicesPage/roomEssentialsPage/ppe-banner.png";
import ManSanitizer from "../../../assets/graphics/servicesPage/roomEssentialsPage/men-sanitizer.jpg";
import WomanSanitizer from "../../../assets/graphics/servicesPage/roomEssentialsPage/women-sanitizer.jpg";
import ListIcon from "../../../assets/graphics/servicesPage/roomEssentialsPage/mainRoomEssentials/list-icon.png";
import ServiceForm from "../../components/ServiceForm/serviceForm";



class CompletePPEKitPack extends Component {

    scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };

    render() {
        return (
            <div className="room_essentials_page_wrapper">
               <div className="ppeKit_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={PPEBanner} alt="HomeinAbroad"/>
                    </div>
                    <div className="ppeKit_hero_content_wrapper">
                        <p className="ppeKit_title">Complete PPE Kit </p>
                        <hr></hr>
                        <button className="enquire_Button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                       {/* <p className="price_title">Price £ 95</p> */}
                    </div>
                </div>

                <div className="container">
                <div className="ppeKit_content_wrapper">
                    <p className="ppeKit_content_title"> Order our Personal Protective Equipment (PPE) Amenity pack, or hand sanitiser products to help keep you safe.</p>
                   

                   <div className="single_ppeKit_wrapper">
                       <div className="heading_wrapper">
                   <img src={ListIcon}></img>
                   <p className="ppeKit_content_heading">PPE Amenity Kit contains</p>
                   </div>
                    <p className="ppeKit_heading_content">  4 x sachets of anti bac wipes 4 x civilian non-medical facemask 4 x 
                        pairs of vinyl non-medical 
                 gloves 1 x 50ML anti bac gel All packed in 1 x reusable organza bag.</p> 
                </div>

                <div className="single_ppeKit_wrapper">
                <div className="heading_wrapper">
                <img src={ListIcon}></img>
              <p className="ppeKit_content_heading"> 15ml Pocket Size Hand Sanitiser</p> 
              </div>
              <p className="ppeKit_heading_content">  We also provide luxury hand sanitiser pocket size (15ml) spray. 
                  These are provided by Sanscent a luxury brand who infuse alcohol hand sanitiser 
                  with Creed Aventus scents. Highly effective surgical grade proven formula is fully
                   tested and proven to be antibacterial, virucidal and yeasticidal. Kills 99.999% of 
                   many common germs. High alcohol content - 80% for
                   quick effective hand sanitisation and moisturising to skin.</p> 
              </div>


              <div className="single_ppeKit_wrapper">
              <div className="heading_wrapper">
                <img src={ListIcon}></img>
              <p className="ppeKit_content_heading"> DELIVERY</p> 
              </div>
              <p className="ppeKit_heading_content"> Price includes next day delivery as standard (Monday to Friday),
                   order before 14:00 for next day delivery.</p> 
              </div>

                    <p className="facility_content_heading">This Pack Includes</p>

                    <div className="facility_wrapper">
                        <div className="row">

                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper">
                                <img src={ManSanitizer}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">MEN'S HAND SANITISER</p>
                                        <p className="facility_description" >SANSCENT - LUXURY POCKET HAND SANITISER </p>
                                        <p className="facility_description" >Stay safe smell amazing. Credit card style antibacterial hand sanitiser (15ml).   </p>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                             </div>
                             </div>
                         </div>

                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="single_facility_wrapper">
                                <img src={WomanSanitizer}></img>
                                 <div className="facility_content_wrapper">
                                      <p className="facility_title">WOMEN'S HAND SANITISER</p>
                                        <p className="facility_description">SANSCENT - LUXURY POCKET HAND SANITISER</p>
                                        <p className="facility_description" >Stay safe smell amazing. Credit card style antibacterial hand sanitiser (15ml).  
                                        </p>
                                        <button className="enquire_button" onClick={this.scrollToForm} id="enquireButton">Enquire Now</button>
                             </div>
                             </div>
                         </div>
                   
                         </div>
                    </div>

                    <div id= "formSection" className="contact_form">
                        <ServiceForm/>
                    </div>
      
                </div>

                </div>
            </div>
        )
    }
}

export default {
    component: CompletePPEKitPack
};
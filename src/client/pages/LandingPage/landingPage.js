import React, {Component} from "react";
import LeadsBannerImage from '../../../assets/graphics/landingPage/landing-page-banner.jpg';
import LeadsGenerationForm from "../../components/LeadGenerationForm/leadGenerationForm";
import RoomIcon from "../../../assets/graphics/landingPage/RoomIconOriginal.jpg";
import BookRoomIcon from "../../../assets/graphics/landingPage/BookRoomIcon.jpg";
import ContractSignIcon from "../../../assets/graphics/landingPage/ContractSign.jpg";
import TransparencyIcon from "../../../assets/graphics/landingPage/search.svg";
import PriceMatchIcon from "../../../assets/graphics/landingPage/pricematch.svg";
import HomeGuaranteIcon from "../../../assets/graphics/landingPage/homeguarante.svg";
import CheckboxIcon from "../../../assets/graphics/landingPage/checkbox.svg";
import OfferIcon from "../../../assets/graphics/landingPage/offer.png";

class LandingPage extends Component {
    render() {
        return (
            <div className="landing_page_wrapper">
                <div className="landingPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={LeadsBannerImage} alt="HomeinAbroad"/>
                    </div>
                    <div className="content_wrapper">
                        <p className="content_title">Home Away from Home</p>
                        <p className="content_details">Studios, Flats & Appartments</p>
                        <div className="offer_wrapper">
                            <img src={OfferIcon}/>
                            <p> Get GBP/AUD 200 off </p></div>

                        <div className="offer_wrapper">
                            <img src={OfferIcon}/>  <p>Free Bank Account </p></div>
                        <div className="offer_wrapper">
                            <img src={OfferIcon}/>   <p> Free Pick-Up from Airport</p></div>
                        <div className="offer_wrapper">
                            <img src={OfferIcon}/>     <p> Cineworld Unlimited World</p></div>


                        <ul className="quality_content">
                            <li><img src={CheckboxIcon}/>Easy Cancellation</li>
                            <li><img src={CheckboxIcon}/>No Booking Charges</li>
                            <li><img src={CheckboxIcon}/>No University <spna>/</spna> Visa No Pay</li>
                        </ul>

                    </div>
                    <div className="getintouch_wrapper">
                        <p className="getintouch_title">Assisting you for FREE</p>
                        <div id="leadsFormSection" className="leads_generation_form_wrapper">
                            <LeadsGenerationForm/>
                        </div>

                    </div>
                </div>
                <div className="container-fluid">
                    <div className="whyus_wrapper">
                        <div className="whyus_background">
                            <p className="whyus_title">Hassle- Free Booking with your Trusted Uni-Experts</p>
                            <p className="whyus_subheading"> Why Us ?</p>
                        </div>


                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src={TransparencyIcon} alt="Select your room"/>
                                <div className="whyus_description">
                                    <p className="whyus_content">Transparency</p>
                                    <p className="content_description">You will get your accommodation on time. There
                                        will be no waiting period for you. </p>
                                </div>


                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src={PriceMatchIcon} alt="Select your room"/>
                                <div className="whyus_description">
                                    <p className="whyus_content">Price Match</p>
                                    <p className="content_description">Find a lower Price and we will match it.<br/>No
                                        question asked.</p>
                                </div>

                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img src={HomeGuaranteIcon} alt="Select your room"/>
                                <div className="whyus_description">

                                    <p className="whyus_content">Perfect Home Guarantee</p>
                                    <p className="content_description"> Select the best accommodation, providing
                                        safe & cozy living experience.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="howweworks_wrapper">
                        <div className="howitworks_heading">
                            <p>How it <span>works</span></p></div>
                        <div className="row">
                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div className="image_wrapper">
                                    <img src={RoomIcon} alt="Select your room"/>
                                </div>

                                <div className="howitworks_content">
                                    <p className="howitworks_subheading"> Select your room </p>
                                    <hr></hr>
                                    <p className="howitworks_description">Enter your University, locality or property
                                        name and choose between various options
                                        based on your budget preferences, amenity requirements etc.</p>
                                </div>
                            </div>


                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                                <div className="image_wrapper ">
                                    <img src={BookRoomIcon} alt="Click on Book Now"/>
                                </div>


                                <div className="howitworks_content">
                                    <p className="howitworks_subheading"> Click on Book Now </p>
                                    <hr></hr>
                                    <p className="howitworks_description">When you find your room of choice, make a
                                        request to book and
                                        fill in the required details after which our booking experts will call you and
                                        guide you on the next steps, all for free.
                                        Alternatively, you can also put down the deposit on the portal itself.</p>
                                </div>
                            </div>


                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div className="image_wrapper">
                                    <img src={ContractSignIcon} alt="Sign Contract"/>
                                </div>
                                <div className="howitworks_content">
                                    <p className="howitworks_subheading"> Sign Contract</p>
                                    <hr></hr>
                                    <p className="howitworks_description">Digitally Sign your contract on our portal and
                                        you are ready to move it.
                                        You will pay your rent directly to the property.
                                        The services that we provide are completely free of cost.</p>
                                </div>
                            </div>


                        </div>
                        <div className="howitworks_finish">
                            <p className="finish_heading">Here you go! </p>
                            <p className="finish_subheading">Now the room is yours</p>
                        </div>
                    </div>


                    <div className="query_wrapper">
                        <div className="query_heading">
                            <p> Any query?</p>
                        </div>
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <p className="email_section"> Email: support@homeinabroad.com</p>

                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                <p className="call_section"> Call: +44-2086388957 <span>/ </span>+61-280069159 </p>

                            </div>


                        </div>
                    </div>
                    <div className="getintouch_form">
                        <div className="getintouch_wrapper">
                            <p className="getintouch_title">Assisting you for FREE</p>


                            <div id="leadsFormSection" className="leads_generation_form_wrapper">
                                <LeadsGenerationForm/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component: LandingPage
};
import React, { Component } from "react";
import { connect } from "react-redux";
import moment from 'moment';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

import {fetchPropertyDetails} from "../../actions";
import { type } from "jquery";

class RoomTypeSection extends Component {
    state = {
        selectedRoomType : {
            id : "",
            images : []
        },
        showTypeGallery : false,
        showSelectedRateOverview : false,
        selectedPropertyDataForBooking : null,
        selectedCategoryType : this.props.propertyDetails.roomCategories[0].id,
        expandedRoomType : this.props.propertyDetails.roomCategories[0].roomTypes[0].id

    };

    handleSelectedRoomTypeImage = (event, typeId, typeImages) => {
        this.setState({
            selectedRoomType : {
                id : typeId,
                images : typeImages
            },
            showTypeGallery : true
        })
    };

    handleCategoryTypeSelection =(e)=>{
        this.setState({
            selectedCategoryType : parseInt(e.target.id)
        },)
    }

    toggleTypeGallery = () => {
        this.setState({
            showTypeGallery : !this.state.showTypeGallery
        })
    };

    handleSelectedRateForBooking = (e, chosenRate) => {
        this.setState({
            showSelectedRateOverview : true,
            selectedPropertyDataForBooking : chosenRate
        }, () => {
            sessionStorage.setItem("selectedPrice", JSON.stringify(chosenRate));
        })
    };

    toggleSelectedRatePopupOverview = () => {
        this.setState({
            showSelectedRateOverview : !this.state.showSelectedRateOverview
        })
    };

    closePopUp = () => {
         this.setState({
           showSelectedRateOverview : false
         })
     };

    handleSelectedRoomTypeForView = (e) => {
        this.setState({
            expandedRoomType : parseInt(e.target.id)
        })
    };

    collapseSelectedRoomTypeForView = () =>{
        this.setState({
            expandedRoomType:false
        })
    }

    render() {
        return(
            <div className="room_types_section_wrapper">
                {
                    this.state.showTypeGallery ?
                        <div className="room_type_gallery">
                            <div onClick={this.toggleTypeGallery} className="close_type_gallery">X</div>
                            {
                                this.state.selectedRoomType.images.map((image, i) => {
                                    return(
                                        <div className="single_image_wrapper" key={i}>
                                            <img src={image.image} alt={i} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    : null
                }

                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p className="section_headline">Select your perfect room in <b>{this.props.propertyDetails.name}</b></p>
                  
                                 <div className="roomcategory_selection_wrapper">
                            {
                                this.props.propertyDetails.roomCategories.map((category,i) => {
                                    return(
                                           <button className={this.state.selectedCategoryType === category.id ? "selected_category" : ""} onClick={this.handleCategoryTypeSelection} id={category.id}  key={i}>{category.name}</button>
                                           )
                                })
                            }
                           
                              {
                                  this.props.propertyDetails.roomCategories.map((categoryRoom,i)=>{
                                 return(   
                                 this.state.selectedCategoryType === categoryRoom.id ? 
                                 <div id={categoryRoom.id} key={i}>
                                <p className="category_description">{ ReactHtmlParser(categoryRoom.description) }
                                </p>
                             {
                                        categoryRoom.roomTypes.map((type, i) => {
                                            return(
                                                <div  id={type.id} className="single_accordion_headline" key={i}>
                                                    <p onClick={this.handleSelectedRoomTypeForView} id={type.id}>{type.title}</p>
                                                    <p onClick={this.handleSelectedRoomTypeForView} id={type.id}>{
                                                    type.bedPricing.length === 0 ||  type.bedPricing.length === 1 ?  type.bedPricing.length + " " + "Option Available" 
                                                   : type.bedPricing.length + " " + "Options Available"}</p>
                                                    {
                                                    this.state.expandedRoomType === type.id ?
                                                    <span onClick={this.collapseSelectedRoomTypeForView} id={type.id} className="arrow up_arrow"></span>
                                                    : <span onClick={this.handleSelectedRoomTypeForView} id={type.id} className="arrow down_arrow"></span>
                                                
                                                     }

                                                    {
                                                        this.state.expandedRoomType === type.id ?
                                                            <div key={i} className="type_config_wrapper">
                                                                <div className="roomtype_image_definition_offer_wrapper">
                                                                    <div className="room_type_hero_image">
                                                                        <img src={type.images[0].image} alt={type.title} />
                                                                        { 
                                                                            type.images.length >=2 ?
                                                                            <button onClick={(event) => this.handleSelectedRoomTypeImage(event, type.id, type.images)} className="btn btn-primary">
                                                                               View {type.images.length ===2 ?
                                                                               type.images.length - 1 + " " + "more photo" 
                                                                            :  type.images.length - 1 + " " + "more photos"  }</button>
                                                                            :  null
                                                                        }
                                                                        <p className="room_area"><span>Area: </span> {type.area}</p>
                                                                       
                                                                    </div>

                                                                    <div className="roomtype_definition">
                                                                        {
                                                                            type.facilities.map((facility, i) => {
                                                                                return(
                                                                                    <li key={i}><img src={facility.logo} alt={facility.title} title={facility.title} />{facility.title}</li>
                                                                                )
                                                                            })
                                                                        }
                                                                    </div>

                                                                    <div className="roomtype_offer">
                                                                        {
                                                                            type.typeOffers.map((offer, i) => {
                                                                                if(moment(offer.validity_date).isAfter(moment())) {
                                                                                    return(
                                                                                        <div key={i} className="single_type_offer">{offer.message}</div>
                                                                                    )
                                                                                }
                                                                            })
                                                                        }
                                                                    </div>
                                                                </div>
                                                                {
                                                                    type.bedPricing.map((price, i) => {
                                                                        let selectedRate = {
                                                                            citySlug : this.props.propertyDetails.city_slug,
                                                                            propertyId : this.props.propertyDetails.id,
                                                                            propertyName : this.props.propertyDetails.name,
                                                                            roomCategoryName : categoryRoom.name,
                                                                            roomTypeName : type.title,
                                                                            roomTypeId : type.id,
                                                                            bedPrice : price.bed_discounted_price === 0 ? price.display_price_bed : price.display_discount_price_bed,
                                                                            checkInDate : price.room_checkin_date,
                                                                            checkOutDate : price.room_checkout_date,
                                                                            tenancyValue : parseInt(price.tenancy_duration),
                                                                            tenancyUnit : price.tenancy_duration_unit,
                                                                            bedNumeralPriceValue : price.bed_discounted_price === 0 ? price.bed_price : price.bed_discounted_price,
                                                                            tenancyId : price.id
                                                                        };
                                                                        return(
                                                                            <div className="rate_config_wrapper">
                                                                                <div className="single_rate_wrapper">
                                                                                    <p>Move in {moment(price.room_checkin_date).format('MMM d, YYYY')} <b>till</b> {moment(price.room_checkout_date).format('MMM d, YYYY')} | For {price.tenancy_duration} {price.tenancy_duration_unit} </p>
                                                                                    <p>{price.bed_discounted_price === 0 ? price.display_price_bed : price.display_discount_price_bed}</p>
                                                                                    <button id={type.id} onClick={(e) => this.handleSelectedRateForBooking(e, selectedRate)} className="btn btn-primary">RESERVE ROOM</button>
                                                                                </div>
                                                                            </div>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        :  null
                                                        
                                                    }


                                                </div>
                                            )
                                        })
                            }
                            </div>
                            :null
                                   
                            )
                        })
                              }
                            </div >
                        </div>

                        {
                            this.state.showSelectedRateOverview ?
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="selected_rate_for_reserve">
                                        
                                        <p>Room Type: <span>{this.state.selectedPropertyDataForBooking.roomTypeName}</span></p>
                                        <p>Price: <span>{this.state.selectedPropertyDataForBooking.bedPrice}</span></p>
                                        <p>Tenancy: <span>{moment(this.state.selectedPropertyDataForBooking.checkInDate).format('MMM d, YYYY')} till {moment(this.state.selectedPropertyDataForBooking.checkOutDate).format('MMM d, YYYY')} | For {this.state.selectedPropertyDataForBooking.tenancyValue} {this.state.selectedPropertyDataForBooking.tenancyUnit}</span></p>
                                        <div className="buttons_wrapper">
                                        <button onClick={this.closePopUp} className="btn btn-primary">Cancel</button>
                                        <a href={"/" + this.props.propertyDetails.country_slug + "/" + this.props.propertyDetails.city_slug + "/" + this.props.propertyDetails.slug + "/book-now"}>
                                            <button className="btn btn-primary">RESERVE YOUR ROOM</button>
                                        </a>
                                     
                                        </div>  
                                    </div>
                                </div>
                                : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {propertyDetails: state.propertyDetails};
}

export default connect(mapStateToProps, {fetchPropertyDetails})(RoomTypeSection);
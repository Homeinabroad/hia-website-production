import React, { Component } from "react";
import { connect } from "react-redux";
import ReactHtmlParser from 'react-html-parser';

import { fetchPropertyDetails } from "../../actions";

class PropertyFacilitiesSection extends Component {
    render() {
        return(
            <div className="property_facilities_section_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="property_facilities_wrapper">
                                <div className="single_facility_type_wrapper">
                                    <h6>Amenities</h6>
                                    <ul>
                                        {
                                            this.props.propertyDetails.amenities.map((amenity, i) => {
                                                return(
                                                    <li key={i}><span><img src={amenity.logo} alt={amenity.title} /></span>{amenity.title}</li>
                                                )
                                            })
                                        }
                                    </ul>
                                </div>

                                <div className="single_facility_type_wrapper">
                                    <h6>Rent Inclusives</h6>
                                    <ul>
                                        {
                                            this.props.propertyDetails.rent_inclusions.map((rent, i) => {
                                                return(
                                                    <li key={i}><span><img src={rent.logo} alt={rent.title} /></span>{rent.title}</li>
                                                )
                                            })
                                        }
                                    </ul>
                                </div>

                                <div className="single_facility_type_wrapper">
                                    <h6>Safety and Security</h6>
                                    <ul>
                                        {
                                            this.props.propertyDetails.safety_inclusions.map((safety, i) => {
                                                return(
                                                    <li key={i}><span><img src={safety.logo} alt={safety.title} /></span>{safety.title}</li>
                                                )
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="payment_rules_wrapper">
                                <h6>Why you should book {this.props.propertyDetails.name}</h6>
                                <div>
                                    {ReactHtmlParser(this.props.propertyDetails.why_book)}
                                </div>
                            </div>

                            <div className="payment_rules_wrapper">
                                <h6>Payment Rules</h6>
                                <div>
                                    {ReactHtmlParser(this.props.propertyDetails.payment_procedure)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {propertyDetails: state.propertyDetails};
}

export default connect(mapStateToProps, {fetchPropertyDetails})(PropertyFacilitiesSection);
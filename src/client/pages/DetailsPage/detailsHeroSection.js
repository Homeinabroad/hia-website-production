import React, {Component} from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import moment from 'moment';
import ReactHtmlParser from 'react-html-parser';
import $ from "jquery";

import {fetchPropertyDetails} from "../../actions";
import CoronaIcon from "../../../assets/graphics/details/corona-icon.svg";

class DetailsHeroSection extends Component {
    state = {
        showGallery: false,
        expandPropertyDescription: false,
        showCancellationPolicy: false,
    };

    togglePropertyGallery = () => {
        this.setState({
            showGallery: !this.state.showGallery
        })
    };

    openCancellationPolicy = () => {
        this.setState({
            showCancellationPolicy: !this.state.showCancellationPolicy
        })
    };

    expandDescription = (propertyDescription) => {
        if (propertyDescription === false) {
            this.setState({
                expandPropertyDescription: true
            }, () => {
                $('html, body').stop(true, false).animate({
                    scrollTop: $(".detail_content").offset().top - 100
                }, 1000);
            });
        } else {
            this.setState({
                expandPropertyDescription: false
            }, () => {
                $('html, body').stop(true, false).animate({
                    scrollTop: $(".detail_content").offset().top - 100
                }, 1000);
            });
        }
    };

    render() {
        let offersArray = [];
        this.props.propertyDetails.propertyOffers.map((offer) => {
            if (moment(offer.validity_date).isAfter(moment())) {
                return (
                    offersArray.push(offer.message)
                )
            }
        });

        return (
            <div className="hero_section_details_page">
                {
                    this.state.showGallery ?
                        <div className="property_gallery">
                            <div onClick={this.togglePropertyGallery} className="close_gallery_button">X</div>
                            {
                                this.props.propertyDetails.images.map((image, i) => {
                                    return (
                                        <div key={i} className="single_image_wrapper">
                                            <img src={image.image} alt={i}/>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        : null
                }
                <div className="property_upfront_images_wrapper">
                    <div className="column_first_image">
                        <img src={this.props.propertyDetails.images[0].image} alt={this.props.propertyDetails.name}
                             className="property_upfront_images"/>
                    </div>

                    <div className="column_second_image">
                        <img src={this.props.propertyDetails.images[1].image} alt={this.props.propertyDetails.name}
                             className="property_upfront_images"/>
                        <img src={this.props.propertyDetails.images[2].image} alt={this.props.propertyDetails.name}
                             className="property_upfront_images"/>
                        <button onClick={this.togglePropertyGallery} className="btn btn-primary">View more photos
                        </button>
                    </div>

                    <div className="property_overview_and_offer_wrapper">
                        <div className="property_overview_wrapper">
                            <h2>{this.props.propertyDetails.name}</h2>
                            <p>{this.props.propertyDetails.address}</p>
                            <p>starts
                                at <span>{this.props.propertyDetails.price_unit + this.props.propertyDetails.min_bed_pricing + "/Week"}</span>
                            </p>
                        </div>

                        <p className="distance">Distance from City
                            Centre: <span>{this.props.propertyDetails.distance_from_city_center.toFixed(2)}&nbsp;miles</span>
                        </p>
                        <p className="cancellation_policy" onClick={this.openCancellationPolicy}>
                            <img src={CoronaIcon} alt="COVID-19" />
                            COVID-19 Cancellation Policy
                        </p>
                        {
                            this.state.showCancellationPolicy ?
                                <div className="policy_wrapper">
                                    <div className="close_policy" onClick={this.openCancellationPolicy}>X</div>
                                    <div className="policy_content_wrapper">
                                        <p className="policy_title"> Covid-19 Cancellation Policy </p>
                                        <p className="policy_content">
                                            Flexible Check In - Let us know if your Uni Term dates change and you
                                            won't be charged for
                                            weeks you don't need.</p>
                                        <li> 2020.03.13 - 2020.08.31</li>
                                        <p className="policy_terms">
                                            Terms & Conditions: This offer applies to all existing customers who
                                            booked and have
                                            already completed a Tenancy Agreement for a room with us, and all new
                                            customers who complete
                                            a booking and complete a Tenancy Agreement for 2020/21 on or before 31st
                                            August 2020. To be
                                            eligible to benefit from this promotion the original 2020/21 Tenancy
                                            Agreement must be for a
                                            minimum tenancy length of 34 weeks, with a tenancy check in date on or
                                            before 31st October 2020.
                                            This promotion excludes all short stays and/or semester stays. In order
                                            to benefit from this
                                            promotion a customer’s university or higher education institute (HEI)
                                            must delay the start of
                                            the 2020/21 Academic Year for the customer’s course. In the event that
                                            they do so we will be
                                            willing to defer the check in date of that Tenancy Agreement and credit
                                            the customer’s account
                                            for the unused complete calendar weeks of the tenancy, up until the
                                            earlier of (1) a period of one
                                            calendar week before the new Academic Year start date, (2) the 31st
                                            October 2020, subject always to
                                            the terms set out in this promotion. This offer also applies to
                                            customers whose course starts
                                            initially online only. You can apply for this offer if the face to face
                                            teaching for your course
                                            has a delayed start date. We will continue to monitor university and HEI
                                            Academic Year start dates
                                            and may review or amend this offer accordingly. If a university or HEI
                                            delays the start of the
                                            Academic Year and a customer wishes to delay their check in date under
                                            this promotion they will
                                            need to contact us by email on or before 31st August 2020. Having
                                            received contact from a customer
                                            we will make a web-form available for the customer to complete on or
                                            before 7th September 2020 to
                                            inform us of their request for a new check in date in line with the
                                            above. We will then cross
                                            check this information against their Tenancy Agreement and HEI Academic
                                            Year dates before
                                            applying the promotion. The customer’s account will be adjusted to
                                            reflect the new check in
                                            date and any future payment changes once the customer has checked into
                                            their accommodation f
                                            or the 2020/21 Academic Year. Customers who have paid for the whole of
                                            their 2020/21 tenancy
                                            in full before they check in will receive a refund for the unused weeks,
                                            after they have checked in.
                                        </p>
                                    </div>
                                </div>
                                : null
                        }

                        {
                            offersArray.length > 0 ?
                                offersArray.map((offer, i) => {
                                    return (
                                        <div className="property_offer_wrapper">
                                            <p>EXCLUSIVE OFFERS</p>
                                            <ul>
                                                <li>{offer}</li>
                                            </ul>
                                        </div>
                                    )
                                }) : null
                        }


                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="property_description_wrapper">
                                <h1> {this.props.propertyDetails.heading1}</h1>
                                <div className="property_detail">
                                    <p className={this.state.expandPropertyDescription
                                        ? "expandedHeight detail_content"
                                        : "collapsedHeight detail_content"}>

                                        {ReactHtmlParser(this.props.propertyDetails.description)} </p>
                                    {
                                        this.state.expandPropertyDescription ?
                                            <h6 className="load_more"
                                                onClick={() => this.expandDescription(this.state.expandPropertyDescription)}>
                                                <b>Read Less </b></h6>
                                            :
                                            <h6 className="load_more"
                                                onClick={() => this.expandDescription(this.state.expandPropertyDescription)}>
                                                <b>Read More </b></h6>

                                    }

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {propertyDetails: state.propertyDetails};
}

export default withRouter(connect(mapStateToProps, {fetchPropertyDetails})(DetailsHeroSection));
import React, { Component } from "react";

import webConfig from '../../../../webConfig.json';

class NearbyPlacesSection extends Component {
    render() {
        return(
            <div className="nearby_places_section">
                <div className="container">
                    <div className="show_desktop row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>Universities nearby to IQ Hammersmith </p>
                            <table cellPadding="6" cellSpacing="6">
                                <th>University Name</th>
                                <th>Distance</th>
                                <th>Walking Time</th>
                                <th>Transit Time by bus</th>

                                <tr>
                                    <td>King's College London (KCL)</td>
                                    <td>3.2 miles</td>
                                    <td>31 minutes</td>
                                    <td>16 minutes</td>
                                </tr>

                                <tr>
                                    <td>King's College London (KCL)</td>
                                    <td>3.2 miles</td>
                                    <td>31 minutes</td>
                                    <td>16 minutes</td>
                                </tr>

                                <tr>
                                    <td>King's College London (KCL)</td>
                                    <td>3.2 miles</td>
                                    <td>31 minutes</td>
                                    <td>16 minutes</td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
            
                <div className="show_mobile nearby_uni_wrapper_mobile">
                    <p>Universities nearby to IQ Hammersmith</p>
                    <div className="uni_cards_wrapper">
                        <div className="single_uni_wrapper">
                            <p>University of Canterburry</p>
                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/distance.svg`} />
                                <span>2.1 mi</span>
                            </div>

                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/walk.svg`} />
                                <span>16 mins</span>
                            </div>

                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/car.svg`} />
                                <span>8 mins</span>
                            </div>
                        </div>

                        <div className="single_uni_wrapper">
                            <p>Auckland University of technology</p>
                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/distance.svg`} />
                                <span>2.1 mi</span>
                            </div>

                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/walk.svg`} />
                                <span>16 mins</span>
                            </div>

                            <div>
                                <img src={`${webConfig.siteURL}/assets/graphics/details/car.svg`} />
                                <span>8 mins</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NearbyPlacesSection;
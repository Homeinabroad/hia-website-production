import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import endPointConfig from "../../endpoints/index";
import CountryCodeDropdown from "../../components/CountryCode/CountryCode";
import {fetchPropertyDetails} from "../../actions";
const axios = require('axios');

class ContactForm extends Component {
    state = {
        universityList : [],
        userData : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            message : ""
        },
        universityName : "",
        contactCountryCode: "",
        errorMessage : "",
        disableButton : false
    };

    componentDidMount() {
        axios.get(endPointConfig.cityUniversities + this.props.propertyDetails.city_slug)
        .then((res) => {
            let uniArray = [];
            res.data.map((uni) => {
                return(
                    uniArray.push({name : uni.name, id : uni.id})
                )
            })
            this.setState({
                universityList : uniArray
            })
        })
    }

    handleFormInputValueChanges = (e) => {
        let obj = this.state.userData;
        obj[e.target.id] = e.target.value;
        this.setState({
            userData : obj
        })
    };

    handleUniversitySelection = (e) => {
        this.setState({
            universityName : e.target.value
        })
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

    validateContactForm = () => {
        if(this.state.userData.firstName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your first name.",
                disableButton: false
            })
            return false;
        }
        else if(this.state.userData.lastName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your last name.",
                disableButton: false
            })
            return false;
        }
        else if(this.state.userData.email.trim() === "") {
            this.setState({
                errorMessage : "Please enter your email address.",
                disableButton: false
            })
            return false;
        }
        else if(this.state.userData.contactNumber.trim() === "") {
            this.setState({
                errorMessage : "Please enter your contact number.",
                disableButton: false
            })
            return false;
        }
        else if(this.state.userData.message.trim() === "") {
            this.setState({
                errorMessage : "Please enter your message.",
                disableButton: false
            })
            return false;
        }
        else if(this.state.universityName.trim() === "") {
            this.setState({
                errorMessage : "Please select your university.",
                disableButton: false
            })
            return false;
        }
        else return true;
    };

    submitUserData = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : "",
            disableButton : true
        });
        if(this.validateContactForm() === true) {
            let userDataBodyObj = {
                first_name : this.state.userData.firstName.trim(),
                last_name : this.state.userData.lastName.trim(),
                email : this.state.userData.email.trim().toLowerCase(),
                contact_number : this.state.contactCountryCode + "-" + this.state.userData.contactNumber.trim(),
                lead_source : "Property Details Page - Contact - Website",
                message : this.state.userData.message.trim(),
                budget : "NA",
                university : this.state.universityName,
            };
            axios({
                method: 'POST',
                url: endPointConfig.leadGeneration,
                data: userDataBodyObj,
                headers: {'Content-Type': 'application/json'}
            })
            .then((res) => {
                if(res.status === 201) {
                    window.location.assign("/details-contact-us/thank-you");
                }
                else {
                    this.setState({
                        errorMessage : "Oops! Something went wrong. Please try again.",
                        disableButton : false,
                    })
                }
            })
        }
    }

    render() {
        let universityDropdown = (
            this.state.universityList.length > 0 ?
                this.state.universityList.map((uni, i) => {
                    return(
                        <option key={i} value={uni.name}>{uni.name}</option>
                    )
                }) : null
        );
        return(
            <div className="contact_form_section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-9 col-sm-9 col-xs-12">
                            <div className="contact_form_wrapper">
                                <p>Please enter your details to get personalised accommodation</p>
                                <form onSubmit={this.submitUserData}>
                                    <div>
                                        <div className="single_field_wrapper">
                                            <label>FIRST NAME</label>
                                            <input type="text" id="firstName" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>

                                        <div className="single_field_wrapper">
                                            <label>LAST NAME</label>
                                            <input type="text" id="lastName" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                    </div>

                                    <div>
                                        <div className="single_field_wrapper">
                                            <label>EMAIL ADDRESS</label>
                                            <input type="email" id="email" required={true} onChange={this.handleFormInputValueChanges} />
                                        </div>
                                        <div className="single_field_wrapper">
                                            <label>CONTACT NUMBER</label>
                                            <div className="contact_number_wrapper">
                                                <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)}/>
                                                <input type="number" id="contactNumber" required={true} onChange={this.handleFormInputValueChanges} />
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div className="select_field_wrapper">
                                            <label>UNIVERSITY</label>
                                            <select required={true} onChange={this.handleUniversitySelection}>
                                                <option disabled hidden selected>SELECT YOUR UNIVERSITY</option>
                                                {universityDropdown}
                                            </select>
                                        </div>
                                    </div>

                                    <div className="textarea_field_wrapper">
                                        <label>MESSAGE</label>
                                        <textarea id="message" required={true} onChange={this.handleFormInputValueChanges}></textarea>
                                    </div>
                                    {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                                    <button disabled={this.state.disableButton} className="btn btn-primary">GET IN TOUCH</button>
                                </form>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                            <div className="contact_details_wrapper">
                                <p>You can also:</p>
                                <p>Call us on</p>
                                <p>+44-2086388957 <span>/ </span>+61-280069159</p>
                                <p>or</p>
                                <p>Mail us at</p>
                                <p>support@homeinabroad.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {propertyDetails: state.propertyDetails};
}

export default withRouter(connect(mapStateToProps, {fetchPropertyDetails})(ContactForm));
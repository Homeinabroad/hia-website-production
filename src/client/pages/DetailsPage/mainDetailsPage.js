import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { fetchPropertyDetails } from "../../actions";
import DetailsHeroSection from "./detailsHeroSection";
import PropertyFacilitiesSection from "./propertyFacilitiesSection";
import RoomTypesSection from "./roomTypeSection";
import NearbyPlacesSection from "./nearbyPlacesSection";
import ContactSection from "./contactFormSection";

class MainDetailsPage extends Component {
    head = () => {
        return (
            <Helmet>
                <title>{this.props.propertyDetails.meta_title}</title>
                <meta name="description" content={this.props.propertyDetails.meta_description}/>
                <meta name="keywords" content={this.props.propertyDetails.meta_keywords}/>
                <link rel="canonical" href={"https://www.homeinabroad.com" + this.props.location.pathname}/>
            </Helmet>
        )
    };

    render() {
        return(
            <div className="details_page">
                {this.head()}
                <DetailsHeroSection />
                <PropertyFacilitiesSection />
                <RoomTypesSection />
                {/*<NearbyPlacesSection />*/}
                <ContactSection />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { propertyDetails: state.propertyDetails };
}

function loadData(store) {
    return store.dispatch(fetchPropertyDetails());
}

export default {
    loadData,
    component: withRouter(connect(mapStateToProps, { fetchPropertyDetails })(MainDetailsPage))
};
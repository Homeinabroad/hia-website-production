import React, {Component} from "react";

import OnTimeBookingIcon from '../../../assets/graphics/homePage/on-time-booking.svg';
import PerfectHomeIcon from '../../../assets/graphics/homePage/perfect-home.svg';
import PriceMatchIcon from '../../../assets/graphics/homePage/price-match.svg';
import TransparencyIcon from '../../../assets/graphics/homePage/transparency.svg';

class UspSection extends Component {
    render() {
        return (
            <div className="usp_section_wrapper">
                <div className="container">
                    <div className="row">
                        <h2 className="section_headline">THE HOMEINABROAD DIFFERENCE</h2>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="single_usp_wrapper">
                                <img src={OnTimeBookingIcon} alt="On Time Booking"/>
                                <div className="usp_explanation">
                                    <p>On-Time Booking</p>
                                    <p>
                                        You will get your accommodation on time.
                                        There will be no waiting period for you.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="single_usp_wrapper">
                                <img src={PerfectHomeIcon} alt="Perfect Home Guarantee" />
                                <div className="usp_explanation">
                                    <p>Perfect Home Guarantee</p>
                                    <p>
                                        Select the best accommodation, providing safe & cozy
                                        living experience.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="single_usp_wrapper">
                                <img src={PriceMatchIcon} alt="Price Match" />
                                <div className="usp_explanation">
                                    <p>Price Match</p>
                                    <p>
                                        Find a lower Price and we will match it.
                                        No question asked. *
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div className="single_usp_wrapper">
                                <img src={TransparencyIcon} alt="Transparency" />
                                <div className="usp_explanation">
                                    <p>Transparency</p>
                                    <p>
                                        You pay the rent, the whole rent, and nothing but
                                        the rent - we don't do commissions, booking fees,
                                        or hidden charges.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UspSection;
import React, { Component } from "react";

import BrilliantBuildingImage from "../../../assets/graphics/homePage/brilliant-building.png";
import BrilliantRoomsImage from "../../../assets/graphics/homePage/brilliant-rooms.png";
import WellEquippedImage from "../../../assets/graphics/homePage/well-equipped.png";
import WellMaintainedImage from "../../../assets/graphics/homePage/well-maintained.png";

class InspectionSection extends Component {
    render() {
        return(
            <div className="inspection_section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p className="section_headline">In person quality inspection</p>
                        </div>

                        <div className="show_desktop">
                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div className="single_inspection_wrapper">
                                    <img className="inspection_image" src={WellEquippedImage} alt="Well Equipped" />
                                    <p className="inspection_headline">Well-equipped</p>
                                    <p className="inspection_description">You can expect a consistent set of amenities you need to live like you do at home – from fast wifi and televisions ready for streaming, to full kitchens </p>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div className="single_inspection_wrapper">
                                    <img className="inspection_image" src={WellMaintainedImage} alt="Well Maintained" />
                                    <p className="inspection_headline">Well-maintained</p>
                                    <p className="inspection_description">Our Providers take the extra effort to ensure your room is extra clean, clutter-free, and fully functioning–from manicured outdoor spaces to tidy bathrooms with strong water pressure. </p>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div className="single_inspection_wrapper">
                                    <img className="inspection_image" src={BrilliantBuildingImage} alt="Brilliant Building" />
                                    <p className="inspection_headline">Brilliant Building</p>
                                    <p className="inspection_description">Our Providers makes sure all the facilities have private gyms to cinema screens, study zones to communal kitchens, you won’t find anything quite other student portal.</p>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div className="single_inspection_wrapper">
                                    <img className="inspection_image" src={BrilliantRoomsImage} alt="Brilliant Rooms" />
                                    <p className="inspection_headline">Brilliant Rooms</p>
                                    <p className="inspection_description">Our Providers, make sure that all the rooms are created by award-winning designers to keep our students relaxed, rested and ready to do the best work of their life.</p>
                                </div>
                            </div>
                        </div>

                        <div className="show_mobile">
                            <div className="inspection_wrappers_mobile">
                                <div className="single_inspection_wrapper">
                                    <div className="image_wrapper">
                                        <img className="inspection_image" src={WellEquippedImage} alt="Well Equipped" />
                                    </div>
                                    <div className="description_wrapper">
                                        <p className="inspection_headline">Well-equipped</p>
                                        <p className="inspection_description">You can expect a consistent set of amenities you need to live like you do at home – from fast wifi and televisions ready for streaming, to full kitchens </p>
                                    </div>
                                </div>

                                <div className="single_inspection_wrapper">
                                    <div className="image_wrapper">
                                        <img className="inspection_image" src={WellMaintainedImage} alt="Well Maintained" />
                                    </div>
                                    <div className="description_wrapper">
                                        <p className="inspection_headline">Well-maintained</p>
                                        <p className="inspection_description">Our Providers take the extra effort to ensure your room is extra clean, clutter-free, and fully functioning–from manicured outdoor spaces to tidy bathrooms with strong water pressure. </p>
                                    </div>
                                </div>

                                <div className="single_inspection_wrapper">
                                    <div className="image_wrapper">
                                        <img className="inspection_image" src={BrilliantBuildingImage} alt="Brilliant Building" />
                                    </div>
                                    <div className="description_wrapper">
                                        <p className="inspection_headline">Brilliant Building</p>
                                        <p className="inspection_description">Our Providers makes sure all the facilities have private gyms to cinema screens, study zones to communal kitchens, you won’t find anything quite other student portal.</p>
                                    </div>
                                </div>

                                <div className="single_inspection_wrapper">
                                    <div className="image_wrapper">
                                        <img className="inspection_image" src={BrilliantRoomsImage} alt="Brilliant Rooms" />
                                    </div>
                                    <div className="description_wrapper">
                                        <p className="inspection_headline">Brilliant Rooms</p>
                                        <p className="inspection_description">Our Providers, make sure that all the rooms are created by award-winning designers to keep our students relaxed, rested and ready to do the best work of their life.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default InspectionSection;
import React, { Component } from "react";
import $ from "jquery";

import BannerImageDesktop from '../../../assets/graphics/homePage/homepage-banner-desktop.jpg';
import TypeAhead from "../../components/SearchBar/searchBar";
import OfferIcon from "../../../assets/graphics/homePage/common-offer.png";

class HeroSection extends Component {
    state = {
        deviceType : ""
    }

    componentDidMount() {
        let windowSize = $(document).width();
        if(windowSize <= 600) {
            this.setState({
                deviceType : "mobile"
            })
        }
        else {
            this.setState({
                deviceType : "desktop"
            })
        }
    }

    render() {
        return (
            <div className="homepage_hero_section">
                {
                    this.state.deviceType === "desktop" &&
                    <div className="hero_image_wrapper">
                        <img src={BannerImageDesktop} className="show_desktop" alt="HomeinAbroad" />
                    </div>
                }

                <div className="hero_quotation_wrapper">
                    <h1>Book your Student Accommodation hassle free</h1>
                    <TypeAhead />
                    <div className="offer_showcase_wrapper">
                        <div className="single_offer_wrapper">
                            <img src={OfferIcon} alt="Offer" />
                            <p>Get GBP/AUD 200 off</p>
                        </div>

                        <div className="single_offer_wrapper">
                            <img src={OfferIcon} alt="Offer" />
                            <p>Free Bank Account</p>
                        </div>

                        <div className="single_offer_wrapper">
                            <img src={OfferIcon} alt="Offer" />
                            <p>Free Pick-Up from Airport</p>
                        </div>

                        <div className="single_offer_wrapper">
                            <img src={OfferIcon} alt="Offer" />
                            <p>Cineworld Unlimited World</p>
                        </div>
                    </div>
                </div>

                <div className="content_banner">
                    The best of Student Accommodation is now HomeinAbroad – offering the
                    world's most extraordinary student accommodation with the highest standard
                    of service.
                </div>
            </div>
        )
    }
}

export default HeroSection;
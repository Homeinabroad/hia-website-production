import React, { Component } from "react";

import RemittanceIcon from '../../../assets/graphics/homePage/remmitance.png';
import TaxiIcon from '../../../assets/graphics/homePage/taxi.svg';
import GuarantorIcon from '../../../assets/graphics/homePage/Guarantor.svg';
import BankAccountIcon from '../../../assets/graphics/homePage/bank-account.svg';
import FlightBookingIcon from '../../../assets/graphics/homePage/flight-booking.svg';

class ValueAddedServiceSection extends Component {
    render() {
        return (
            <div className="homepage_vas_section">
                <div className="container">
                    <div className="row">
                        <p className="section_headline">WE ARE A ONE STOP SOLUTION</p>
                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <div className="different_box single_vas_wrapper">
                                <img src={RemittanceIcon} />
                                <p>Remittance</p>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <div className="similar_box single_vas_wrapper">
                                <img src={TaxiIcon} />
                                <p>Taxi Service</p>
                            </div>

                            <div className="similar_box single_vas_wrapper">
                                <img src={GuarantorIcon} />
                                <p>Guarantor</p>
                            </div>
                        </div>

                        <div className="show_desktop col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <div className="similar_box single_vas_wrapper">
                                <img src={BankAccountIcon} />
                                <p>Bank Account</p>
                            </div>

                            <div className="similar_box single_vas_wrapper">
                                <img src={FlightBookingIcon} />
                                <p>Flight Booking</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ValueAddedServiceSection;
import React, { Component } from "react";
import LazyLoad from "react-lazyload";
import { Helmet } from "react-helmet";

import HeroSection from "./heroSection";
import UspSection from "./uspSection";
import TopCitiesSection from "./topCities";
import TopPropertiesSection from "./topProperties";
import InspectionSection from "./inspection";
import TestimonialSection from "./testimonials";
//import ValueAddedServicesSection from "./vasSection";
import SeoContentSection from "./seoContentSection";

class HomePage extends Component {
    head = () => {
        return (
            <Helmet>
                <title>Find Overseas Student Accommodation and Housing - homeinabroad.com</title>
                <meta name="description" content="Homeinabroad offer the best student accommodation place, Choose the redefining and hassle-free student living around the campus."/>
                <link rel="canonical" href="https://homeinabroad.com/" />
                <meta property="og:image" content="https://hia-s3-website.s3.eu-west-2.amazonaws.com/media/facilities/logo.png"/>
                <meta property="og:url" content="https://www.homeinabroad.com"/>
                <meta property="og:title" content="Find Overseas Student Accommodation and Housing - homeinabroad.com"/>
                <script type="application/ld+json">{`
                    {
                          "@context": "https://schema.org",
                          "@type": "Organization",
                          "name": "HomeinAbroad",
                          "url": "https://homeinabroad.com/",
                          "logo": "https://homeinabroad.com/js/images/logo.png",
                          "contactPoint": {
                            "@type": "ContactPoint",
                            "telephone": "",
                            "contactType": "customer service",
                            "areaServed": "AU",
                            "availableLanguage": "en"
                            
                          },
                          "aggregateRating": {
                            "@type": "AggregateRating",
                            "ratingValue": "3.2",
                            "bestRating": "5",
                            "worstRating": "2",
                            "ratingCount": "40"
                            },
                          "sameAs": [
                            "https://www.facebook.com/Homeinabroad-100236905128631/?modal=admin_todo_tour",
                            "https://www.instagram.com/homeinabroad/"
                          ]
                        }
                    `}
                </script>
            </Helmet>
        );
    };
    render() {
        return(
            <div className="homepage">
                {this.head()}
                <HeroSection />
                <LazyLoad>
                    <UspSection />
                </LazyLoad>
                <LazyLoad>
                    <TopCitiesSection />
                </LazyLoad>
                <LazyLoad>
                    <TopPropertiesSection />
                </LazyLoad>
                {/*<LazyLoad>
                    <ValueAddedServicesSection />
                </LazyLoad>*/}
                <LazyLoad>
                    <InspectionSection />
                </LazyLoad>
                <LazyLoad>
                    <TestimonialSection />
                </LazyLoad>
               <SeoContentSection />
            </div>
        )
    }
}

export default {
    component: HomePage
};
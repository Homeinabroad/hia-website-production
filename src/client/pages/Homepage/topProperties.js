import React, {Component} from "react";
import apiEndPointConfig from "../../endpoints/index";

const axios = require("axios");

class TopProperties extends Component {
    state = {
        featuredProperties: [],
        deviceType : ""
    };

    componentDidMount() {
        let windowSize = $(document).width();
        if(windowSize <= 600) {
            this.setState({
                deviceType : "mobile"
            })
        }
        else {
            this.setState({
                deviceType : "desktop"
            })
        }
        this.getFeaturedProperties();
    }

    getFeaturedProperties = () => {
        axios.get(apiEndPointConfig.featuredProperties)
            .then((res) => {
                this.setState({
                    featuredProperties: this.state.deviceType === "mobile" ? res.data.splice(0, 8) : res.data.splice(0, 6)
                })
            })
    };

    render() {
        return (
            <div className="top_properties">
                <p className="section_headline">POPULAR PROPERTIES</p>
                <div className="show_desktop">
                    <div className="container">
                        <div className="row">
                            {
                                this.state.deviceType === "desktop" && this.state.featuredProperties.length > 0 &&
                                this.state.featuredProperties.map((properties, i) => {
                                    return (
                                        <a href={"/" + properties.country_slug + "/" + properties.city_slug + "/" + properties.slug}>
                                            <div key={i} className="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                                <div className="single_property_wrapper">
                                                    <img src={properties.first_image} alt={properties.name}/>
                                                    <div className="property_description_wrapper">
                                                        <div className="rating_wrapper">
                                                            <span className="glyphicon glyphicon-star"></span>
                                                            {properties.rating}
                                                        </div>
                                                        <p className="property_name">{properties.name}</p>
                                                        <p className="property_place">{properties.country_name} - {properties.city_name}</p>
                                                        <p className="property_min_price">starts
                                                            at <span>{properties.price_unit}{properties.min_bed_pricing}/week</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>

                <div className="show_mobile">
                    <div className="properties_wrapper_mobile">
                        {
                            this.state.featuredProperties.length > 0 &&
                            this.state.featuredProperties.map((property, i) => {
                                return(
                                    <a key={i} href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug}>
                                        <div className="single_property_wrapper_mobile">
                                            <div className="image_wrapper">
                                                <img src={property.first_image} alt={property.name} />
                                            </div>
                                            <div className="description_wrapper">
                                                <p><span className="glyphicon glyphicon-star"></span>{property.rating}</p>
                                                <p>{property.name}</p>
                                                <p>{property.country_name} - {property.city_name}</p>
                                                <p>starts at <span>{property.price_unit}{property.min_bed_pricing}/week</span></p>
                                            </div>
                                        </div>
                                    </a>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default TopProperties;
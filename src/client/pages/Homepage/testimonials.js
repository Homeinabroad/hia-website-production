import React, {Component} from "react";

import SophiaImage from '../../../assets/graphics/homePage/girl.jpg';
import NataliaImage from "../../../assets/graphics/homePage/natalia.jpg";
import LynnImage from "../../../assets/graphics/homePage/lynn.jpg";
import NjokiImage from "../../../assets/graphics/homePage/njoki.jpg";

class TestimonialSection extends Component {
    render() {
        return (
            <div className="testimonial_section">
                <div className="show_desktop">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div className="background_div">
                                    <p className="section_headline">OUR REPUTATIONS AROUND THE GLOBE</p>
                                    <div className="all_testimonials_wrapper">
                                        <div className="single_testimonial_wrapper">
                                            <div className="reviewer_image">
                                                <img src={SophiaImage} alt="Sophia"/>
                                            </div>

                                            <div className="reviewer_name_and_review">
                                                <p>Sophia Saldon</p>
                                                <p>London, United Kingdom</p>
                                                <p>
                                                    I needed a place where I could complete my degree work in
                                                    peace which was also close to home and clean too. I have
                                                    successfully
                                                    found this after only one day of speaking to him. Would definitely
                                                    recommend HomeinAbroad.
                                                </p>
                                            </div>
                                        </div>

                                        <div className="single_testimonial_wrapper">
                                            <div className="reviewer_image">
                                                <img src={NataliaImage} alt="Natalia"/>
                                            </div>

                                            <div className="reviewer_name_and_review">
                                                <p>Natalia Hir</p>
                                                <p>Nottingham, United Kingdom</p>
                                                <p>
                                                    I needed a place where I could complete my degree work in
                                                    peace which was also close to home and clean too. I have
                                                    successfully
                                                    found this after only one day of speaking to him. Would definitely
                                                    recommend HomeinAbroad.
                                                </p>
                                            </div>
                                        </div>

                                        <div className="single_testimonial_wrapper">
                                            <div className="reviewer_image">
                                                <img src={LynnImage} alt="Lynn"/>
                                            </div>

                                            <div className="reviewer_name_and_review">
                                                <p>Lynn Duncan</p>
                                                <p>Sheffield, United Kingdom</p>
                                                <p>
                                                    If you are looking for good, friendly service when
                                                    it comes to picking your student accommodation, then
                                                    you should choose the right platform. HomeinAbroad
                                                    provides good rooms, student-friendly pricing all
                                                    whilst ensuring your needs are all met. I would
                                                    definitely recommend HomeinAbroad to my friends and relatives.
                                                </p>
                                            </div>
                                        </div>

                                        <div className="single_testimonial_wrapper">
                                            <div className="reviewer_image">
                                                <img src={NjokiImage} alt="Sylvia"/>
                                            </div>

                                            <div className="reviewer_name_and_review">
                                                <p>Sylvia Njoki</p>
                                                <p>London, United Kingdom</p>
                                                <p>
                                                    It was so amazing. They took no time in finding me the perfect place
                                                    to stay. I would recommend HomeinAbroad to everyone who wants to
                                                    book their student accommodation anywhere.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="show_mobile">
                    <p className="section_headline">OUR REPUTATIONS AROUND THE GLOBE</p>
                    <div className="testimonials_wrapper_mobile">
                        <div className="single_testimonial_wrapper_mobile">
                            <p>Sophia Saldon</p>
                            <p>London, United Kingdom</p>
                            <p>
                                I needed a place where I could complete my degree work in
                                peace which was also close to home and clean too. I have successfully
                                found this after only one day of speaking to him. Would definitely
                                recommend HomeinAbroad.
                            </p>
                        </div>

                        <div className="single_testimonial_wrapper_mobile">
                            <p>Natalia Hir</p>
                            <p>Nottingham, United Kingdom</p>
                            <p>
                                If you are looking for good, friendly service when
                                it comes to picking your student accommodation, then
                                you should choose the right platform. HomeinAbroad
                                provides good rooms, student-friendly pricing all
                                whilst ensuring your needs are all met. I would
                                definitely recommend HomeinAbroad to my friends and relatives.
                            </p>
                        </div>

                        <div className="single_testimonial_wrapper_mobile">
                            <p>Lynn Duncan</p>
                            <p>Sheffield, United Kingdom</p>
                            <p>
                                It was so amazing. They took no time in finding me the perfect place to stay. I would
                                recommend HomeinAbroad to everyone who wants to book their student accommodation
                                anywhere.
                            </p>
                        </div>

                        <div className="single_testimonial_wrapper_mobile">
                            <p>Sylvia Njoki</p>
                            <p>London, United Kingdom</p>
                            <p>
                                Agents at HomeinAbroad is very helpful and replied me very quickly. This made me feel
                                comfortable and sure in order to book my accommodation. Thank you so much HomeinAbroad.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TestimonialSection;
import React, { Component } from "react";

class SeoContentSection extends Component {
    state ={
        collapsedHeight : true
    }

    expandSeoContaent = () =>{
        this.setState({
            collapsedHeight : false
        })
    };

    collapseSeoContaent = () => {
        this.setState({
            collapsedHeight : true
        })
    }

    render() {
        return(
            <div className="seo_content_section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p className="section_headline">Choose Home in Aboard for Your Student Accommodation Needs</p>
                            <div>
                            HomeInAbroad is a one-stop solution for students looking for a perfect place to reside. We provide a diverse
                            range of International Student Accommodation, that suits every scholar's budget. Our housing properties
                            worldwide are safe, furnished and comfortable. We provide good and reliable student residing properties in
                            popular academic destinations for a convenient and homey stay in a new city.
                            <br />
                            We help students in finding the perfect accommodation that is crafted with all smart and advanced amenities.
                            Our on-site student support desk is 24/7 available to assist aspirants with the authentic search of good,
                            secure, cosy and cheap International Student Accommodation easily. We at HomeInAbroad care for
                            students planning to pursue careers and study plans in the new academic hub, so with us they can
                            experience best of the student residing property over their desired choices.
                            <br />
                            With us, students can not only search but also get all-end accommodation related information and easy
                            booking efficiency as well. The top academic destinations for International Student Accommodation we
                            serve are United Kingdom, Australia, Ireland, Europe, USA, and more in 100+ cities across the planet.
                          
                            </div>
                          
                            {
                                this.state.collapsedHeight ?
                                <div >
                                 <p className="collapse_expand_handler" onClick={this.expandSeoContaent}>Read More ...</p>
                                 </div> 
                                 :
                                 <div >
                                        <h6>UK Student Accommodation</h6>
                            <div>
                            HomeInAbroad is a systematic approach for reliable UK Student Accommodation for scholars new in-
                            country. We offer numerous secure student residing in places that ensure complete comfort and convenience.
                            To experience good academic UK is a popular destination, and with us, they can avail of homey and
                            affordable Student Accommodations.
                            <br />
                            With our provided accommodation property, the safety of newcomers in the city is a significant aspect given
                            by us. HomeInAbroad is one of the trustworthy student housing providers in many cities all over the UK. The
                            experts working with us will customize the needs of students with affordability they want. Picking up the
                            right UK Student Accommodation is no more a hassle for scholars planning to pursue their academics.
                            <br />
                            As out experts keen to give students comfort stay with complete peace of mind for studying. The popular
                            cities in the UK we serve for quality and convenient stay for students are London, Manchester, Birmingham,
                            Liverpool, Oxford, Cambridge, Nottingham etc. The accommodation types and properties we offer are
                            relatively secure, homey and fledged with amenities. Precisely, staying in such UK Student Accommodation
                            will also release the tension of guardians for their children.
                            <br />
                            </div>
                            <h6>Australia Student Accommodation</h6>
                            <div>
                            HomeInAbroad offers scholars residing properties in Australia which are authentic, verified and curated ones.
                            With us, students can get a variety of options in Australia Student Accommodation proving good stay,
                            property types and cost simultaneously. We know Australian cities are worldwide popular academic
                            destinations, therefore throughout the year thousands of students came in the region in the hope of a better
                            future.
                            <br />
                            The squad of experts working with us are always dedicated to serving students with all kind accommodation
                            queries. HomeInAbroad ensures aspirants to have a convenient, cosy, and comfortable stay. We are
                            complete plan in action serving students with multiple services in accommodation, taxi/cab, and flight booking.
                            Moreover, the student residing property offered by us are safe, spacious and fledged with all advanced
                            amenities.
                            <br />
                            We even guarantee the cost-efficiency of the Australia Student Accommodation, with complete warming
                            stay, and peace of mind to study. Our accommodation services are flexible and available for students in the
                            popular academic hubs of Australia like Sydney, Melbourne, Brisbane, Canberra, Adelaide, Newcastle, and
                            more. With us, you can find your kind of accommodation, which is nearest to your academic institute, and
                            even access to nearby markets, transport system etc. HomeInAbroad understands the student's struggles
                            and journey to look for a safe place to stay and study in a new city.
                            
                            </div>
                            <p className="collapse_expand_handler" onClick={this.collapseSeoContaent}>Read Less ...</p>
                            
                            </div> 

                            }
                         
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SeoContentSection;
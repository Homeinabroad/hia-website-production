import React, { Component } from "react";

import endPointConfig from "../../endpoints/index";
import $ from "jquery";

const axios = require('axios');

class TopCities extends Component {
    state = {
        featuredCities : [],
        isLoading : true,
        deviceType : ""
    };

    componentDidMount() {
        let windowSize = $(document).width();
        if(windowSize <= 600) {
            this.setState({
                deviceType : "mobile"
            }, () => {
                this.getTrendingCities();
            })
        }
        else {
            this.setState({
                deviceType : "desktop"
            }, () => {
                this.getTrendingCities();
            })
        }
    }

    getTrendingCities = () => {
        axios.get(endPointConfig.featuredCities)
            .then((res) => {
                if(res.status === 200) {
                    this.setState({
                        featuredCities : res.data.splice(0,6),
                        isLoading: false
                    })
                }
            })
    }

    render() {
        return(
            <div className="top_cities">
                <p className="section_headline">TOP DESTINATIONS</p>
                {
                    this.state.deviceType === "desktop" && this.state.featuredCities.length > 0 &&
                    <div className="top_cities_wrapper_desktop">
                        <div className="column_wrapper">
                            <div className="top_line_city">
                                <div className="large_box single_city_wrapper">
                                    <a href={"/" + this.state.featuredCities[0].country_slug + "/" + this.state.featuredCities[0].slug}>
                                        <div className="single_city_wrapper large_box">
                                            <img src={this.state.featuredCities[0].banner_image}
                                                 alt={this.state.featuredCities[0].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[0].name}</p>
                                                <p>{this.state.featuredCities[0].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="bottom_line_city" style={{"margin" : "20px 0 0 0"}}>
                                <div className="small_box single_city_wrapper" style={{"margin" : "0 20px 0 0"}}>
                                    <a href={"/" + this.state.featuredCities[3].country_slug + "/" + this.state.featuredCities[3].slug}>
                                        <div className="single_city_wrapper small_box">
                                            <img src={this.state.featuredCities[3].banner_image}
                                                 alt={this.state.featuredCities[3].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[3].name}</p>
                                                <p>{this.state.featuredCities[3].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="small_box single_city_wrapper">
                                    <a href={"/" + this.state.featuredCities[4].country_slug + "/" + this.state.featuredCities[4].slug}>
                                        <div className="single_city_wrapper small_box">
                                            <img src={this.state.featuredCities[4].banner_image}
                                                 alt={this.state.featuredCities[4].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[4].name}</p>
                                                <p>{this.state.featuredCities[4].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div className="column_wrapper">
                            <div className="bottom_line_city">
                                <div className="small_box single_city_wrapper" style={{"margin" : "0 20px 0 0"}}>
                                    <a href={"/" + this.state.featuredCities[1].country_slug + "/" + this.state.featuredCities[1].slug}>
                                        <div className="single_city_wrapper small_box">
                                            <img src={this.state.featuredCities[1].banner_image}
                                                 alt={this.state.featuredCities[1].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[1].name}</p>
                                                <p>{this.state.featuredCities[1].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="small_box single_city_wrapper">
                                    <a href={"/" + this.state.featuredCities[2].country_slug + "/" + this.state.featuredCities[2].slug}>
                                        <div className="single_city_wrapper small_box">
                                            <img src={this.state.featuredCities[2].banner_image}
                                                 alt={this.state.featuredCities[2].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[2].name}</p>
                                                <p>{this.state.featuredCities[2].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="top_line_city" style={{"margin" : "20px 0 0 0"}}>
                                <div className="large_box single_city_wrapper">
                                    <a href={"/" + this.state.featuredCities[5].country_slug + "/" + this.state.featuredCities[5].slug}>
                                        <div className="single_city_wrapper large_box">
                                            <img src={this.state.featuredCities[5].banner_image}
                                                 alt={this.state.featuredCities[5].name}/>
                                            <div className="city_name_and_country">
                                                <p>{this.state.featuredCities[5].name}</p>
                                                <p>{this.state.featuredCities[5].country_name}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {
                    this.state.deviceType === "mobile" && this.state.featuredCities.length > 0 &&
                    <div className="cities_wrapper_mobile">
                        {
                            this.state.featuredCities.length > 0 &&
                            this.state.featuredCities.map((city, i) => {
                                return(
                                    <a key={i} href={"/" + city.country_slug + "/" + city.slug}>
                                        <div className="single_city_wrapper_mobile">
                                            <img src={city.banner_image} alt={city.name} />
                                            <p>{city.name}</p>
                                        </div>
                                    </a>
                                )
                            })
                        }
                    </div>
                }
            </div>
        )
    }
}

export default TopCities;
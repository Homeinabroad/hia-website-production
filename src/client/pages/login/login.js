import React, { Component } from "react";
import endPointConfig from "../../endpoints";

const axios = require('axios');

class Login extends Component{
    state = {
        loginCredentials: {
            email: "",
            password: ""
        },
        forgotPasswordTemplate: false,
        registeredEmail: "",
        errorMessage : "",
        disableButton : false,
        successMessage : null
    };

    componentDidMount() {
        if(sessionStorage.getItem("token")) {
            window.location.assign(document.referrer);
        }
    }

    handleInputValues = (e) => {
        let obj = this.state.loginCredentials;
        obj[e.target.name] = e.target.value;
        this.setState({
            loginCredentials: obj
        })
    };

    validateLoginForm = () => {
        if(this.state.loginCredentials.email.trim() === "") {
            this.setState({
                errorMessage : "EMail is required.",
                disableButton : false
            })
            return false
        }
        else if(this.state.loginCredentials.password.trim() === "") {
            this.setState({
                errorMessage : "Password is required.",
                disableButton : false
            })
            return false
        }
        else return true
    };

    submitLoginForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : "",
            disableButton : true
        })
        if(this.validateLoginForm() === true) {
            let loginBodyObj = {
                username : this.state.loginCredentials.email,
                password : this.state.loginCredentials.password
            };
            axios({
                method : 'POST',
                url : endPointConfig.login,
                data : loginBodyObj
            })
                .then((res) => {
                    if(res.status === 200) {
                        this.setState({
                            errorMessage : "",
                            disableButton : false
                        })
                        sessionStorage.setItem("token", res.data.access);
                        sessionStorage.setItem("userId", res.data.user);
                        window.location.assign("/");
                    }
                })
                .catch((error) => {
                    if(error) {
                        this.setState({
                            errorMessage : error.response.data.detail + " " + "Please confirm your credentials.",
                            disableButton : false
                        })
                    }
                })
        }
    };

    toggleForgotPassword = () => {
        this.setState({
            forgotPasswordTemplate: !this.state.forgotPasswordTemplate,
            errorMessage : ""
        })
    };

    changePassword = (e) => {
        this.setState({
            registeredEmail: e.target.value
        })
    };

    validateForgotPasswordForm = () => {
        if(this.state.registeredEmail.trim() === "") {
            this.setState({
                errorMessage : "Please enter your registered EMail Address.",
                disableButton : false
            })
            return false
        }
        else return true;
    };

    submitForgotPasswordForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : "",
            disableButton : true
        })
        if(this.validateForgotPasswordForm() === true) {
            let forgotPasswordBodyObj = {
                email : this.state.registeredEmail
            };
            axios({
                method : 'POST',
                url : endPointConfig.passwordReset,
                data : forgotPasswordBodyObj
            })
                .then((res) => {
                    if(res.status === 200) {
                        this.setState({
                            successMessage : "We've sent you a password reset link on your EMail Address"
                        }, () => {
                            setTimeout(() => {
                                this.setState({
                                    successMessage : null,
                                    forgotPasswordTemplate : false,
                                    errorMessage : "",
                                    disableButton : false,
                                    registeredEmail : ""
                                })
                            },3000)
                        })
                    }
                })
        }
    };

    render(){
        return(
            <div className="container login_wrapper">
                <div className="row">
                    <div className="show_desktop col-lg-8 col-col-md-8 col-sm-8 col-xs-12">

                    </div>

                    <div className="col-lg-4 col-col-md-4 col-sm-4 col-xs-12">
                        <div className="login_container">
                            <form className="form_wrapper" onSubmit={this.submitLoginForm}>
                                <label>EMAIL ADDRESS</label>
                                <input 
                                    type="email" 
                                    name="email" 
                                    onChange={this.handleInputValues}
                                    required={true}
                                    value={this.state.loginCredentials.email}
                                />
                                
                                <label>PASSWORD</label>
                                <input 
                                    type="password" 
                                    name="password" 
                                    onChange={this.handleInputValues}
                                    required={true}
                                    value={this.state.loginCredentials.password}
                                />
                                <span onClick={this.toggleForgotPassword}>Forgot Password?</span>
                                {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                                <button disabled={this.state.disableButton} type="submit" className="btn btn-primary pink_button">SIGN IN</button>
                            </form>

                            <p className="account_creation_command">
                                Don't have an account yet? &nbsp;
                                <a href="/registration">
                                    Register Now
                                </a>
                            </p>
                        </div>
                    </div>
                </div>

                {
                    (this.state.forgotPasswordTemplate) 
                    ? <div className="forgot_password_container">
                        <form onSubmit={this.submitForgotPasswordForm} className="fp_box">
                            <h4>Please Enter your registered EMail Address</h4>
                            <input 
                                type="email" 
                                name="registeredEmail"
                                onChange={this.changePassword}
                                required={true}
                                value={this.state.registeredEmail}
                            />
                            {this.state.successMessage && <p className="success_message">{this.state.successMessage}</p>}
                            {this.state.errorMessage !== "" ? <p className="z">{this.state.errorMessage}</p> : null}
                            <button disabled={this.state.disableButton} className="btn btn-primary pink_button">SUBMIT</button>
                            <button onClick={this.toggleForgotPassword} className="btn btn-primary white_button">CANCEL</button>
                        </form>
                    </div>
                    : null   
                }    
            </div>
        );
    }
}


export default {
    component: Login
};
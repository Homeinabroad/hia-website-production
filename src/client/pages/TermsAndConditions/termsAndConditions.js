import React, { Component } from "react";

import { Helmet } from "react-helmet";

class TermsAndConditions extends Component {

    head = () => {
        return (
            <Helmet>
            <title>Terms and Privacy Policy - Homeinabroad.com</title>
        </Helmet>
        )
    };
    render() {
        return(
           <div className="terms_conditions_page">
              
               <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1 className="terms_conditions_page_headline">
                            Terms and Conditions
                            </h1>
                            <h3 className="page_subheading">Terms and Conditions of Website Use</h3>
                            <p className="terms-conditions_content">These terms of use (together with the documents referred to in them) tell you the terms
                                of use on which you may use or access <a href="https://www.homeinabroad.com/" target="blank" >www.homeinabroad.com,</a> a subdomain or any
                                     such related websites and/or mobile application for such website (our “Sites”) whether
                                    as a guest or registered user. Use of our Sites includes accessing, browsing or
                                    registering for an account. If you log in to our Sites through a third party such as
                                    Facebook, Google etc. then you will be bound by these terms when you reach our Site.
                                    Please read and accept these terms of use carefully before using our Sites, as these will
                                    apply to your use of our Sites. If you do not agree to these terms of use, you must not
                                    use our Sites.<br></br>
                                        By accessing this website we assume you accept these terms and conditions. Do not
                            continue to use Home In Abroad if you do not agree to take all of the terms and
                            conditions stated on this page.<br></br>
                            The following terminology applies to these Terms and Conditions, Privacy Statement
                            and Disclaimer Notice and all Agreements:"Client","You",and "Your" refers to you, the
                            person log on this website and compliant to the Company’s terms and conditions."The Company","Ourselves","We","Our" and 
                            "Us", refers to our Company."Party", "Parties" or "Us",refers to both the Client and ourselves. All terms refer to the offer, acceptance
                            and consideration of payment necessary to undertake the process of our assistance to
                            the Client in the most appropriate manner for the express purpose of meeting the
                            Client’s needs in respect of provision of the Company’s stated services, in accordance
                            with and subject to, prevailing law of Australia. Any use of the above terminology or
                            other words in the singular, plural, capitalization and/or he/she or they, are taken as
                            interchangeable and therefore as referring to same.
                            
                            </p>
                            <h3 className="page_subheading">Who We Are  </h3>
                            <p className="terms-conditions_content">Home In Abroad is an organization who caters to accommodation needs of students
                            who are travelling to United Kingdom, USA, and Australia and across the globe. We
                            provide the best available student accommodation and makes Student’s University
                            experience truly joyous by providing them the top quality rooms as per their
                            requirements. For details contact us or alternatively visit<a href="https://www.homeinabroad.com/" target="blank" > www.homeinabroad.com</a>
                            </p>

                            <h3 className="page_subheading">License   </h3>
                            <p className="terms-conditions_content">Unless otherwise stated, Homeinabroad and/or its licensors own the intellectual
                            property rights for all material on Homeinabroad. All intellectual property rights are
                            reserved. You may access this site for your own personal use subjected to restrictions
                            set in these terms and conditions.<br></br>
                            You must not:
                            <ul>
                                <li>Republish material from Homeinabroad</li>
                                <li>Sell, rent or sub-license material from Homeinabroad</li>
                                <li>Reproduce, duplicate or copy material from Homeinabroad</li>
                                <li>Redistribute content from Homeinabroad</li>
                            </ul>
                            <br></br>
                            This Agreement shall begin on the date hereof.<br></br>
                            Parts of this website offer an opportunity for users to post and exchange opinions and
                            information in certain areas of the website. Homeinabroad does not filter, edit, publish
                            or review Comments prior to their presence on the website. Comments do not reflect
                            the views and opinions of Homeinabroad, its agents and/or affiliates. Comments reflect
                            the views and opinions of the person who post their views and opinions. To the extent
                            permitted by applicable laws, Homeinabroad shall not be liable for the Comments or for
                            any liability, damages or expenses caused and/or suffered as a result of any use of
                            and/or posting of and/or appearance of the Comments on this website.<br></br>
                            Homeinabroad reserves the right to monitor all Comments and to remove any
                            Comments which can be considered inappropriate, offensive or causes breach of these
                            Terms and Conditions.<br></br>
                            You warrant and represent that:
                            <ul>
                                <li>You are entitled to post the Comments on our website and have all necessary licenses
                            and consents to do so;</li>
                            <li>The Comments do not invade any intellectual property right, including without
                            limitation copyright, patent or trademark of any third party;</li>
                            <li>The Comments do not contain any defamatory, libellous, offensive, indecent or
                                otherwise unlawful material which is an invasion of privacy</li>
                                <li>The Comments will not be used to solicit or promote business or custom or present
                                commercial activities or unlawful activity.</li>
                            </ul>
                            You hereby grant Homeinabroad a non-exclusive license to use, reproduce, edit and
                            authorize others to use, reproduce and edit any of your Comments in any and all forms,
                            formats or media.
                            </p>
                            <h3 className="page_subheading">Your Account and Password   </h3>
                            <p className="terms-conditions_content">In order to register for an account on our Sites you must be aged 18 or over at the point
                                of registration or be 13 or older and have your parent or guardian’s consent to register
                                for an account on our Sites. You must (or your parent or guardian acting on your behalf)
                                have the power to enter a binding contract with us and not be barred from doing so
                                under any applicable laws. If you choose, or you are provided with, any user
                                identification code, password or any other piece of information as part of our security
                                procedures to set up an account, you must treat such information as confidential. You
                                must not disclose it to any third party. We have the right to disable any user
                                identification code or password, whether chosen by you or allocated by us, at any time,
                                if in our reasonable opinion you have failed to comply with applicable law or any of the
                                provisions of these terms of use and/or if we believe that your account is being used in
                                an unauthorised or fraudulent manner. If you know or suspect that anyone other than
                                you knows your user identification code or password you must promptly notify us at
                                support@homeinabroad.com. Following such notification you may be required to set
                                up a new account with a new identification code and/or password.</p>
                                <h3 className="page_subheading">Prices   </h3>
                                <p className="terms-conditions_content">The prices of properties displayed on the Sites are liable to change at any time. Despite
                                our best efforts, some of the prices listed on the Sites may be incorrect. We expressly
                                reserve the right to correct any pricing errors on our Sites and/or on potential bookings
                                which have not yet been completed. We display the prices that Advertisers provide to us
                                from time to time. We are not responsible or liable for the accuracy of the prices
                                displayed, to the maximum extent permitted by applicable law. Due to the international
                                nature of our Sites, the currency of the prices shown may vary depending on your
                                location. Currency rates given on the Sites are based on various publicly available
                                sources and should be used as guidelines only. Rates are not verified as accurate and
                                actual prices may vary from those shown on the Sites. From time to time, third parties
                                may list promotions, special offers or other forms of coupon on our Sites (“Coupons”).
                                Coupons will contain terms and conditions that will apply in addition to these Terms,
                                and will be void if you attempt to redeem the Coupon in violation of either these Terms
                                or the terms of the Coupon. Unless expressly stated on the Coupon, it may not be used
                                in combination with other promotions or discounts. Coupons are only redeemable
                                during the promotional period specified in the Coupon, subject to availability. These
                                Coupons will be non-transferable and have no alternative cash value.</p>
                                <h3 className="page_subheading">Changes and Cancellation   </h3>
                                <p className="terms-conditions_content">Any tenancy agreement entered into will be between a Student and an Advertiser.  It is
                            a Student’s responsibility to make themselves aware of the Advertiser’s cancellation policy at the time of booking. In the event that you need to change or cancel your
                            agreement, please contact your allocated Homeinabroad booking agent.</p>
                            <h3 className="page_subheading">Communication with our team  </h3>
                                <p className="terms-conditions_content">Any communication through email, SMS or phone calls will be recorded for quality
                            assurance purposes..</p>
                            <h3 className="page_subheading">Changes to the Terms </h3>
                                <p className="terms-conditions_content">We may revise these terms at any time by amending this page. We will use appropriate
                                    means, such as relevant announcements on our homepage, to inform you on such
                                    amendments. If you do not agree with the changes, you must stop using the Sites.</p>
                                    <h3 className="page_subheading">Offers </h3>
                                <p className="terms-conditions_content">All promotional offers listed on the HomeinAbroad website are applicable after a
                                    student checks in to the property, unless stated otherwise. Offers made available in
                                    partnership with property providers are subject to the policies of respective providers.
                                    Offers are non-transferable and valid until supplies last. HomeinAbroad reserves the
                                    right to withdraw, amend or cancel an offer at any time.</p>
                                    <h1 className="terms_conditions_page_headline">
                            Privacy Policies
                            </h1>
                                <p className="terms-conditions_content">At HOMEINABROAD, accessible from <a href="https://www.homeinabroad.com/" target="blank" >http://homeinabroad.com</a> one of our main
                    priorities is the privacy of our visitors. This Privacy Policy document contains types of
                    information that is collected and recorded by Student Room Booking and how we use it.<br></br>
                    one of our main priorities is the privacy of our visitors. This Privacy Policy document
                    contains types of information that is collected and recorded by Student Room Booking
                    and how we use it.<br></br>
                    If you have additional questions or require more information about our Privacy Policy,
                do not hesitate to contact us through email at support@homeinabroad.com</p>
                <h3 className="page_subheading">Log Files</h3>
                <p className="terms-conditions_content">Homeinabroad follows a standard procedure of using log files. These files log visitors
                    when they visit websites. All hosting companies do this and a part of hosting services&#39;
                    analytics. The information collected by log files include internet protocol (IP) addresses,
                    browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages,
                    and possibly the number of clicks. These are not linked to any information that is
                    personally identifiable. The purpose of the information is for analyzing trends,
                    administering the site, tracking users's movement on the website, and gathering
                    demographic information.</p>
                    <h3 className="page_subheading">Cookies</h3>
                <p className="terms-conditions_content">Homeinabroad follows a standard procedure of using log files. These files log visitors
                We use data collection device known as “cookies” at our website. “Cookies” are small
                files situated on Your mobile/ computer/ device’s hard disk that assist us in providing
                customised services. A cookie helps us analyse your preferences by, for example,
                recording the number of times you have used the Website, and help us in tailoring our
                services to suit your interests. We use various analytics tools and social media
                engagement tool which sets a cookie on your device. Data may be collected by social
                media companies that may enable them showing advertisements. A “cookie” does not
                give Us access to your device. You may choose to disable the “cookie” feature on Your
                device. Using our online chat functionality may also mean that we collect information
                on your country and IP address. This information will not be used for any purpose other
                than to enhance the functionality of the website.</p>

                <h3 className="page_subheading">Our Use of Your Information</h3>
                <p className="terms-conditions_content">The information given by You shall be used to contact You when necessary. After your
                    oral or written confirmation, information will be passed on to our third party
                    accommodation partners for booking your Accommodation. Please read third party
                    accommodation partner’s privacy policy before giving oral or written confirmation We
                    can send you details about our services or blog posts and to notify about changes in
                    service. The information we collected from you can be used to improve our services,
                    analytics, research, survey, advertisement and make suggestions to you and other users
                    of the accommodations.<br></br>
                    We may combine information given by other sources  with information you give to us
                        and information we collect about you. We may use information given by other sources
                        and the combined information for the above purposes.</p>
                        <h3 className="page_subheading">Disclosure of Your Information to Third Parties</h3>
                     <p className="terms-conditions_content">Information will be passed on to our third party accommodation partners for booking
                    your Accommodation after your oral or written confirmation. Analytics and search
                    engine providers that assist us in the improvement and optimisation of our site. In case
                    We get acquired by a company, we may share your information to that company If we
                    are under a duty to disclose or share your personal data in order to comply with any
                    legal obligation or to protect privacy and safety of Homeinabroad or other user we can
                    disclose your information to various government agencies and/or third party
                    enforcement agencies
                    Our Site may contain links to third party sites, including sites with which we have
                    affiliate agreements. We have no control and hence not responsible for the privacy
                    practices or the content of such third party websites. This Privacy Policy extends only to
                    personal data we collect from you either via our website. We recommend that you
                    check the privacy and security policies and procedures of each and every other website
                    that you visit.</p>
                    <h3 className="page_subheading">Governing Law & Amendment</h3>
                     <p className="terms-conditions_content">This legal notice shall be governed by and construed in accordance with English law.
                        Disputes arising in connection with this legal notice shall be subject to the exclusive
                        jurisdiction of the English courts although we retain the right to bring proceedings
                        against you for breach of these terms in your country of residence or any other relevant
                        country. <br></br>
                    Our Site may contain links to third party sites, including sites with which we have
                    Our Privacy Policy may change from time to time. Hence we shall post any privacy policy
                changes on this page.</p>




                        </div>
                        </div>
                        </div>

             
           </div>
        )
    }
}

export default {
    component : TermsAndConditions
};
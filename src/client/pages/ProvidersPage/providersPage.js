import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import $ from "jquery";

import LeadsBannerImage from '../../../assets/graphics/landingPage/landing-page-banner.jpg';
import OfferIcon from "../../../assets/graphics/landingPage/offer.png";
import CheckBoxIcon from "../../../assets/graphics/providerLandingPage/checkedBox.png";
import StarRatingComponent from 'react-star-rating-component';
import JennyImage from '../../../assets/graphics/providerLandingPage/jenny.jpg';
import NupurImage from '../../../assets/graphics/providerLandingPage/Nupur.jpg';
import DylanImage from '../../../assets/graphics/providerLandingPage/dylan.jpg';
import SaraImage from '../../../assets/graphics/providerLandingPage/Sara.jpg';
import StudentImage from '../../../assets/graphics/providerLandingPage/students.jpg';
import IQLogo from '../../../assets/graphics/providerLandingPage/iqLogo.svg';
import LeadsGenerationForm from "../../components/LeadGenerationForm/leadGenerationForm";


import endPointConfig, { cityPropertyListing, featuredCities } from "../../endpoints/index";
import {mainListing} from '../../actions/index';

const axios = require("axios");

class ProviderPage extends Component {
    state ={
        featuredCities :[],
        propertyLists : [],
        selectedCity: "",
        selectedTestimonial : "1",
        selectedCityCountry : "",
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getPropertyListing();
        this.getCities();
    }

    scrollToForm = () => {
        $('html, body').animate({
            scrollTop: $("#formSection").offset().top - 100
        }, 1000);
    };

    scrollToProperties = () => {
        $('html, body').animate({
            scrollTop: $("#propertiesSection").offset().top - 100
        }, 1000);
    };

    viewNextTestimonial = () => {
        this.setState({
            selectedTestimonial : (parseInt(this.state.selectedTestimonial) + 1).toString()
        })
    };

    viewPreviousTestimonial = () => {
        this.setState({
            selectedTestimonial : (parseInt(this.state.selectedTestimonial) - 1).toString()
        })
    };

    openProperties = (e) => {
        this.setState({
            selectedCity: e.target.id.split("/")[1],
            selectedCityCountry : e.target.id
        }, () =>{
            this.getPropertyListing();
        })
    };

    getCities = () => {
        axios.get(endPointConfig.featuredCities)
        .then((res) => {
            if(res.status === 200) {
                this.setState({
                    featuredCities : res.data.splice(0,3),
                }, () => {
                    this.setState({
                        selectedCity : this.state.featuredCities[0].slug,
                        selectedCityCountry: this.state.featuredCities[0].country_slug + "/" + this.state.featuredCities[0].slug
                    }, () => {
                        this.getPropertyListing();
                    })
                }) 
            }
        })
    };
   
    getPropertyListing = () => {
        axios.get(endPointConfig.cityPropertyListing + this.state.selectedCity)
            .then((res) => {
                if (res.status === 200 && res.data.length > 0) {
                    this.setState({
                        propertyLists: res.data.splice(0,4)
                    })
                } else if (res.status === 200 && res.data.length === 0) {
                    this.setState({
                        noPropertyMessage: "No Property found for your searched result/specification."
                    })
                } else return null;
               
            })
    } ;
    
    render() {
        return (
           <div className="provider_page_wrapper">
                 <div className="providerPage_hero_section">
                    <div className="hero_image_wrapper">
                        <img src={LeadsBannerImage} alt="HomeinAbroad"/>
                    </div>
                    <div className="hero_offer_wrapper">
                        <p className="offer_title">Offer on IQ Properties</p>
                        <div className="offer_wrapper">
                        <img src={OfferIcon}/> <p> Get £100 Cashback + Free Bedding Pack </p></div>
                        <div className="offer_wrapper">
                        <img src={OfferIcon}/> <p> Get £100 Cashback + Free Bedding Pack </p></div>
                        <div className="offer_wrapper">
                        <img src={OfferIcon}/> <p> Get £100 Cashback + Free Bedding Pack </p></div>
                        <div className="offer_wrapper">
                        <img src={OfferIcon}/> <p> Get £100 Cashback + Free Bedding Pack </p></div>
                    </div>
                    <div className="hero_content_wrapper">
                        <div className="name_wrapper">
                        <img src={IQLogo}></img>
                        <p className="property_title">  Accommodation</p>
                        </div>
                        <hr></hr>
                        <p className="property_detail"> Student Accommodation in UK</p>
                     
                        <button onClick={this.scrollToForm} id="bookButton" className="btn btn-success book_button">Book Accommodation</button>
                      
                    </div>
                    <div className="colored_wrapper">
                        <p>It's your space. Creating safe, welcoming homes</p>
                        <button onClick={this.scrollToProperties} id ="findMore">Find out more</button>
                    </div>
                </div>
                
                <div className="container">
                    <div className="reasons_wrapper">
                        <p className="reason_title">Three reasons</p>
                        <p className="reason_subheading">to feel at home</p>
                    </div>

                    <div className="reason_description_wrapper">
                    <div className="row">  
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div className="reason_content">
                                <p className="reason_content_title">Safe and secure</p>
                                <p className="reason_content_description">
                                    Your IQ home benefits from 24-hour on site staff,secure entry systems
                                    and dedicated maintenance and housekeeping teams
                                </p>
                            </div>
                            </div>

                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div className="reason_content">
                                <p className="reason_content_title">Close and convenient</p>
                                <p className="reason_content_description">
                                    Our locations are close o the university, so you will be usually
                                    be within walking distance of campus
                                </p>
                            </div>
                            </div>

                            <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div className="reason_content">
                                <p className="reason_content_title">Everything included</p>
                                <p className="reason_content_description">
                                    All inclusive bills and useful onsite facilities keep life hassle-free
                                </p>
                            </div>
                            </div>
                        </div>
                        </div>

                        <hr className="first_border"></hr>
                        <hr className="second_border"></hr>

                        <div className="neighbourhood_wrapper">
                            <p className="neighbourhood_title">Great neighbourhoods  </p>
                            <p className="neighbourhood_subheading">to make your own</p>
                            <p className="neighbourhood_description">
                                There are more than 50 IQ buildings located close to universities in 
                                27 UK cities. <br></br>
                                Find out what else is nearby in our student city guides.
                            </p>
                        </div>

                        <div id= "propertiesSection" className="city_properties_wrapper">
                            <div className="row">
                            <div className="cities_wrapper">
                                 {
                                this.state.featuredCities.length > 0 ?
                                this.state.featuredCities.map((city, i) => {
                                    return(
                                        <ul>
                                            <div className="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                                                    <div className="city_name_and_country">
                                                    <li className= {this.state.selectedCity === city.slug ? "selected_city" : "" } onClick={this.openProperties}  id={city.country_slug + "/" + city.slug} >{city.name}
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    )
                                }) : "Loading"
                        }
                                       
                                  
                        </div>
                        </div>

                        <div className="properties_wrapper">
                            <div className="row">
                        {
                             this.state.propertyLists.length > 0 &&
                             this.state.propertyLists.map((property, i) => {
                                return(
                                    <a href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug} target="blank">
                                        <div key={i} className="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                            <div className="single_property_wrapper">
                                            <img src={property.first_image} alt={property.name}/>
                                                <div className="property_description_wrapper">
                                                <StarRatingComponent value={property.rating} editing={false} />
                                                    <p className="property_name">{property.name} </p>
                                                    <p className="property_place">{property.address}</p>
                                                    <p className="property_min_price">starts at 
                                                    <span>{property.price_unit + property.min_bed_pricing + "/week"}</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                )
                            })
                        }
                        </div>
                        </div>
                        <a target="blank"  href={"/" + this.state.selectedCityCountry}>
                        <button className="search_more_properties" >View More Properties </button>
                        </a>
                        </div>

                       <hr className="first_underline"></hr>
                       <hr className="second_underline"></hr>

                       <div className="testemonial_section">
                                    <div className="testemonial_title">
                                        <p>Hear it from</p>
                                        <p>our residents</p>
                                    </div> 
                                    
                                    <div className="testemonial_box">
                                    <div className="row">

                                    {this.state.selectedTestimonial === "1" ? 
                                    <div id="1">
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile"> 
                                    <img className="students_image" src={JennyImage}></img> </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">  
                                          <p className="testemonial_content">
                                           When I was working in the studio until late,or working
                                          on my job,I would come back and the receptionist would be really
                                          friendly, and it felt really safe and homely coming back to that 
                                          </p>
                                          <p className="student_name">Jenny</p>
                                          <p className="student_country">Marsden House</p>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">  
                                        <img className="students_image" src={JennyImage}></img>
                                        </div>
                                        </div> : null }
                                        {this.state.selectedTestimonial === "2" ? 
                                    <div id="1">
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile"> 
                                    <img className="students_image" src={SaraImage}></img> </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">  
                                        <p className="testemonial_content">
                                        iQ helped me by the community in the building… 
                                        I was able to share my food with everyone.
                                        </p>
                                        <p className="student_name">Sara</p>
                                        <p className="student_country">Bloomsbury</p>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">  
                                        <img className="students_image" src={SaraImage}></img>
                                        </div>
                                        </div> : null }
                                    {this.state.selectedTestimonial === "3" ? 
                                    <div id="1">
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile"> 
                                    <img className="students_image" src={DylanImage}></img> </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">  
                                        <p className="testemonial_content">
                                        I’m two minutes away from pretty much everything, two minutes away from the university, 
                                        from the local amenities, from my work.
                                        </p>
                                        <p className="student_name">Dylan</p>
                                        <p className="student_country">IQ, Bristol</p>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">  
                                        <img className="students_image" src={DylanImage}></img>
                                        </div>
                                        </div> : null }
                                        {this.state.selectedTestimonial === "4" ? 
                                    <div id="1">
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile"> 
                                    <img className="students_image" src={NupurImage}></img> </div>
                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">  
                                        <p className="testemonial_content">
                                        I chose iQ Shoreditch because it was literally a 5-minute walk from my uni. 
                                        I saw the rooms, they were spacious,
                                         the view was really nice, I could see The Shard from my room.
                                        </p>
                                        <p className="student_name"> Nupur</p>
                                        <p className="student_country">Shoreditch</p>
                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">  
                                        <img className="students_image" src={NupurImage}></img>
                                        </div>
                                        </div> : null }
                                        <div className="previous_next_wrapper">
                                        <button disabled={this.state.selectedTestimonial === "1"} onClick={this.viewPreviousTestimonial} className={" previous_testemonial " + (this.state.selectedTestimonial === "1" ? "disabled_style" : "")} ></button>
                                        <button disabled={this.state.selectedTestimonial === "4"} onClick={this.viewNextTestimonial} className={"next_testemonial " + (this.state.selectedTestimonial === "4" ? "disabled_style" : "")} ></button>
                                        </div>
                               </div>
                           </div>
                       </div>

                       <div id= "formSection" className="contact_us_wrapper">
                           <div className="contact_heading_section">
                               <div className="upper_heading">
                                <p>Want apartment of your choice?</p>
                                <p>Get response within 1 hour</p>
                                </div>
                                <div className="quality_wrapper">
                                <p className="quality_box">
                                    <img src={CheckBoxIcon}></img>
                                    24*7 Personal Assistance
                                </p>
                                <p className="quality_box">
                                <img src={CheckBoxIcon}></img>
                                   Verified Property
                                </p>
                                <p className="quality_box">
                                <img src={CheckBoxIcon}></img>
                                  Free Service
                                </p>
                                </div>
                                <p className="lower_heading">Just give your prefernces & get best apartments for your stay
                                    without any hassle.
                                </p>
                           </div> 
                           <div className="form_wrapper">
                           <LeadsGenerationForm/>
                           </div>
                       </div>
                     

                       <div className="about_us_section">
                           <div className="row">
                               <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                   <p className="about_us_heading">
                                        About Home In Abroad
                                   </p>
                                   <p className="about_us_content">
                                        Are you going to study abroad and you need the student 
                                        accommodation where you could spend a memorable time during
                                        your years of study? If yes, you have reached the right
                                        destination.HomeinAbroad assists the students in finding 
                                        the verified accommodation within walking distance from the 
                                        university or as per their priorties.HomeinAbroadm provides 
                                        student accommodation in over 203 education hubs. <br></br>
                                        <br></br>
                                        <span>Facilities </span> <br></br> <br></br>
                                        Students can filter the properties according to their requirements
                                        at HomeInAbroad. HomeInAbroad faciliates them to select the homes
                                        on the basis of distance from the university campus, city center, or
                                        type of location they want.On the other hand, each student accommodation is 
                                        well connected to the public transport system. Moreover, the students
                                        find everything required in today's scenario inside the properties.
                                        They find water, gas, electricity and Wi-Fi and the charges are 
                                        inclusive of all the aforesaid facilities.   

                                   </p>
                               </div>
                               <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                   <img src={StudentImage}></img>
                                </div>
                           </div>
                       </div>
                </div>
           </div>
        )
    }
}
function mapStateToProps(state) {
    return {cityDetails: state.cityDetails};
}

function loadData(store) {
    return store.dispatch(mainListing());
}
export default {
    loadData,
    component: withRouter(connect(mapStateToProps, {mainListing})(ProviderPage))
};
import React, {Component} from "react";


import BannerImage from "../../../assets/graphics/howItWorksPage/how-it-works-banner.jpg";
import RoomIcon from "../../../assets/graphics/howItWorksPage/step-process-1.png";
import BookRoomIcon from "../../../assets/graphics/howItWorksPage/step-process-2.png";
import ContractSignIcon from "../../../assets/graphics/howItWorksPage/step-process-3.png";
import BlueIcon from "../../../assets/graphics/howItWorksPage/blue-icon.png";
import PinkIcon from "../../../assets/graphics/howItWorksPage/pink-icon.png";
import StudentFirst from "../../../assets/graphics/howItWorksPage/student-first.svg";
import StudentSecond from "../../../assets/graphics/howItWorksPage/student-second.svg";
import StudentThird from "../../../assets/graphics/howItWorksPage/student-third.svg";
import StudentFourth from "../../../assets/graphics/howItWorksPage/student-fourth.svg";


class HowItWorksPage extends Component {
  
    render() {
        return (
          <div className="how_it_works_wrapper">
             <div className="how_it_works_heroSection">
                 <div className="hero_image_wrapper">
                  <img src={BannerImage} alt="HomeinAbroad"/>
                 </div>
              </div>

              <div className="how_it_works_section">
              <div className="container"> 
                   <div className="main_content">
                     <div className="row">
                  
                     <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile">
                             <div className="image_section">
                             <img src={RoomIcon} alt="Select your room"/>
                             </div>
                         </div>
                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="content_section">
                              <div className="content_sub_section">
                                 <img src={PinkIcon}></img>
                                 <p className="content_headline"> Select your room </p>
                                 </div>
                                 
                                 <p className="content_description">Enter your University, locality or property
                                name and choose between various options
                                based on your budget preferences, amenity requirements etc.</p>
                             </div>
                         </div>
                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">
                             <div className="image_section">
                             <img src={RoomIcon} alt="Select your room"/>
                             </div>
                         </div>
                     </div>
                     </div>
               
                     <div className="main_content">
                     <div className="row">
                   
                     <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="image_section">
                             <img src={BookRoomIcon} alt="Click on Book Now"/>
                             </div>
                         </div>
                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="content_section">
                                 <div className="content_sub_section">
                                 <img src={BlueIcon}></img>
                                 <p className="content_headline"> Click on Book Now </p>
                                 </div>
                                 <p className="content_description">When you find your room of choice, make a
                                   request to book and fill in the required details after which our booking experts will call you and
                                   guide you on the next steps, all for free.Alternatively, you can also put down the deposit on the portal itself.</p>
                             </div>
                         </div>
                       
                         </div>
                     </div>

                     <div className="main_content">
                     <div className="row">
                   
                     <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_mobile">
                             <div className="image_section">
                             <img src={ContractSignIcon} alt="Sign Contract"/>
                             </div>
                         </div>
                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                             <div className="content_section">
                             <div className="content_sub_section">
                             <img src={PinkIcon}></img>
                             <p className="content_headline"> Sign Contract </p>
                             </div>
                                 <p className="content_description">Digitally Sign your contract on our portal and
                                    you are ready to move it. You will pay your rent directly to the property.
                                    The services that we provide are completely free of cost.</p>
                             </div>
                         </div>
                         <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 show_desktop">
                             <div className="image_section">
                             <img src={ContractSignIcon} alt="Sign Contract"/>
                             </div>
                         </div>
                     </div>
                     </div> 
                   

              <div className="ending_section">
                  <p className="ending_title">Apartment is booked now</p>
                  <p className="ending_subtitle">The room is now yours. Enjoy your journey</p>
                
            
              <div className="query_wrapper">
                    <p className="query_heading"> feel free to contact</p>
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <p className="email_section"> Email <br></br>
                                support@homeinabroad.com</p>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <p className="call_section"> Call <br></br> 
                                +44-2086388957 <span>/ </span>+61-280069159 </p>
                            </div>
                        </div>   
                    </div>
                    <div className="student_image_wrapper show_desktop">
                            <img className="first_image" src={StudentFirst}></img>
                            <img className="second_image" src={StudentSecond}></img>
                            <img className="third_image" src={StudentThird}></img>
                            <img className="fourth_image" src={StudentFourth}></img>
                            <img className="fifth_image" src={StudentFirst}></img>
                            <img className="sixth_image" src={StudentSecond}></img>
                            <img className="seventh_image" src={StudentThird}></img>
                        </div>
                        </div>
                    </div>
                    </div>
          </div>
        )
    }
}


export default {
    component: HowItWorksPage
};
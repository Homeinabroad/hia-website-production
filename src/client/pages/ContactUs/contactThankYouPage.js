import React, { Component } from "react";

class ContactThankYouPage extends Component {

    redirectBackToContactPage = () => {
        window.location.assign(document.referrer);
    };

    render() {
        return(
            <div className="thankyou_page_template">
                <p>Thank you for reaching out to us. We've received your query.</p>
                <p>One of our experts will get back to you soon.</p>
                <button onClick={this.redirectBackToContactPage} className="btn btn-primary">EXPLORE MORE</button>
            </div>
        )
    }
}

export default {
    component : ContactThankYouPage
};
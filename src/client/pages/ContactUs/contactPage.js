import React, {Component} from 'react';

import ContactBannerImage from '../../../assets/graphics/contact/Contact-Banner.png';
import UKIcon from "../../../assets/graphics/contact/uk.jpg";
import AustraliaIcon from "../../../assets/graphics/contact/australia.jpg";
import CallIcon from "../../../assets/graphics/contact/Call.svg";
import WhatsAppIcon from "../../../assets/graphics/contact/whatsapp.svg";
import endPointConfig from "../../endpoints/index";
import CountryCodeDropdown from "../../components/CountryCode/CountryCode";

const axios = require('axios');

class ContactPage extends Component {
    state = {
        userData: {
            firstName: "",
            lastName: "",
            email: "",
            contactNumber: "",
            message: ""
        },
        contactCountryCode: "",
        errorMessage: "",
        disableButton: false
    };

    handleFormValueChange = (e) => {
        let obj = this.state.userData;
        obj[e.target.id] = e.target.value;
        this.setState({
            userData: obj
        })
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

    validateContactForm = () => {
        if (this.state.userData.firstName.trim() === "")    {
            this.setState({
                errorMessage: "Please enter your first name.",
                disableButton: false
            })
            return false
        } else if (this.state.userData.lastName.trim() === "") {
            this.setState({
                errorMessage: "Please enter your last name.",
                disableButton: false
            })
            return false
        } else if (this.state.userData.email.trim() === "") {
            this.setState({
                errorMessage: "Please enter your Email Address.",
                disableButton: false
            })
            return false
        } else if (this.state.userData.contactNumber.trim() === "") {
            this.setState({
                errorMessage: "Please enter your Contact Number.",
                disableButton: false
            })
            return false
        } else if (this.state.userData.message.trim() === "") {
            this.setState({
                errorMessage: "Please enter your Message.",
                disableButton: false
            })
            return false
        } else return true
    };

    submitContactForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage: "",
            disableButton: true
        })
        if (this.validateContactForm() === true) {
            let userDataObjBody = {
                first_name: this.state.userData.firstName,
                last_name: this.state.userData.lastName,
                email: this.state.userData.email,
                contact_number: this.state.contactCountryCode + "-" + this.state.userData.contactNumber,
                message: this.state.userData.message,
                lead_source: "Contact-US Page-Website"
            }
            axios({
                method: 'POST',
                url: endPointConfig.leadGeneration,
                data: userDataObjBody
            })
                .then((res) => {
                    if (res.status === 201) {
                        this.setState({
                            errorMessage: "",
                            disableButton: false
                        })
                        window.location.assign("/contact/thank-you");
                    } else {
                        this.setState({
                            errorMessage: "Oops! Something went wrong. Please try again later.",
                        })
                    }
                    
                })
                .catch((err) => {
                    if(err) {
                    var errorMessage;
                   for(const error in err.response.data){
                       errorMessage= (`${err.response.data[error]}`)
                   }
                        this.setState({
                            errorMessage : errorMessage, 
                            disableButton: false
                        })
                    }
                });
        }
    };

    render() {
        return (
            <div className="contact_page">
                <div className="form_and_contact_details_wrapper">
                    <img src={ContactBannerImage} className="contact_banner" alt="Contact | HomeinAbroad"/>
                    <div className="contact_details_wrapper">
                         <p className="contact_details_headline">We have our presence through out the globe</p>
                        <div className="single_country_address">
                      <img src={UKIcon} alt="United Kingdom"/> 
                            <p className="country_name">United Kingdom</p>
                        <p className="country_address">30 Stamford St, South Bank, London SE1 9LQ, United Kingdom
                                    </p>
                            <p className="mobile_number">
                                <img src={CallIcon} alt="Call"/>
                                +44-2086388957
                            </p>
                            <p className="whatsapp_number">
                                <img src={WhatsAppIcon} alt="WhatsApp"/>
                                +44-2086388957
                            </p> 
                        </div>

                        {/* <div className="single_country_address">
                      <img src={IndiaIcon}/> 
                            <p className="country_name">India</p>
                          <p className="country_address">QWER, TYUIO, UITYPIUYHHHJHUGG, London, United Kingdom</p>
                            <p className="mobile_number">
                                <img src={CallIcon}/>
                                +44-234567890
                            </p>
                            <p className="whatsapp_number">
                                <img src={WhatsAppIcon}/>
                                +44-234567890
                            </p> 
                        </div> */}

                        <div className="single_country_address">
                            <img src={AustraliaIcon}/> 
                            <p className="country_name">Australia</p>
                      <p className="country_address">126, Holden Drive, Oran Park
                        Sydney, New South Wales 2570,
                            Australia</p>
                            <p className="mobile_number">
                                <img src={CallIcon}/>
                                +61-280069159
                            </p>
                            <p className="whatsapp_number">
                                <img src={WhatsAppIcon}/>
                                +61-280069159
                            </p> 
                        </div> 
                    </div>

                    <div className="form_wrapper">
                        <p>Please Enter your details and message.</p>
                        <form onSubmit={this.submitContactForm}>
                            <div className="single_field_wrepper">
                                <label>YOUR FIRST NAME</label>
                                <input type="text" id="firstName" pattern="[a-zA-Z]{1,}" required={true}
                                       onChange={this.handleFormValueChange}/>
                            </div>

                            <div className="single_field_wrepper">
                                <label>YOUR LAST NAME</label>
                                <input type="text" id="lastName"  pattern="[a-zA-Z]{1,}" required={true} onChange={this.handleFormValueChange}/>
                            </div>

                            <div className="single_field_wrepper">
                                <label>YOUR EMAIL ADDRESS</label>
                                <input type="text" id="email" required={true} onChange={this.handleFormValueChange}/>
                            </div>

                            <div className="single_field_wrepper">
                                <label>YOUR CONTACT NUMBER</label>
                                <div className="contact_number_wrapper">
                                    <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)}/>
                                    <input type="number" id="contactNumber" required={true}
                                           onChange={this.handleFormValueChange}/>
                                </div>
                            </div>

                            <div className="single_field_wrepper">
                                <label>YOUR MESSAGE FOR US</label>
                                <textarea type="text" id="message" required={true}
                                          onChange={this.handleFormValueChange}></textarea>
                            </div>
                            {this.state.errorMessage !== "" ?
                                <p className="error_message">{this.state.errorMessage}</p> : null}
                            <button disabled={this.state.disableButton} className="btn btn-primary">SUBMIT QUERY
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component: ContactPage
};
import React, {Component} from "react";

import CountryCodeDropdown from "../../components/CountryCode/CountryCode";
import endPointConfig from "../../endpoints";

import BannerImage from '../../../assets/graphics/referAndEarnPage/refer-and-earn.jpg';

const axios = require("axios");

class ReferAndEarnPage extends Component {
    state = {
        showThankYouPopup : false,
        contactCountryCode : "",
        disableButton: false,
        errorMessage : "",
        userData : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            message : "",
            friendFirstName : "",
            friendLastName : "",
            friendEmail : "",
            friendContactNumber : "",
            friendUniversity : "",
            friendCityofStudy : "",
        }
    };

    validateReferAndEarnForm = () => {
        if(this.state.userData.firstName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your first name.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.lastName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your last name.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.email.trim() === "") {
            this.setState({
                errorMessage : "Please enter your EMail Address.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.contactNumber.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Contact Number.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.message.trim() === "") {
            this.setState({
                errorMessage : "Please enter your message.",
                disableButton: false,
            })
            return false
        }
        if(this.state.userData.friendFirstName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your friend first name.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.friendLastName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your friend last name.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.friendEmail.trim() === "") {
            this.setState({
                errorMessage : "Please enter your friend EMail Address.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.friendContactNumber.trim() === "") {
            this.setState({
                errorMessage : "Please enter your friend Contact Number.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.friendCityofStudy.trim() === "") {
            this.setState({
                errorMessage : "Please enter your friend City of Study.",
                disableButton: false,
            })
            return false
        }
        else if(this.state.userData.friendUniversity.trim() === "") {
            this.setState({
                
                errorMessage : "Please enter your friend University of Study.",
                disableButton: false,
            })
            return false
        }
        // else if(this.state.userData.contactNumber.trim() === this.state.userData.friendContactNumber.trim()) {
        //     this.setState({
        //         errorMessage : "Contact Number should not match.",
        //         disableButton: false,
        //     })
        //     return false
        // }
        // else if(this.state.userData.email.trim() === this.state.userData.friendEmail.trim()) {
        //     this.setState({
        //         errorMessage : "EmailId should not match.",
        //         disableButton: false,
        //     })
        //     return false
        // }
        else return true;
    };

    
    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

   

    handleFieldValueChange = (e) => {
        let obj = {...this.state.userData};
        obj[e.target.id] = e.target.value;
        this.setState({
            userData : obj
        })
    };

    closeThankYouPopup = () => {
        this.setState({
            showThankYouPopup : false
        })
    };

    submitReferAndEarnFormData = (e) =>{
        e.preventDefault();
        this.setState({
            errorMessage : "",
        });
        if(this.validateReferAndEarnForm() === true){
            let objBody = {
                referrer_first_name : this.state.userData.firstName,
                referrer_last_name : this.state.userData.lastName,
                referrer_email : this.state.userData.email,
                referrer_contact_number : this.state.userData.contactNumber,
                message : this.state.userData.message, 
                friend_first_name : this.state.userData.friendFirstName,
                friend_last_name : this.state.userData.friendLastName,
                friend_email : this.state.userData.friendEmail,
                friend_contact_number : this.state.userData.friendContactNumber,
                friend_city_of_study : this.state.userData.friendCityofStudy,
                friend_university : this.state.userData.friendUniversity
            };
            axios({
                method: 'POST',
                url : endPointConfig.referAndEarn,
                data : objBody,
                headers : {'Content-Type' : 'application/json'}
            })
            .then((res) =>{
                if(res.status===201){
                    this.setState({
                        disableButton: false,
                            showThankYouPopup : true,
                            errorMessage : "",
                            userData : {
                                firstName : "",
                                lastName : "",
                                email : "",
                                contactNumber : "",
                                message : "",
                                friendFirstName : "",
                                friendLastName : "",
                                friendEmail : "",
                                friendContactNumber : "",
                                friendCityofStudy : "",
                                friendUniversity : "",
                            },
                            contactCountryCode : "",
                            
                    })
                }
                else{
                    this.setState({
                        errorMessage : "Oops! Something went wrong. Please try again.",
                        disableButton : false,
                    })
                }
            })
        }
    };
    
    render() {
        return (
          <div className="refer_and_earn_wrapper">
                {
                    this.state.showThankYouPopup ?
                    <div className="popup_backdrop">
                        <div className="popup_contents">
                            <p>Thank you! Your details have been submitted.</p>
                            <p>One of our accommodation expert will get back to you very soon.</p>
                            <button className="pink_button" onClick={this.closeThankYouPopup}>OK</button>
                        </div>
                    </div>
                    : null
                }
             <div className="refer_and_earn_heroSection">
                 <div className="hero_image_wrapper">
                  <img src={BannerImage} alt="HomeinAbroad"/>
                 </div>
              </div>

              <div className="container">
                  <div className="refer_form_wrapper">
                  
                      <p className="form_heading_section" >Enter your and your referral's details and earn money by getting
                          referral points <br></br>
                          <span> More you refer More you earn</span>
                      </p>
                    
                      <form onSubmit={this.submitReferAndEarnFormData}>
                    <p className="person_heading">Enter your details</p>
                      <div className="single_form_field">
                        <label>FIRST NAME</label>
                        <input type="text" value={this.state.userData.firstName} id="firstName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>LAST NAME</label>
                        <input type="text" value={this.state.userData.lastName} id="lastName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>EMAIL ADDRESS</label>
                        <input type="email" value={this.state.userData.email} id="email" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>CONTACT NUMBER</label>
                        <div className="contact_number_wrapper">
                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)} />
                            <input type="number" value={this.state.userData.contactNumber} id="contactNumber" onChange={this.handleFieldValueChange} required />
                        </div>
                    </div>

                    <div className="single_form_field">
                        <label>MESSAGE</label>
                        <textarea type="text" value={this.state.userData.message} id="message" rows="6" cols = "50" onChange={this.handleFieldValueChange} required />
                    </div>
                  
                    <hr></hr>

                    <p className="referral_heading">Enter your referral details</p>
                    <div className="single_form_field">
                        <label>FIRST NAME</label>
                        <input type="text" value={this.state.userData.friendFirstName} id="friendFirstName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>LAST NAME</label>
                        <input type="text" value={this.state.userData.friendLastName} id="friendLastName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>EMAIL ADDRESS</label>
                        <input type="email" value={this.state.userData.friendEmail} id="friendEmail" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>CONTACT NUMBER</label>
                        <div className="contact_number_wrapper">
                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)} />
                            <input type="number" value={this.state.userData.friendContactNumber} id="friendContactNumber" onChange={this.handleFieldValueChange} required />
                        </div>
                    </div>

                    <div className="single_form_field">
                        <label>City of Study</label>
                        <input type="text" value={this.state.userData.friendCityofStudy} id="friendCityofStudy" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>UNIVERSITY GOING FOR STUDY</label>
                        <input type="text" value={this.state.userData.friendUniversity} id="friendUniversity" onChange={this.handleFieldValueChange} required />
                    </div>
                    <button disabled={this.state.disableButton} className="btn btn-primary">SUBMIT</button>
                      </form>
                  </div>
              </div>

          </div>
        )
    }
}


export default {
    component: ReferAndEarnPage
};
import React, { Component } from "react";

import CommonThankYouMessage from "../../components/CommonThankYouMessage/CommonThankYouMessage";

class BookingThankYouPage extends Component {

    redirectBackToHomePage = () => {
        window.location.assign(document.referrer);
    };

    render() {
        return(
            <div className="thankyou_page_template">
                <CommonThankYouMessage />
            </div>
        )
    }
}

export default {
    component : BookingThankYouPage
};
import React, { Component } from 'react';
import moment from 'moment';
import { withRouter } from "react-router-dom";

import NationalityDropdown from "../../components/NationalityDropDown/NationalityDropDown";
import CountryCodeDropdown from "../../components/CountryCode/CountryCode";
import endPointConfig from "../../endpoints";
const axios = require('axios');

class BookingForm extends Component {
    state = {
        selectedPriceData : null,
        universityList : [],
        userDetails : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            dateOfBirth : ""
        },
        contactCountryCode : "",
        yearOfStudy : "",
        nationality : "",
        userGender : "",
        errorMessage : "",
        disableButton : false,
        universityToAttend : ""
    };

    componentDidMount() {
        window.scrollTo(0,0);
        this.setState({
            selectedPriceData : JSON.parse(sessionStorage.getItem("selectedPrice"))
        }, () => {
            this.getUniversityList();
        })
    }

    getUniversityList = () => {
        axios.get(endPointConfig.cityUniversities + this.state.selectedPriceData.citySlug)
            .then((res) => {
                let uniArray = [];
                res.data.map((uni) => {
                    return(
                        uniArray.push({name : uni.name, id : uni.id})
                    )
                })
                this.setState({
                    universityList : uniArray
                })
            })
    };

    handleInputValueChange = (e) => {
        let obj = this.state.userDetails;
        obj[e.target.id] = e.target.value;
        this.setState({
            userDetails : obj
        })
    };

    handleGenderSelection = (e) => {
        this.setState({
            userGender : e.target.id
        })
    };

    handleYearOfStudySelection = (e) => {
        this.setState({
            yearOfStudy : e.target.value
        })
    };

    handleUniversitySelection = (e) => {
        this.setState({
            universityToAttend : parseInt(e.target.value)
        })
    }

    setNationality = (nationality) => {
        this.setState({
            nationality: nationality
        });
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

    validateBookingForm = () => {
        if(this.state.userDetails.firstName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your First Name.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.userDetails.lastName.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Last Name.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.userDetails.email.trim() === "") {
            this.setState({
                errorMessage : "Please enter your EMail Address.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.userDetails.contactNumber.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Contact Number.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.userGender.trim() === "") {
            this.setState({
                errorMessage : "Please select your Gender.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.userDetails.dateOfBirth.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Date of Birth.",
                disableButton : false
            })
            return false;
        }

        else if(this.state.nationality.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Nationality.",
                disableButton : false
            })
            return false;
        }

        else return true;
    };

    submitBookingForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : "",
            disableButton : true
        })
        if(this.validateBookingForm() === true) {
            let userData = {
                first_name : this.state.userDetails.firstName,
                last_name : this.state.userDetails.lastName,
                gender : this.state.userGender,
                email : this.state.userDetails.email,
                contact_number : this.state.contactCountryCode + "-" + this.state.userDetails.contactNumber,
                dob : this.state.userDetails.dateOfBirth,
                nationality : this.state.nationality,
                year_of_study : this.state.yearOfStudy,
                university : this.state.universityToAttend,
                property : this.state.selectedPriceData.propertyId,
                lead_source : "Booking Page - Website",
                room_type : this.state.selectedPriceData.roomTypeId,
                bed_price : this.state.selectedPriceData.tenancyId,
                check_in_date : this.state.selectedPriceData.checkInDate,
                checkout_date : this.state.selectedPriceData.checkOutDate
            }
            axios({
                method: 'POST',
                url: endPointConfig.guestBooking,
                data: userData
            })
                .then((res) => {
                    if(res.status === 201) {
                        window.location.assign(this.props.location.pathname + "/thank-you");
                    }
                    else {
                        this.setState({
                            errorMessage : "Oops! Something went wrong. Please try again.",
                            disableButton : false,
                        })
                    }
                })
        }
    };

    render(){
        let universityDropdown = (
            this.state.universityList.length > 0 ?
                this.state.universityList.map((uni, i) => {
                    return(
                        <option key={i} value={uni.id}>{uni.name}</option>
                    )
                }) : null
        );
        return(
            <div className="booking_form_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div className="form_wrapper">
                                <h3>Reserve Now Pay Later!</h3>
                                <form onSubmit={this.submitBookingForm} className="form-group">
                                    <div className="field_wrapper">
                                        <label>First Name</label>
                                        <input type="text" required onChange={this.handleInputValueChange} id="firstName" className="form-control" />
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Last Name</label>
                                        <input type="text" required onChange={this.handleInputValueChange} id="lastName" className="form-control" />
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Email</label>
                                        <input type="text" required onChange={this.handleInputValueChange} id="email" className="form-control" />
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Contact Number</label>
                                        <div className="contact_field_wrapper">
                                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)}/>
                                            <input type="number" required onChange={this.handleInputValueChange} id="contactNumber" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Gender</label>
                                        <div className="gender_wrapper">
                                            <div onClick={this.handleGenderSelection} id="male" className={this.state.userGender === "male" ? "selected_gender" : ""}>Male</div>
                                            <div onClick={this.handleGenderSelection} id="female" className={this.state.userGender === "female" ? "selected_gender" : ""}>Female</div>
                                            <div onClick={this.handleGenderSelection} id="others" className={this.state.userGender === "others" ? "selected_gender" : ""}>Others</div>
                                        </div>
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Date Of Birth</label>
                                        <input type="date" required onChange={this.handleInputValueChange} id="dateOfBirth" className="form-control" />
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Nationality</label>
                                        <NationalityDropdown  nationality={(e) => this.setNationality(e)} />
                                    </div>
                                    <div className="field_wrapper">
                                        <label>Year of Study</label>
                                        <select required={true} onChange={this.handleYearOfStudySelection}>
                                            <option selected disabled hidden>SELECT YOUR YEAR OF STUDY</option>
                                            <option value="1yr">First Year</option>
                                            <option value="2yr">Second Year</option>
                                            <option value="3yr">Third Year</option>
                                            <option value="4yr">Fourth Year</option>
                                            <option value="Int">Internship</option>
                                            <option value="phd">P.HD</option>
                                            <option value="mas">Masters</option>
                                        </select>
                                    </div>
                                    <div className="field_wrapper">
                                        <label>University going to attend</label>
                                        <select required={true} onChange={this.handleUniversitySelection}>
                                            <option disabled hidden selected>SELECT YOUR UNIVERSITY</option>
                                            {universityDropdown}
                                        </select>
                                    </div>
                                    {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                                    <button disabled={this.state.disableButton} className="btn btn-primary">RESERVE ROOM</button>
                                </form>
                            </div>
                        </div>

                        {
                            this.state.selectedPriceData ?
                                <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <div className="selected_property_details_wrapper">
                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Property</div>
                                            <div className="detail_value">{this.state.selectedPriceData.propertyName}</div>
                                        </div>

                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Room Category</div>
                                            <div className="detail_value">{this.state.selectedPriceData.roomCategoryName}</div>
                                        </div>

                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Room Type</div>
                                            <div className="detail_value">{this.state.selectedPriceData.roomTypeName}</div>
                                        </div>

                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Price</div>
                                            <div className="detail_value">{this.state.selectedPriceData.bedPrice}</div>
                                        </div>

                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Tenancy</div>
                                            <div className="detail_value">{moment(this.state.selectedPriceData.checkInDate).format('MMM d, YYYY')} till {moment(this.state.selectedPriceData.checkOutDate).format('MMM d, YYYY')} | {this.state.selectedPriceData.tenancyValue} {this.state.selectedPriceData.tenancyUnit}</div>
                                        </div>

                                        <div className="single_detail_wrapper">
                                            <div className="detail_key">Total Price</div>
                                            <div className="detail_value">{this.state.selectedPriceData.bedNumeralPriceValue * this.state.selectedPriceData.tenancyValue}</div>
                                        </div>
                                    </div>
                                </div>
                            : null
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default {
    component: withRouter(BookingForm)
};
import React, {Component} from "react";
import { withRouter } from "react-router-dom";

import endPointConfig from "../../endpoints";

const axios = require('axios');

class RetrievePasswordPage extends Component {
    state = {
        passwords : {
            password : "",
            rePassword : ""
        },
        disableButton : false,
        errorMessage : null,
        successMessage : null
    };

    handleInputValues = (e) => {
        let obj = {...this.state.passwords};
        obj[e.target.name] = e.target.value;
        this.setState({
            passwords : obj
        })
    };

    validatePassword = () => {
        if(this.state.passwords.password.trim() === "" || this.state.passwords.rePassword.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Passwords",
                disableButton : false,
            });
            return false;
        }
        else if (this.state.passwords.password.trim() !== this.state.passwords.rePassword.trim()) {
            this.setState({
                errorMessage : "Your passwords didn't match",
                disableButton : false,
            });
            return false;
        }
        else if (this.state.passwords.password.includes(" ") || this.state.passwords.rePassword.includes(" ")) {
            this.setState({
                errorMessage : "Passwords should not contain space characters.",
                disableButton : false,
            });
            return false;
        }
        else return true;
    };

    submitPasswordResetForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : null,
            disableButton : true,
        });
        if(this.validatePassword() === true) {
            let passwordResetBodyData = {
                password : this.state.passwords.password,
                token : this.props.location.search.split("=")[1]
            };
            axios({
                method : 'POST',
                url : endPointConfig.confirmPasswordReset,
                data : passwordResetBodyData,
                headers : {
                    'Content-Type': 'application/json'
                }
            })
                .then((res) => {
                    if(res.status === 200) {
                        this.setState({
                            successMessage : "Your password has been set successfully. You will be redirected to the login page."
                        }, () => {
                            setTimeout(() => {
                                this.setState({
                                    successMessage : null,
                                    forgotPasswordTemplate : false,
                                    errorMessage : "",
                                    registeredEmail : ""
                                }, () => {
                                    window.location.assign("/login");
                                })
                            },3000)
                        })
                    }
                })
                .catch((err) => {
                    if(err) {
                        this.setState({
                            errorMessage : err.response.data.password[0],
                            disableButton : false,
                        })
                    }
                })
        }
    };

    render() {
        return (
            <div className="retrieve_password_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="enter_password_wrapper">
                                <form onSubmit={this.submitPasswordResetForm}>
                                    <label>Enter Password</label>
                                    <input
                                        type="password"
                                        name="password"
                                        onChange={this.handleInputValues}
                                        required={true}
                                        value={this.state.passwords.password}
                                    />

                                    <label>Re-enter Password</label>
                                    <input
                                        type="password"
                                        name="rePassword"
                                        onChange={this.handleInputValues}
                                        required={true}
                                        value={this.state.passwords.rePassword}
                                    />
                                    {this.state.successMessage && <p className="success_message">{this.state.successMessage}</p>}
                                    {this.state.errorMessage && <p className="error_message">{this.state.errorMessage}</p>}
                                    <button disabled={this.state.disableButton} className="btn btn-primary">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component: withRouter(RetrievePasswordPage)
};
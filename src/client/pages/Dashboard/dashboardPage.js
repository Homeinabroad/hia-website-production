import React, {Component} from "react";

import endPointConfig from "../../endpoints";
import UserProfileImage from "../../../assets/graphics/userDashboardPage/user-profile.png";
import SearchNewBooking from "../../../assets/graphics/userDashboardPage/search-booking.svg";
import PersonalInformation from "../../../assets/graphics/userDashboardPage/personal-information.png";
import Settings from "../../../assets/graphics/userDashboardPage/Settings.png";
import UploadDocumentIcon from "../../../assets/graphics/userDashboardPage/upload-icon.png";
import PropertyImage from "../../../assets/graphics/userDashboardPage/propertyImage.jpg";
import DeleteIcon from "../../../assets/graphics/userDashboardPage/delete-icon.png";
import EditIcon from "../../../assets/graphics/userDashboardPage/edit-icon.svg";
import BookingIcon from "../../../assets/graphics/userDashboardPage/booking-icon.png";
import WishlistIcon from "../../../assets/graphics/userDashboardPage/wishlist-icon.png";
import AddDocumentIcon from "../../../assets/graphics/userDashboardPage/document-add.png";
import CountryCodeDropdown from "../../components/CountryCode/CountryCode";
import StarRatingComponent from 'react-star-rating-component';



const axios = require('axios');

class UserDashBoardPage extends Component {
    state={
        openSection : "bookings",
        changePasswordBox: false,
        personalInformationSection : false,
        openDocuments : false,
        openEditContactNumber : false,
        attachFile : false,
        passwords : {
            oldPassword : "",
            password : "",
            confirmPassword : ""
        },
        disableButton : false,
        errorMessage : null,
        successMessage : null,
        userId : null,
        userData : [],
        userFile : "",
        searchBar : false,
        userUpdateId: "",
        contactNumberUpdation : "",
        countryCodeUpdation : "",
    }

    componentDidMount() {
          if (sessionStorage.getItem("token")) {
              let token = sessionStorage.getItem("token");
              let userId = sessionStorage.getItem("userId");
              this.setState({
                  accessToken: token,
                  userId: userId,
              }, () => {
                  this.getUserDetails();
              })
          }
      
    }

   

      getUserDetails = () => {
         axios({
              method: 'GET',
              url: endPointConfig.userDashboard,
              headers: {
                  'Content-Type': 'application/json',
                  'Authorization' : "Bearer " + this.state.accessToken
              }
          })
          .then((res) => {
              if (res.status === 200) {
                  this.setState({
                     userData: res.data,
                     userUpdateId : res.data[0].id,
                  })
              }
          })
      };

      getUserDetailsUpdate = (e) => {
        e.preventDefault();
        this.setState({
            disableButton : true,
        })
        let userDataUpdateObjBody = {
            contact_number : this.state.countryCodeUpdation+ "-" +this.state.contactNumberUpdation 
        };
      axios({
           method: 'PATCH',
           url: endPointConfig.userDashboard + this.state.userUpdateId + "/",
           data : userDataUpdateObjBody,
           headers : {
               'Content-Type': 'application/json',
               'Authorization' : "Bearer " + this.state.accessToken
               
           }
       })
       .then((res) => {
           if (res.status === 200) {
               this.setState({
                 successMessage : "Your Contact Number has been Updated",
                 disableButton : false
               },() => {
                  setTimeout(() => {
                      this.setState({
                          successMessage : null,
                          errorMessage : "",
                          userDataUpdate : {
                          contactNumberUpdation: "",
                          countryCodeUpdation : "",
                          },
                      }, )
                  },3000)
                  this.getUserDetails();
              })
           }
       })
       .catch((error) => {
        if(error) {
            this.setState({
                errorMessage : error.response.data[error],
                disableButton : false,
            })
        }
    })
   };


   
    openSelectedSection = (e) =>{
    this.setState({
        openSection : e.target.id
    })
    };

    closePasswordBox =() =>{
        this.setState({
            changePasswordBox : false,
        
        })
    };

    closePersonalInformation =() => {
        this.setState({
            personalInformationSection :false
        })
    };

    closeUploadDocumentSection =() => {
        this.setState({
            openDocuments :false
        })
    };

   
    openEditContactNumberPopUp = () =>{
        this.setState({
            openEditContactNumber : true
        })
    };

    closeEditContactNumberPopUp = () =>{
        this.setState({
            openEditContactNumber : false
        })
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };


    openUploadPopUp =() =>{
        this.setState({
            attachFile : true
        })
    };

    closeUploadPopUp =() =>{
        this.setState({
            attachFile : false
        })
    }

    uploadFiles = (event) => {
        this.setState({
           // userFile : event.target.files[0]
        })
    };
    openSearchBar = () =>{
        this.setState({
            searchBar : true
        })
    }


    handleContactValueChange = (e) => {
       this.setState({
               contactNumberUpdation : e.target.value
       })
    };

    handleCountryCodeValueChange = (e) => {
        this.setState({
                countryCodeUpdation : e.target.id
        })
     };


    handleInputValues = (e) => {
        let obj = {...this.state.passwords};
        obj[e.target.name] = e.target.value;
        this.setState({
            passwords : obj
        })
    };
   

    validatePassword = () => {
        if(this.state.passwords.oldPassword.trim() === "" ||  this.state.passwords.password.trim() === "" || this.state.passwords.confirmPassword.trim() === "") {
            this.setState({
                errorMessage : "Please enter your Passwords",
                disableButton : false,
            });
            return false;
        }
        else if (this.state.passwords.password.trim() !== this.state.passwords.confirmPassword.trim()) {
            this.setState({
                errorMessage : "Your passwords didn't match",
                disableButton : false,
            });
            return false;
        }
        else if (this.state.passwords.oldPassword.includes(" ") || this.state.passwords.password.includes(" ") || this.state.passwords.confirmPassword.includes(" ")) {
            this.setState({
                errorMessage : "Passwords should not contain space characters.",
                disableButton : false,
            });
            return false;
        }
        else if (this.state.passwords.oldPassword.trim() === this.state.passwords.password.trim()) {
            this.setState({
                errorMessage : "Old and new passwords can't be same.",
                disableButton : false,
            });
            return false;
        }
        else return true;
    };



    submitPasswordResetForm = (e) => {
        e.preventDefault();
        this.setState({
            errorMessage : null,
            disableButton : true,
        });
        if(this.validatePassword() === true) {
            let passwordChangeBodyData = {
                old_password : this.state.passwords.oldPassword,
                new_password : this.state.passwords.password,
            };
            axios({
                method : 'PUT',
                url : endPointConfig.passwordChange,
                data : passwordChangeBodyData,
                headers : {
                    'Content-Type': 'application/json',
                    'Authorization' : "Bearer " + this.state.accessToken
                    
                }
            })
                .then((res) => {
                    if(res.status === 200) {
                        this.setState({
                            successMessage : "Your password has been changed successfully. ",
                            disableButton : false
                        }, () => {
                            setTimeout(() => {
                                this.setState({
                                    successMessage : null,
                                    errorMessage : "",
                                    passwords : {
                                        oldPassword : "",
                                        password : "",
                                        confirmPassword : ""
                                    },
                                }, )
                            },3000)
                        })
                    }
                })
                .catch((error) => {
                    if(error) {
                        this.setState({
                            errorMessage : error.response.data[error],
                            disableButton : false,
                        })
                    }
                })
        }
    };

  
    render() {
        return (
            <div className="dashboard_page_wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-xs-12 col-xs-12">
                            <div className="sidebar_wrapper">
                              
                                <div className="option_wrapper">
                                <div className="single_option_wrapper">
                                        <img onClick={this.openSelectedSection} id ="bookings" src={PersonalInformation}></img>
                                        <p onClick={this.openSelectedSection} id ="bookings">Bookings</p>
                                    </div>
                                    <div className="single_option_wrapper">
                                        <img onClick={this.openSelectedSection} id ="personalInformation" src={PersonalInformation}></img>
                                        <p onClick={this.openSelectedSection} id ="personalInformation">Personal Information</p>
                                    </div>
                                    <div className="single_option_wrapper">
                                        <img src={Settings} onClick={this.openSelectedSection} id ="changePassword" ></img>
                                        <p onClick={this.openSelectedSection} id="changePassword">Change Password</p>
                                    </div>
                                    <div className="single_option_wrapper">
                                        <img  src={UploadDocumentIcon} onClick={this.openSelectedSection} id ="uploadDocuments"></img>
                                        <p onClick={this.openSelectedSection} id ="uploadDocuments">Upload Documents</p>
                                    </div>
                                </div>
                                
                                    {
                                    //    <div className="user_profile_wrapper">
                                    //    <p className="user_name"> {this.state.userData.first_name} {this.state.userData.last_name}</p>
                                    //    <p className="user_number"> {this.state.userData.contact_number}</p>
                                    //    <p className="user_email"> {this.state.userData.email}</p>
                                    //     </div>
                                    } 
                                      
                                 
                            </div>
                        </div>

                        <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                       {/* ************************Bookings********************** */} 
                       <div  className="bookings_main_wrapper">     

                        { this.state.userData.length > 0 && this.state.openSection === "bookings" ?
                            <div id="bookings" className="bookings_wrapper"> 
                             <div className="booking_wrapper_title">
                                <div className="booking_headline_title">
                                    <p className="bookings_title">Hi, {this.state.userData[0].first_name} {this.state.userData[0].last_name}</p>
                                    <p className="bookings_sub_title">Ready to start</p>
                               </div>
                               <img src={UserProfileImage}></img>
                            </div>

                            <div className="bookings_main_wrapper">
                              <p className="overview_title">Overview</p>
                              <div className="overview_wrapper">
                                  <div className="single_overview_wrapper">
                                      <img src={BookingIcon}></img>
                                      <p>Booking</p>
                                  </div>
                                  <div className="single_overview_wrapper">
                                      <img src={WishlistIcon}></img>
                                      <p>Wishlist</p>
                                  </div>
                              
                              </div>
                             
                                 {
                                  
                                    this.state.userData[0].bookings.map((booking, i) => {
                                        return(
                                            <div className="single_bookings_main_wrapper"> 
                                                      <div class="image_wrapper">
                                                            <img src={booking.room_type.images[0].image}></img>
                                                    </div>  
                                                    <div className="content_wrapper">
                                                    <p>{booking.property}</p>
                                                    <p>{booking.room_type.title}</p>
                                                    <StarRatingComponent value={5} editing={false} />
                                                    <p>Price: £{booking.bed_price.bed_price} per week</p> 
                                                    <p>Check in Date: {booking.check_in_date} </p>
                                                    <p>Check out Date: {booking.check_in_date} </p>
                                                    </div>
                                                    <div className="edit_delete_section">
                                     <div className="status_section">
                                     <p>Status: In_Process</p>
                                     </div>
                                 </div> 
                                                </div>
                                        )
                                    })
                                }
                                
                                 </div>
                                 
                               
                           
                            </div> : null}
                            </div>


             {/* ************************Password Reset********************** */}  
                            
                            {
                                    this.state.openSection === "changePassword" ?
                                    <div   id ="changePassword" className="password_wrapper">
                                    <div>
                                        <p className="reset_heading">You can reset your password</p>
                                        <form onSubmit= {this.submitPasswordResetForm}>
                                         <div className="password_content_wrapper">  
                                    <label>Enter Old Password</label>
                                    <input 
                                    type="password" 
                                    name="oldPassword" 
                                    onChange={this.handleInputValues}
                                    required={true}
                                    value={this.state.passwords.oldPassword}
                                      />
                                    <label>Enter New Password</label>
                                    <input 
                                    type="password" 
                                    name="password" 
                                    onChange={this.handleInputValues}
                                    required={true}
                                    value={this.state.passwords.password}
                                     />
                                       <label>Re-Enter Password</label>
                                       <input 
                                    type="password" 
                                    name="confirmPassword" 
                                    onChange={this.handleInputValues}
                                    required={true}
                                    value={this.state.passwords.confirmPassword}
                                />
                                {this.state.successMessage && <p className="success_message">{this.state.successMessage}</p>}
                                {this.state.errorMessage && <p className="error_message">{this.state.errorMessage}</p>}
                                <div className="buttons_wrapper"> 
                                <button disabled={this.state.disableButton}  className="btn btn-primary pink_button">Reset Password</button>
                                </div>
                                </div> 
                                </form>
                                </div>
                                    </div>
                                    : null
                                }
                          


                 {/* ************************Personal Information********************** */}  
                              
                                    {
                                        this.state.userData.length > 0 && this.state.openSection === "personalInformation" ?
                                        <div id="personalInformation"  className="personal_information_section">
                                        <div  className="personal_information_wrapper">
                                            <p>You can edit your contact number</p>
                                            <div className="personal_details_section">
                                          <p>Name: <span className="user_information_span">{this.state.userData[0].first_name} {this.state.userData[0].last_name}</span>  </p>
                                          <p>Email-Id:<span className="user_information_span"> {this.state.userData[0].email}</span>  </p>
                                          <p>Contact Number: <span className="user_information_span"> {this.state.userData[0].contact_number} </span> 
                                            <span className="user_image_span">
                                            <img onClick={this.openEditContactNumberPopUp} src={EditIcon}>
                                            </img></span></p>
                                          </div>
                                          {
                                               this.state.openEditContactNumber ?
                                               <div className="edit_contact">
                                                <form className="contact_form" onSubmit={this.getUserDetailsUpdate}>
                                                <div className="single_field_wrepper">
                                                    <label>Update CONTACT NUMBER</label>
                                                        <div className="contact_number_wrapper">
                                                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)} name="countryCodeUpdation"  onChange={this.handleCountryCodeValueChange}/>
                                                            <input type="number" name="contactNumberUpdation" required={true}
                                                                onChange={this.handleContactValueChange}/>
                                                        </div>
                                                 </div>
                                                 <button disabled={this.state.disableButton} className="btn btn-primary ">UPDATE</button>
                                                 <button onClick={this.closeEditContactNumberPopUp}  className="btn btn-primary ">CANCEL</button>
                                                </form>
                                               </div>
                                               : null     
                                            }
                                        </div> 
                                        </div>
                                        :null
                                    }
                               



            {/* ************************Upload file********************** */}  
                               
                                    {
                                        this.state.openSection === "uploadDocuments" ?
                                        <div  id = "uploadDocuments"  className="upload_document_section">
                                        <div className="upload_document_wrapper">
                                            <p className="upload_document_title">Documents</p>
                                            <p className="instruction"> * Only jpeg,jpg,png and pdf file are allowed </p>
                                            <div className="image_wrapper">
                                            <img src={AddDocumentIcon} onClick={this.openUploadPopUp}></img>
                                            </div>
                                            {
                                                this.state.attachFile ? 
                                                <div className="attach_file_wrapper">
                                                    <input type="file" accept="image/x-png,image/gif,image/jpeg, image/jpg" multiple="multiple" onChange={this.uploadFiles} />
                                                    <button>Upload</button>
                                                </div>
                                                : null
                                            }
                                            <div className="table_wrapper">
                                        <table>
                                            <tr>
                                                <th>Attachment</th>
                                                <th>Status</th>
                                                <th>Remark</th>
                                                <th>Date Uploaded</th>
                                                <th>Edit / View</th>
                                            </tr>

                                        </table>
                                        </div>
                                        </div>
                                        </div>
                                        : null
                                    }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default {
    component: UserDashBoardPage
};
import React, { Component } from "react";
import endPointConfig from "../../endpoints";

const axios = require('axios');

class Newsletter extends Component {
    state = {
        emailAddress : "",
        disableButton : false,
        errorMessage : "",
        showThankYouPopup : false
    };

    handleEmailValueChange = (e) => {
        this.setState({
            emailAddress : e.target.value
        })
    };

    submitNewsletterForm = (e) => {
        e.preventDefault();
        this.setState({
            disableButton : true
        })
        if(this.state.emailAddress.trim() === "") {
            this.setState({
                errorMessage : "Please enter your EMail Address.",
                disableButton : false,
            })
        }
        else {
            axios({
                method : 'POST',
                url : endPointConfig.newsletterSubscription,
                data : {email : this.state.emailAddress}
            })
                .then((res) => {
                    if(res.status === 201) {
                        this.setState({
                            errorMessage : "",
                            showThankYouPopup: true,
                            disableButton : false,
                            email : ""
                        })
                    }
                })
                .catch((err) => {
                    if(err) {
                        this.setState({
                            errorMessage : "You are already subscribed to our newsletter.",
                            disableButton : false
                        })
                    }
                })
                
        }
    };

    toggleNewsletterPopup = () => {
        this.setState({
            showThankYouPopup : !this.state.showThankYouPopup,
            disableButton : false
        })
    }

    render() {
        let thankYouPopup =
            <div className="popup_backdrop">
                <div className="newsletter_popup_content">
                    <p>Welcome abroad. Our community will share good stuffs and related offers with you.</p>
                    <button onClick={this.toggleNewsletterPopup} className="btn btn-primary">OK</button>
                </div>
            </div>

        return(
            <div className="newsletter">
                {this.state.showThankYouPopup ? thankYouPopup : null}
                <div className="newsletter_description">
                    <p>Sign up to get our latest exclusive updates, deals, offers and promotions</p>
                </div>

                <div className="newsletter_form_wrapper">
                    <form onSubmit={this.submitNewsletterForm}>
                        <input
                            onChange={this.handleEmailValueChange}
                            id="emailAddress"
                            type="email"
                            placeholder="EMAIL"
                            required={true}
                            value={this.state.email}
                        />
                        <button disabled={this.state.disableButton} className="btn btn-primary">SIGN UP</button>
                        {this.state.errorMessage !== "" ? <p className="newsletter_error_message">{this.state.errorMessage}</p> : null}
                    </form>
                </div>
            </div>
        )
    }
}

export default Newsletter;
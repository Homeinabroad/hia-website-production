import React, { Component } from "react";
import {withRouter} from "react-router-dom";


import endPoinConfig from "../../endpoints/index";

const axios = require('axios');

class ServiceForm extends Component {
    state = {
        serviceData : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            message : "",
        },
        errorMessage : "",
        disableButton : ""
    };

    handleFormInputValueChanges = (e) => {
        let obj = this.state.serviceData;
        obj[e.target.id] = e.target.value;
        this.setState({
            serviceData : obj
        })
    };

    

    validateServiceForm = () => {
        if(this.state.serviceData.firstName.trim() === "") {
          this.setState({
            errorMessage : "Please enter your first name.",
            disableButton : false
          })
          return false
        }
        else if(this.state.serviceData.lastName.trim() === "") {
            this.setState({
              errorMessage : "Please enter your last name.",
              disableButton : false
            })
            return false
          }
        else if(this.state.serviceData.email.trim() === "") {
          this.setState({
            errorMessage : "Please enter your Email Address.",
            disableButton : false
          })
          return false
        }
        else if(this.state.serviceData.contactNumber.trim() === "") {
          this.setState({
            errorMessage : "Please enter your Contact Number.",
            disableButton : false
          })
          return false
        }
        else if(this.state.serviceData.message.trim() === "") {
          this.setState({
            errorMessage : "Please enter your Message.",
            disableButton : false
          })
          return false
        }
       
        else return true
      };
    
      submitServiceForm = (e) => {
        e.preventDefault();
        this.setState({
          errorMessage : "",
          disableButton : true
        })
        if(this.validateServiceForm() === true) {
          let userDataObjBody = {
            first_name : this.state.serviceData.firstName,
            last_name : this.state.serviceData.lastName,
            email : this.state.serviceData.email,
            contact_number : this.state.serviceData.contactNumber,
            message : this.state.serviceData.message,
            service_name : this.props.location.pathname.split("/")[1]
          }
          axios({
            method : 'POST',
            url : endPoinConfig.getService,
            data : userDataObjBody
          })
          .then((res) => {
            console.log("RESP", res)
            if(res.status === 201) {
              this.setState({
                errorMessage : "",
                disableButton : false
              })
              window.location.assign("/service-request/thank-you");
            }
            else {
              this.setState({
                errorMessage : "Oops! Something went wrong. Please try again later.",
                disableButton : false
              })
            }
          });
        }
      };

    render() {
        return(
            <div className="service_form_component_wrapper">
                <form onSubmit={this.submitServiceForm}>
                    <div className="single_row_wrapper">
                        <div className="single_form_field">
                            <label>FIRST NAME</label>
                            <input type="text" id="firstName" required={true} onChange={this.handleFormInputValueChanges} />
                        </div>

                        <div className="single_form_field">
                            <label>LAST NAME</label>
                            <input type="text" id="lastName" required={true} onChange={this.handleFormInputValueChanges} />
                        </div>
                    </div>
                    
                    <div className="single_row_wrapper">
                        <div className="single_form_field">
                            <label>EMAIL ADDRESS</label>
                            <input type="email" id="email" required={true} onChange={this.handleFormInputValueChanges} />
                        </div>

                        <div className="single_form_field">
                            <label>CONTACT NUMBER</label>
                            <input type="number" id="contactNumber" required={true} onChange={this.handleFormInputValueChanges} />
                        </div>
                    </div>
                    
                    <div className="single_row_wrapper">
                        <div className="single_form_field message_field_wrapper">
                            <label>YOUR MESSAGE</label>
                            <textarea id="message" required={true} onChange={this.handleFormInputValueChanges}></textarea>
                        </div>
                    </div>

                
                    {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                    <button disabled={this.state.disableButton} className="btn btn-primary">SUBMIT</button>
                </form>
            </div>
        )
    }
}

export default withRouter(ServiceForm)
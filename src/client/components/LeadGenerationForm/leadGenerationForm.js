import React, { Component } from "react";
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";

import {mainListing} from '../../actions/index';
import CountryCodeDropdown from "../CountryCode/CountryCode";
import endPointConfig from "../../endpoints";

const axios = require('axios');

class LeadsGenerationForm extends Component {
    state = {
        showThankYouPopup : false,
        contactCountryCode : "",
        universityList : [],
        universityName : "",
        disableButton: false,
        errorMessage : "",
        userData : {
            firstName : "",
            lastName : "",
            email : "",
            contactNumber : "",
            message : ""
        }
    };

    componentDidMount() {
        this.getUniversityList();
    }

    getUniversityList = () => {
       if (this.props.location.pathname="/get-in-touch") 
       {
          
        axios.get(endPointConfig.universities) 
        .then((res) => {
            let uniArray = [];
            res.data.map((uni) => {
                return(
                    uniArray.push({name : uni.name, id : uni.id})
                )
            })
            this.setState({
                universityList : uniArray
            })
        })
       }
       else{
      
        axios.get(endPointConfig.cityUniversities + this.props.cityDetails.slug) 
            .then((res) => {
                let uniArray = [];
                res.data.map((uni) => {
                    return(
                        uniArray.push({name : uni.name, id : uni.id})
                    )
                })
                this.setState({
                    universityList : uniArray
                })
            })
        }
    };

    setCountryCode = (code) => {
        this.setState({
            contactCountryCode: code
        });
    };

    handleUniversitySelection = (e) => {
        this.setState({
            universityName : e.target.value
        })
    };

    handleFieldValueChange = (e) => {
        let obj = {...this.state.userData};
        obj[e.target.id] = e.target.value;
        this.setState({
            userData : obj
        })
    };

    closeThankYouPopup = () => {
        this.setState({
            showThankYouPopup : false
        })
    };

    validateLeadsForm = () => {
        if(this.state.userData.firstName.trim() === "") {
            this.setState({
                disableButton: false,
                errorMessage : "Please enter your first name.",
            })
            return false;
        }
        else if(this.state.userData.lastName.trim() === "") {
            this.setState({
                disableButton: false,
                errorMessage : "Please enter your last name.",
            })
            return false;
        }
        else if(this.state.userData.email.trim() === "") {
            this.setState({
                disableButton: false,
                errorMessage : "Please enter your EMail Address.",
            })
            return false;
        }
        else if(this.state.userData.contactNumber.trim() === "") {
            this.setState({
                disableButton: false,
                errorMessage : "Please enter your Contact Number.",
            })
            return false;
        }
        else if(this.state.userData.message.trim() === "") {
            this.setState({
                disableButton: false,
                errorMessage : "Please enter your message.",
            })
            return false;
        }
        else return true;
    };

    submitLeadsFormData = (e) => {
        e.preventDefault();
        this.setState({
            disableButton: true,
            errorMessage : "",
        });
        if(this.validateLeadsForm() === true) {
            let objBody = {
                first_name :  this.state.userData.firstName,
                last_name : this.state.userData.lastName,
                email : this.state.userData.email,
                contact_number : this.state.contactCountryCode + "-" + this.state.userData.contactNumber,
                university : this.state.universityName,
                lead_source: this.props.location.pathname="/get-in-touch" ? "Landing Page - Website - " + this.props.cityDetails.name
                                          : "Listing Page - Website - " + this.props.cityDetails.name,
              //  lead_source : "Listing Page - Website - " + this.props.cityDetails.name 
                message : this.state.userData.message, 
              
            };
           
            axios({
                method: 'POST',
                url: endPointConfig.leadGeneration,
                data: objBody,
                headers: {'Content-Type': 'application/json'}
            })
                .then((res) => {
                    if(res.status === 201) {
                        this.setState({
                            disableButton: false,
                            showThankYouPopup : true,
                            errorMessage : "",
                            userData : {
                                firstName : "",
                                lastName : "",
                                email : "",
                                contactNumber : "",
                                message : ""
                            },
                            contactCountryCode : "",
                            universityName : "",
                        })
                    }
                    else {
                        this.setState({
                            errorMessage : "Oops! Something went wrong. Please try again.",
                            disableButton : false,
                        })
                    }
                })
        }
    };

    render() {
        let universityDropdown = (
            this.state.universityList.length > 0 ?
                this.state.universityList.map((uni, i) => {
                    return(
                        <option key={i} value={uni.name}>{uni.name}</option>
                    )
                }) : null
        );

        return(
            <div className="leads_generation_form_component_wrapper">
                {
                    this.state.showThankYouPopup ?
                    <div className="popup_backdrop">
                        <div className="popup_contents">
                            <p>Thank you! Your details have been submitted.</p>
                            <p>One of our accommodation expert will get back to you very soon.</p>
                            <button className="pink_button" onClick={this.closeThankYouPopup}>OK</button>
                        </div>
                    </div>
                    : null
                }
                <form id="leadsForm" onSubmit={this.submitLeadsFormData}>
                    <p>Our Uni Experts will save you at least of 10 hours, you spent in searching 
                    5,00,000+ properties in an affordable price</p>
                    <div className="single_form_field">
                        <label>FIRST NAME</label>
                        <input type="text" value={this.state.userData.firstName} id="firstName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>LAST NAME</label>
                        <input type="text" value={this.state.userData.lastName} id="lastName" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>EMAIL ADDRESS</label>
                        <input type="email" value={this.state.userData.email} id="email" onChange={this.handleFieldValueChange} required />
                    </div>

                    <div className="single_form_field">
                        <label>CONTACT NUMBER</label>
                        <div className="contact_number_wrapper">
                            <CountryCodeDropdown countryCode={(e) => this.setCountryCode(e)} />
                            <input type="number" value={this.state.userData.contactNumber} id="contactNumber" onChange={this.handleFieldValueChange} required />
                        </div>
                    </div>

                    <div className="full_length_field">
                        <label>UNIVERSITY GOING FOR STUDY</label>
                        <select  value={this.state.universityName} required={true} onChange={this.handleUniversitySelection}>
                            <option>SELECT YOUR UNIVERSITY</option>
                            {universityDropdown}
                        </select>
                    </div>

                    <div className="full_length_field">
                        <label>MESSAGE</label>
                        <textarea value={this.state.userData.message} id="message" onChange={this.handleFieldValueChange} required={true}>

                        </textarea>
                    </div>
                    <p className="getintouch_terms">By submitting the form, you agree to <a href="https://www.homeinabroad.com/terms-and-privacy" target="blank">
                       Terms and Conditions </a> and <a href="https://www.homeinabroad.com/terms-and-privacy" >Privacy Policy</a> </p> 
                    {this.state.errorMessage !== "" ? <p className="error_message">{this.state.errorMessage}</p> : null}
                    <button disabled={this.state.disableButton} className="btn btn-primary">SUBMIT</button>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {cityDetails: state.cityDetails};
}

export default withRouter(connect(mapStateToProps, {mainListing})(LeadsGenerationForm))
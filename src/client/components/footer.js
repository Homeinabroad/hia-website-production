import React from 'react';

import WebsiteLogo from '../../assets/graphics/logos/website-coloured-logo.svg';
import Newsletter from "../components/Newsletter/newsletter";

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="show_desktop">
                        <div className="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <img alt="Homeinabroad" src={WebsiteLogo} className="footer_website_logo" />
                            <p className="short_desc">Homeinabroad is one stop solutions for student accommodation. We are not just the
                                accommodation provider, we are a company that will support in every steps till you get
                                settled in your accommodations. We have different types of accommodations, starting from
                                PBSA to shared apartments to singe private room for long as well as short term stay.
                            </p>
                            <p className="copyright_text">© 2020 | Homeinabroad Inc. |&nbsp;
                                <a target="_blank" href="/terms-and-privacy">
                                    Terms and Privacy
                                </a>
                            </p>
                        </div>

                        <div className="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <p className="footer_headlines">COMPANY</p>
                            <ul>
                                <li>
                                    <a href="/services">
                                        Services
                                    </a>
                                </li>
                                <li>
                                    <a href="/about-us">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="/contact">
                                        Contact Us
                                    </a>
                                </li>
                                <li>
                                    <a href="/refer-and-earn">
                                        Refer And Earn
                                    </a>
                                </li>
                                <li>
                                    <a href="/how-it-works">
                                       How It Works
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div className="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <Newsletter />
                            <p className="follow_headline">FOLLOW US ON</p>
                            <a href="https://www.facebook.com/homeinabroad.accommodation" target="_blank">
                                <div className="fa fa-facebook"></div>
                            </a>

                            <a href="https://www.instagram.com/homeinabroad/" target="_blank">
                                <div className="fa fa-instagram"></div>
                            </a>

                            <a href="" target="_blank">
                                <div className="fa fa-twitter"></div>
                            </a>
                        </div>
                    </div>

                    <div className="show_mobile">
                        <div className="col-xs-12 col-sm-12">
                            <img alt="Homeinabroad" src={WebsiteLogo} className="footer_website_logo" />
                            <p className="short_desc">Homeinabroad is one stop solutions for student accommodation. We are not just the
                                accommodation provider, we are a company that will support in every steps till you get
                                settled in your accommodations. We have different types of accommodations, starting from
                                PBSA to shared apartments to singe private room for long as well as short term stay.
                            </p>

                            <Newsletter />

                            <p className="follow_headline">FOLLOW US ON</p>
                            <a href="https://www.facebook.com/homeinabroad.accommodation" target="_blank">
                                <div className="fa fa-facebook"></div>
                            </a>

                            <a href="https://www.instagram.com/homeinabroad/" target="_blank">
                                <div className="fa fa-instagram"></div>
                            </a>

                            <a href="" target="_blank">
                                <div className="fa fa-twitter"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );  
};

export default Footer;
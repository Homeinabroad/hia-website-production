import React, { Component } from 'react';
import $ from "jquery";

import SearchBar from "../components/SearchBar/searchBar";
import WebsiteLogo from "../../assets/graphics/logos/website-coloured-logo.svg";
import WhiteOutlineLogo from "../../assets/graphics/logos/white-filled-logo.svg";

class Header extends Component {
    state = {
        showSideDrawer: false,
        isLoggedIn: false,
        showDesktopMenus: false,
        showSearchBarOnHeader : false
    }

    componentDidMount() {
        if (sessionStorage.getItem("token")) {
            this.setState({
                isLoggedIn: true
            })
        }
        $(window).scroll(() => {
            let scroll = $(window).scrollTop();
            if (scroll >= 600) {
                this.setState({
                    showSearchBarOnHeader : true
                })
                $(".header_wrapper").addClass("background_header");
            }
            else {
                this.setState({
                    showSearchBarOnHeader : false
                })
                $(".header_wrapper").removeClass("background_header");
            }
        });
    }

    toggleSideDrawer = () => {
        this.setState({
            showSideDrawer: !this.state.showSideDrawer
        })
    };

    logOutUser = () => {
        sessionStorage.removeItem("token");
        window.location.assign("/");
    }

    toggleDesktopMenuBar = () => {
        this.setState({
            showDesktopMenus : !this.state.showDesktopMenus
        })
    };

    render() {
        return (
            <header className="header_wrapper">
                <div className="show_desktop">
                    <a href="/">
                        <div className="logo_wrapper">
                            <img src={this.state.showSearchBarOnHeader ? WebsiteLogo : WhiteOutlineLogo} alt="HomeinAbroad"/>
                        </div>
                    </a>
                </div>

                <div className="show_mobile">
                    <a href="/">
                        <div className="logo_wrapper">
                            <img src={WebsiteLogo} alt="HomeinAbroad"/>
                        </div>
                    </a>
                </div>


                {this.state.showSearchBarOnHeader && <SearchBar/>}

                {
                    !this.state.showDesktopMenus ?
                        <div onClick={this.toggleDesktopMenuBar} className="show_desktop desktop_hamburger">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div> :
                        <div className="close_desktop_hamburger" onClick={this.toggleDesktopMenuBar}>X</div>
                }


                {
                    this.state.showDesktopMenus &&
                    <div className="menus_wrapper">
                        <div className="show_desktop">
                            <ul>
                                <li><a href="/contact">CONTACT US</a></li>
                                <li><a href="/services">SERVICES</a></li>
                                <li><a href="/about-us">ABOUT US</a></li>
                                {!this.state.isLoggedIn ? <li><a href="/login">LOGIN</a></li> : null}
                                {this.state.isLoggedIn ? <li><a href="/dashboard">DASHBOARD</a></li> : null}
                                {this.state.isLoggedIn ? <li onClick={this.logOutUser}>LOGOUT</li> : null}
                            </ul>
                        </div>
                    </div>
                }

                <div onClick={this.toggleSideDrawer}  className="mobile_hamburger show_mobile">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>

                {this.state.showSideDrawer ?
                    <div className="popup_backdrop">
                        <div className="side_drawer_wrapper">
                            <ul>
                                <li><a href="/">HOME</a></li>
                                <li><a href="/contact">CONTACT US</a></li>
                                <li><a href="/services">SERVICES</a></li>
                                <li><a href="/about-us">ABOUT US</a></li>
                                {!this.state.isLoggedIn ? <li><a href="/login">LOGIN</a></li> : null}
                                {this.state.isLoggedIn ? <li><a href="/dashboard">DASHBOARD</a></li> : null}
                                {this.state.isLoggedIn ? <li onClick={this.logOutUser}>LOGOUT</li> : null}
                            </ul>

                            <div className="calling_extension">
                                <p>Call Us on:</p>
                                <p>UNITED KINGDOM</p>
                                <p>+44-2086388957</p>

                                <p>AUSTRALIA</p>
                                <p>+61-280069159</p>
                            </div>

                            <div className="social_media_extension">
                                <p>Follow Us on:</p>
                                <a href="https://www.facebook.com/Homeinabroad-100236905128631" target="_blank">
                                    <div className="fa fa-facebook"></div>
                                </a>

                                <a href="https://www.instagram.com/homeinabroad/" target="_blank">
                                    <div className="fa fa-instagram"></div>
                                </a>

                                <a href="" target="_blank">
                                    <div className="fa fa-twitter"></div>
                                </a>
                            </div>
                            <div onClick={this.toggleSideDrawer} className="fa fa-long-arrow-left"></div>
                        </div>
                    </div>
                    : null
                }
            </header>
        );
    }
}

export default Header;
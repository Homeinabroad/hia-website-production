import React, { Component } from "react";

class CommonThankYouMessage extends Component {
    handleButtonClick = () => {
        window.location.assign(document.referrer);
    };

    render() {
        return (
            <div className="common_thank_you_message">
                <p>Thank you for sharing your information with us!</p>
                <p>We've received your booking request.</p>
                <p>One of our booking expert will be in touch with you soon.</p>
                <button onClick={this.handleButtonClick} className="btn btn-primary">EXPLORE MORE</button>
            </div>
        );
    }
}

export default CommonThankYouMessage;
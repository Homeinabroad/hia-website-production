import React, { Component } from "react";
import $ from "jquery";

import endPointConfig from "../../endpoints/index";
const axios = require('axios');

class TypeAhead extends Component {
    state = {
        searchedKeyword : "",
        searchSuggestions : null,
        showSuggestionList : false
    };

    handleSearchKeywordChange = (e) => {
        this.setState({
            searchedKeyword : e.target.value
        }, () => {
            if(this.state.searchedKeyword.length > 2) {
                axios.get(endPointConfig.search + this.state.searchedKeyword)
                .then((res) => {
                    this.setState({
                        searchSuggestions : res.data,
                        showSuggestionList : true
                    })
                })
            }
        })
    };

    hideSuggestionListOnBlur = () => {
        this.setState({
            showSuggestionList : false
        })
    };

    render() {
        return(
            <div className="typeahead">
                <input 
                    type="text"
                    placeholder="Search your City of study or Property"
                    onChange={this.handleSearchKeywordChange}
                    //onBlur={this.hideSuggestionListOnBlur}
                    value={this.state.searchedKeyword}
                />
                <button className="btn btn-primary">SEARCH</button>
                {
                    (this.state.searchedKeyword.length > 2 && this.state.showSuggestionList) ?
                    <div className="suggestions_list_wrapper">
                        <h6>Cities</h6>
                        <ul>
                        {
                            this.state.searchSuggestions.cities.length > 0 ? this.state.searchSuggestions.cities.map((city) => {
                                return (
                                    <a href={"/" + city.country_slug + "/" + city.slug}>
                                        <li>{city.name}</li>
                                    </a>
                                )
                            }) : <h6>No Cities found for this searched keyword.</h6>
                        }
                        </ul>
                        <h6>Universities</h6>
                        <ul>
                        {
                            this.state.searchSuggestions.universities.length > 0 ? this.state.searchSuggestions.universities.map((university) => {
                                return (
                               <a href={"/" + university.city.country_slug + "/" + university.city.slug + "/u/" + university.slug}>
                                    <li>{university.name}</li>
                                </a>
                                )
                            }) : <h6>No Universities found for this searched keyword.</h6>
                        }
                        </ul>
                        <h6>Properties</h6>
                        <ul>
                        {
                            this.state.searchSuggestions.properties.length > 0 ? this.state.searchSuggestions.properties.map((property) => {
                                return(
                                    <a href={"/" + property.country_slug + "/" + property.city_slug + "/" + property.slug}>
                                        <li>{property.name}</li>
                                    </a>
                                )
                            }) : <h6>No Properties found for this searched keyword.</h6>
                        }
                        </ul>
                    </div> 
                    : null
                }
            </div>
        )
    }
}

export default TypeAhead;
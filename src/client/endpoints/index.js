const baseUrl = "https://api.homeinabroad.com/";

const apiConfig = {
    baseUrl : baseUrl,
    leadGeneration : baseUrl + "lead_generation/",
    getService : baseUrl + "service_request/",
    roomEnquiry : baseUrl + "enquiry/list/",
    roomBooking : baseUrl + "booking/list/",
    search : baseUrl + "search/",
    featuredCities : baseUrl + "city/featured/",
    featuredProperties: baseUrl + "property/featured/",
    newsletterSubscription : baseUrl + "newsletter_subscription/",
    signUp : baseUrl + "user/",
    login : baseUrl + "login/",
    cityPropertyListing : baseUrl + "property/?city__slug=",
    guestBooking : baseUrl + "booking/guest/",
    cityUniversities : baseUrl + "/university/?city__slug=",
    universities: baseUrl + "university/",
    universityListing: baseUrl + "university/",
    passwordReset : baseUrl + "password_reset/",
    confirmPasswordReset : baseUrl + "password_reset/confirm/",
    passwordChange : baseUrl + "password/change",
    userDashboard : baseUrl + "user/",
    cities : baseUrl + "city/",
    serviceList : baseUrl + "service_request/",
    referAndEarn : baseUrl + "referral/",
}

module.exports = apiConfig;
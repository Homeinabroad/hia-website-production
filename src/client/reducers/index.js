import { combineReducers } from 'redux';
import cityDetailReducer from './cityDetailsReducer';
import propertyDetailsReducer from "./propertyDetailsReducer";
import universityDetailsReducer from "./universityDetailsReducer";

export default combineReducers({
    cityDetails : cityDetailReducer,
    propertyDetails : propertyDetailsReducer,
    universityDetails : universityDetailsReducer,
});
import { FETCH_UNIVERSITY_DETAILS } from '../actions';

export default (state = [], action) => {
    switch (action.type) {
        case FETCH_UNIVERSITY_DETAILS:
            return action.payload.data;
        default:
            return state;
    }
};
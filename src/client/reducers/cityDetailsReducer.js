import { FETCH_CITY_DETAILS } from '../actions';

export default (state = [], action) => {
    switch (action.type) {
        case FETCH_CITY_DETAILS:
            return action.payload.data;
        default:
            return state;
    }
};
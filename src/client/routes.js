import App from './app';
import HomePage from './pages/Homepage/mainHomePage';
import BookAccommodation from './pages/BookingPage/bookingpage';
import BookingThankYouPage from './pages/BookingPage/bookingThankyouPage';
import ContactPage from './pages/ContactUs/contactPage';
import ContactThankYouPage from './pages/ContactUs/contactThankYouPage';
import RegistrationPage from './pages/RegistrationPage/registrationPage';
import Services from './pages/ServicePage/servicesPage';
import ServiceRequestThankYouPage from './pages/ServicePage/serviceThankYouPage';
import AboutUsPage from './pages/AboutUsPage/aboutUsPage';
import NotFoundPage from './pages/notFound404Page';
import Login from './pages/login/login';
import CityListingPage from "./pages/CityListing/cityListing";
import PropertyDetailsPage from "./pages/DetailsPage/mainDetailsPage";
import CommonThankYouPage from "./pages/CommonThankYouPage/CommonThankYouPage";
import TermsAndConditions from "./pages/TermsAndConditions/termsAndConditions";
import LandingPage from "./pages/LandingPage/landingPage";
import UniversityListingPage from "./pages/UniversityListing/universityListing";
import RetrievePasswordPage from "./pages/RetrievePasswordPage/retrievePasswordPage";
import UserDashBoardPage from "./pages/Dashboard/dashboardPage";
import ProviderPage from "./pages/ProvidersPage/providersPage";
import StudentLivingProviderPage from "./pages/StudentLivingProviderPage/studentLivingProvider";
import ReferAndEarnPage from "./pages/ReferAndEarnPage/referAndEarnPage";
import HowItWorksPage from './pages/HowItWorksPage/howItWorksPage';
import RoomEssentialsPage from './pages/ServicePage/roomEssentialsPage';
import CompleteKitchenPack from './pages/ServicePage/completeKitchenPack';
import CompleteRoomPack from './pages/ServicePage/completeRoomPack';
import CompleteBedroomPack from './pages/ServicePage/completeBedroomPack';
import CompletePPEKitPack from './pages/ServicePage/completePPEKitPack';
import AirportServicePage from './pages/ServicePage/airportServicePage';
import RemittanceServicePage from './pages/ServicePage/remittanceServicePage';
import ForexServicePage from './pages/ServicePage/forexServicePage';
import AccommodationPage from './pages/ServicePage/accommodationPage';
import RoomReplacementPage from './pages/ServicePage/roomReplacementPage';
import GurantorPage from './pages/ServicePage/gurantorPage';
import MeetAndGreetPage from './pages/ServicePage/meetAndGreetPage';
import OverseasSimPage from './pages/ServicePage/overseasSimPage';
import EducationLoanPage from './pages/ServicePage/educationLoanPage';



export default [
    {
        path: '/git/thank-you',
        ...App,
        routes: [
            {
                ...CommonThankYouPage
            }
        ]
    },
    {
        path: '/:countrySlug/:citySlug/:propertySlug/book-now/thank-you',
        ...App,
        routes: [
            {
                ...BookingThankYouPage
            }
        ]
    },
    {
        path: '/:countrySlug/:citySlug/:propertySlug/book-now',
        ...App,
        routes: [
            {
                ...BookAccommodation
            }
        ]
    },
    {
        path: '/:countrySlug/:citySlug/u/:universitySlug',
        ...App,
        routes: [
            {
                ...UniversityListingPage
            }
        ]
    },
    {
        path: '/:countrySlug/:citySlug/:propertySlug',
        ...App,
        routes: [
            {
                ...PropertyDetailsPage
            }
        ]
    },
    {
        path: '/services/room-essentials/kitchen-pack',
        ...App,
        routes: [
            {
                ...CompleteKitchenPack
            }
        ]
    },
    {
        path: '/services/room-essentials/complete-room-pack',
        ...App,
        routes: [
            {
                ...CompleteRoomPack
            }
        ]
    },
    {
        path: '/services/room-essentials/complete-bedroom-pack',
        ...App,
        routes: [
            {
                ...CompleteBedroomPack
            }
        ]
    },
    {
        path: '/services/room-essentials/complete-ppekit-pack',
        ...App,
        routes: [
            {
                ...CompletePPEKitPack
            }
        ]
    },
  
    {
        path: '/contact/thank-you',
        ...App,
        routes: [
            {
                ...ContactThankYouPage
            }
        ]
    },
    {
        path: '/details-contact-us/thank-you',
        ...App,
        routes: [
            {
                ...CommonThankYouPage
            }
        ]
    },
    {
        path: '/services/room-essentials',
        ...App,
        routes: [
            {
                ...RoomEssentialsPage
            }
        ]
    },
    {
        path: '/services/airport-pickup-service',
        ...App,
        routes: [
            {
                ...AirportServicePage
            }
        ]
    },
    {
        path: '/services/remittance-service',
        ...App,
        routes: [
            {
                ...RemittanceServicePage
            }
        ]
    },
    {
        path: '/services/forex-service',
        ...App,
        routes: [
            {
                ...ForexServicePage
            }
        ]
    },
    {
        path: '/services/accommodation-service',
        ...App,
        routes: [
            {
                ...AccommodationPage
            }
        ]
    },
    {
        path: '/services/room-replacement-service',
        ...App,
        routes: [
            {
                ...RoomReplacementPage
            }
        ]
    },
    {
        path: '/services/gurantor-service',
        ...App,
        routes: [
            {
                ...GurantorPage
            }
        ]
    },
    {
        path: '/services/meet-and-greet-service',
        ...App,
        routes: [
            {
                ...MeetAndGreetPage
            }
        ]
    },
    {
        path: '/services/overseas-sim-service',
        ...App,
        routes: [
            {
                ...OverseasSimPage
            }
        ]
    },
    {
        path: '/services/education-loan-service',
        ...App,
        routes: [
            {
                ...EducationLoanPage
            }
        ]
    },
    {
        path: '/service-request/thank-you',
        ...App,
        routes: [
            {
                ...ServiceRequestThankYouPage
            }
        ]
    },
    {
        path: '/:countrySlug/:citySlug',
        ...App,
        routes: [
            {
                ...CityListingPage
            }
        ]
    },
    {
        path: '/services',
        ...App,
        routes: [
            {
                ...Services
            }
        ]
    },
    {
        path: '/about-us',
        ...App,
        routes: [
            {
                ...AboutUsPage
            }
        ]
    },
    {
        path: '/contact',
        ...App,
        routes: [
            {
                ...ContactPage
            }
        ]
    },
    {
        path: '/login',
        ...App,
        routes: [
            {
                ...Login
            }
        ]
    },
    {
        path: '/registration',
        ...App,
        routes: [
            {
                ...RegistrationPage
            }
        ]
    },
    {
        path: '/dashboard',
        ...App,
        routes: [
            {
                ...UserDashBoardPage
            }
        ]
    },
    {
        path: '/',
        exact: true,
        ...App,
        routes: [
            {
                ...HomePage
            }
        ]
    },
    {
        path: '/terms-and-privacy',
        ...App,
        routes: [
            {
                ...TermsAndConditions
            }
        ]
    },
    {
        path: '/get-in-touch',
        ...App,
        routes: [
            {
                ...LandingPage
            }
        ]
    },
    {
        path: '/retrieve-password',
        ...App,
        routes: [
            {
                ...RetrievePasswordPage
            }
        ]
    },
    {
        path: '/iq-student-accommodation',
        ...App,
        routes: [
            {
                ...ProviderPage
            }
        ]
    },
    {
        path: '/heydaycarmanhall-student-accommodation',
        ...App,
        routes: [
            {
                ...StudentLivingProviderPage
            }
        ]
    },
    {
        path: '/refer-and-earn',
        ...App,
        routes: [
            {
                ...ReferAndEarnPage
            }
        ]
    },
    {
        path: '/how-it-works',
        ...App,
        routes: [
            {
                ...HowItWorksPage
            }
        ]
    },
  
   
    {
        path: '/',
        ...App,
        routes: [
            {
                ...NotFoundPage
            }
        ]
    }
];
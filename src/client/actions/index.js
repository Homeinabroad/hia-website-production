export const FETCH_CITY_DETAILS = 'fetch_city_details';
export const mainListing = () => async (dispatch, getState, api) => {
    const res = await api.get('/');
    dispatch({
        type: FETCH_CITY_DETAILS,
        payload: res
    });
};

export const FETCH_PROPERTY_DETAILS = 'fetch_property_details';
export const fetchPropertyDetails = () => async (dispatch, getState, api) => {
    const res = await api.get('/');
    dispatch({
        type: FETCH_PROPERTY_DETAILS,
        payload: res
    });
};

export const FETCH_UNIVERSITY_DETAILS = 'fetch_university_details';
export const mainUniversityListing = () => async (dispatch, getState, api) => {
    const res = await api.get('/');
    dispatch({
        type: FETCH_UNIVERSITY_DETAILS,
        payload: res
    });
};
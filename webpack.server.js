const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackNodeExternals = require('webpack-node-externals');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");

const config = {
    module: {
        rules: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        //because remove style-loader,my css can't work
                        loader: "css-loader", options: { importLoaders: 1 }// translates CSS into CommonJS
                    }, {
                        loader: "sass-loader" // compiles Sass to CSS
                    }]
                })
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: 'file-loader?name=[name].[ext]&outputPath=images'
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name].'+parseInt(new Date()/1000)+'.css',
            allChunks: true
        }),
        new CleanWebpackPlugin(['build'], {
            //root: '/',
            verbose: true,
            dry: false
        }),
        new HtmlWebpackPlugin({
            title: 'Caching',
            favicon: './favicon.ico',
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: "main",
            filename: "main."+parseInt(new Date()/1000)+".min.js",
            minChunks: Infinity
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
    ],
    // Inform webpack that we're building a bundle
    // for nodeJS, rather than for the browser
    target: 'node',

    // Tell webpack the root file of our
    // server application
    entry: './src/index.js',
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "build/js"),
        publicPath: "/js/"
    },

    externals: [webpackNodeExternals()]
};

module.exports = config;
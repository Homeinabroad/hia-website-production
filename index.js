// For convention/compatibility - run the server process by default if "node ." is executed in the top-level folder
// Make sure we are running node 7.10+
const [major, minor] = process.versions.node.split(".").map(parseFloat);
if (major < 7 || (major === 10 && minor <= 1)) {
    console.log("This code requires a minimum of node 7.10.1 to run. \n Please go to nodejs.org and download version 7.10.1 or greater.\n ");
    process.exit();
}
require("./build/js/main.js");

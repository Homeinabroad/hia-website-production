const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");
const CompressionPlugin = require('compression-webpack-plugin');

const config = {
    module: {
        rules: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                        //because remove style-loader,my css can't work
                        loader: "css-loader", options: { importLoaders: 1 }// translates CSS into CommonJS
                    }, {
                        loader: "sass-loader" // compiles Sass to CSS
                    }]
                })
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: 'file-loader?name=[name].[ext]&outputPath=images'
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            title: 'Caching',
            jsExtension: ".gz",
            cssExtension: ".gz",
            favicon: './favicon.ico',
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new CleanWebpackPlugin(['public'], {
            //root: '/',
            verbose: true,
            dry: false
        }),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.js$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.ContextReplacementPlugin(/moment[\\\/]lang$/, /^\.\/(en)$/),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
        //new webpack.optimize.MinChunkSizePlugin({ minChunkSize: 200000 })
    ],
    // Tell webpack the root file of our
    // server application
    entry: ['./src/client/client.js', './src/assets/scss/styles.scss'],
    // Tell webpack where to put the output file
    // that is generated
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "public/js"),
        publicPath: "/js/"
    }
};
module.exports = config;
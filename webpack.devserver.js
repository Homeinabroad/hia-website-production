const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackNodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require("webpack");

const config = {
    module: {
        rules: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["es2015", "react", "stage-0"]
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(jpg|png|svg|gif)$/,
                use: 'file-loader?name=[name].[ext]&outputPath=images'
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            allChunks: true
        }),

        new CleanWebpackPlugin(['build'], {
            //root: '/',
            verbose: true,
            dry: false
        }),
        new HtmlWebpackPlugin({
            title: 'Caching',
            favicon: './favicon.ico',
        }),

        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        })
    ],
    target: 'node',
    entry: './src/index.js',
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "build/js"),
        publicPath: "/js/"
    },
    mode:'development',
    externals: [webpackNodeExternals()]
};

module.exports = config;
